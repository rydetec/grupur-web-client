
GruPur Platform
==============
Description
------------------------------------------------------

# GruPur is a free open-source decentralized social media platform focused on group interaction, democratic moderation, issue polling, and media discussion.

The entire GruPur platform can be downloaded and modified by anyone, there is no central hosting place for the GruPur network so it does not rely on any one company or individual to host the data. This keeps any entity from attempting to sabatoge our network with DDoS attacks, database hacks or code exploits. Everyone is entitled to everything that GruPur is and everyone is responsible for holding themselves accountable for what they do with it.

## We believe in the right to free speech as well as the individual responsibility to maintain and moderate that right.


Technicial
-------------------
The GruPur web client production environment is hosted on the IPFS https://ipfs.io network. Data can be pinned - based on a tokenomic services model - to be permanently stored on the cluster. IPFS enables decentralized web hosting and supports the management of real top level domains.

As for our database, we store all data using the Ethereum blockchain and smart contracts. Ethereum is a "world computer". Harking back to the days of the mainframe, and probably about as fast, Ethereum can be viewed as a single computer that the whole world can use.

Documentation
----------------
Our documentation can be found at https://web-client.readthedocs.io

Data
--------
GruPur utilizes a decentralized blockchain database. Any data created or stored using GruPur is immutable, meaning it can never be modified and it can never be deleted. Every individual is responsible for their own data on GruPur. If you lose your passphrase, you lose your ability to verify ownership of the data.

GruPur is designed to be a permenant public record. All data is accessible by anyone and can never be deleted. Data owners can append extra metadata to the assets updating values, creating the transaction chain seen in Blockchain applications.


Disclaimer
---------------
## ALL DATA ON GRUPUR IS PUBLIC AND ACCESSIBLE AS PLAIN TEXT. DO NOT SUBMIT ANY PRIVATE DATA. EVERY INDIVIDUAL IS THEMSELVES RESPONSIBLE FOR THE CONTENT THEY SUBMIT.

