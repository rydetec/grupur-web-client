.. _broadcast:

====================
Broadcasts
====================
Description
---------------------------------------------------------
A broadcast_ is an immutable public signed message created by an account_ asset. Broadcasts can use group_ tags to be displayed in a group_ feed and be awarded relevance_ tokens from group_ relevance_ owners that find the content relevant to the group.

A broadcast_ is the core database_ asset_ that is used as consumer content for the `Global <global-feed>`_, Group_, and Account_ feeds. They are messages created through the broadcast_ area and signed by accounts. These messages can be 240 characters long, any handle_ characters are excluded from the character count as well as any portion of a url other than the root domain.

If a handle_ (**#**, **@** or **>**) is used in a broadcast_ it will automatically link to the coorosponding asset_. A **#** is used to tag a group_ or week_, an **@** is used to tag an account_ and a **>** is used to reply to another broadcast_.

If a url is used in a broadcast_ it will be parsed, linked and stripped to the root domain. The first url in any broadcast_ will be parsed and embeded. If no content is found at the link, a screenshot of the webpage will be shown along with the title of the website.

Broadcasts will be filed under the week_ it was created. All broadcasts can be found in the `Global Feed <global-feed>`_ of their week_, in all groups_ they tag and on the author's account page. A broadcast_ with a mention_ will also show up in the mentions section of their account_ page. Any reply to a broadcast will be shown on the individual post page when selected.

Any broadcast posted generates one status point for the author and can be used to transfer to other authors for their content.

To toggle the broadcast_ area on a compatible page, tap the bullhorn icon. Your message preview should update as you type.

Broadcast Asset
-----------------

.. code-block:: javascript

    // This is a broadcast asset
    const broadcast = {
        appId: APP_ID, 
        year: "year-" + new Date().getWeekYear(),
        week: "week-" + new Date().getWeek().doubleDigitString(),
        type: ASSET_BROADCAST,
        account: user.publicKey,
        timestamp: new Date().toString()
    }
    
Broadcast Data
-----------------

.. code-block:: javascript

    // This is a message data type
    const message = {
        appId: APP_ID, 
        year: "year-" + new Date().getWeekYear(),
        week: "week-" + new Date().getWeek().doubleDigitString(),
        type: DATA_MESSAGE,
        data: {
            description: "Broadcast message",
            timestamp: new Date().toString(),
            message: "This is a #test #message."
        }
    }

.. _account: ./account.html
.. _group: ./assets.html#group
.. _groups: ./assets.html#group
.. _relevance: ./assets.html#relevance
.. _asset: ./assets.html
.. _mention: ./handles.html
.. _handle: ./handles.html
.. _week: ./week.html
.. _database: ./database.html