.. _handle:

================
Handles
================
Description
--------------------------------
Handles are special strings that will be parsed into links when rendering pages and posts. They can be included in broadcast messages to hot link around the site and include broadcast in other feeds like Mentions or Group feeds.

Types
---------------
There are a few different types of handles:

- **#** is for group_ or week_ tags. Using this in a post will make the post visible in that group_ but will not place the post in a tagged week_
- **@** is for mentioning an account_ this will hyperlink to their account_ page.
- **>** is for replying to a specific broadcast_ this will hyperlink the to the broadcast you are referencing.

Examples
-------------------

When an external message is to be displayed it may look like the following.

.. code-block:: javascript
    :linenos:
    
    var rawMessage = "Check out this #gif @aB73jMwl18 https://www.example.com/media/archive/2017/10/1/this.gif";
    
All external string are sent through parseRawMessage() for handles and urls to append html code or strip it.

.. code-block:: javascript
    :linenos:
    
    var parsedMessage = parseRawMessage(rawMessage);
    body.innerHTML = parsedMessage;
    
And so the body of the page would have

.. code-block:: html
    :linenos:
    
    Check out this <a href='.\feed.html?tag=gif'>#gif</a> <a href='.\account.html?name=aB73jMwl18>@aB73jMwl18</a> <a href='https://www.example.com/media/archive/2017/10/1/this.gif'>example.com</a>
    
Notes
--------

For character counting purposes the html stripped `parsed message <text-parsing>`_ is counted.

So for the above example when counting characters the algorithm will only count we get 41 since we include whitespace.

.. code-block:: text
    :linenos:
    
    Check out this gif aB73jMwl18 example.com


.. _broadcast: ./broadcasts.html
.. _group: ./group.html
.. _week: ./week.html
.. _account: ./account.html