==============
Database
==============
Data
--------
GruPur utilizes a decentralized blockchain database_. Any data created or stored using GruPur is immutable, meaning it can never be modified and it can never be deleted. `Every individual is responsible for their own data on GruPur <index.html#disclaimer>`_. If you lose your passphrase, you lose your ability to verify ownership of the data.

GruPur is designed to be a permenant public record. All data is accessible by anyone and can never be deleted. Data owners can append extra metadata to the assets updating values, creating the transaction chain seen in Blockchain applications.

.. _Ed25519Keypair:
Ed25519Keypair
--------
BigchainDB JavaScript driver allows you to create a keypair based on a seed. The constructor accepts a 32 byte seed. One of the ways to create a seed from a string (e.g. a passphrase) is the one used by bip39, specifically the function mnemonicToSeed.

.. _mnemonic:
Mnemonic
--------
A mnemonic is a combination of words from a word bank to be used as a seed for generation of your Private and Public Ed25519Keypair to sign transactions to the database. The word bank is comprised of 2048 words and when an account is registered for GruPur you are given 12 random words from the bank as your passkey to generate your unique KeyPair.