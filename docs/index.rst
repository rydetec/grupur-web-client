==============
Platform
==============
Description
------------------------------------------------------

*GruPur is a free open-source decentralized social media platform focused on public group interaction, democratic moderation, issue polling, and media discussion.* 

The entire GruPur platform can be `downloaded <source>`_ and modified by anyone, there is no `central hosting place <#technicial>`_ for the GruPur network so it does not rely on any one company or individual to host the data. This keeps any entity from attempting to sabatoge our network with DDoS attacks, database hacks or code exploits. Everyone is entitled to everything that GruPur is and everyone is responsible for holding themselves and others accountable for what they do with it.

**We believe in the right to free speech as well as the individual responsibility to maintain and moderate that right.**

.. technicial:
Technicial
-------------------
The GruPur web client production environment is hosted on the `IPFS <https://ipfs.io/>`_ network. Data can be pinned - based on a tokenomic services model - to be permanently stored on the cluster. IPFS enables decentralized web hosting and supports the management of real top level domains.

As for our database_, we store all data using `BigChainDB <https://www.bigchaindb.com/>`_. BigchainDB allows developers and enterprise to deploy blockchain proof-of-concepts, platforms and applications with a blockchain database, supporting a wide range of industries and use cases.

Data
--------
GruPur utilizes a decentralized blockchain database_. Any data created or stored using GruPur is immutable, meaning it can never be modified and it can never be deleted. `Every individual is responsible for their own data on GruPur <#disclaimer>`_. If you lose your passphrase, you lose your ability to verify ownership of the data.

GruPur is designed to be a permenant public record. All data is accessible by anyone and can never be deleted. Data owners can append extra metadata to the assets updating values, creating the transaction chain seen in Blockchain applications.

.. disclaimer:
Disclaimer
---------------
**ALL DATA ON GRUPUR IS PUBLIC AND ACCESSIBLE AS PLAIN TEXT. DO NOT SUBMIT ANY PRIVATE DATA. EVERY INDIVIDUAL IS THEMSELVES RESPONSIBLE FOR THE CONTENT THEY SUBMIT.**

.. _database: ./database.html
.. _source: https://bitbucket.org/rydetec/grupur-web-client/src