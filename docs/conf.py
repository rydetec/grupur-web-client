import re

import sphinx

from recommonmark.parser import CommonMarkParser

source_parsers = {
    '.md': CommonMarkParser,
}

source_suffix = ['.rst', '.md']

project = 'GruPur Platform'
author = 'GruPur'
copyright = '2019 GruPur'
version = '0.0.2a1'
release = version

html_sidebars = {
   '**': ['globaltoc.html', 'sourcelink.html', 'searchbox.html']
}
