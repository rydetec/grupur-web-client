=========
Groups
=========
Groups are created when a user visits a tag page, which has not already been claimed, and claims it for themselves. Tag pages are linked from broadcasts when a **#** handle_ is used in the message of a broadcast. On the tag page, every broadcast with the same tag in its message will be displayed in the feed. When a user claims ownership of a group, they are awarded relevance_ points they can transfer to other users for their quality broadcasts and increase their positions in relevance-sorted feeds.

.. _handle: ./handles.html
.. _relevance: ./transfers.html#relevance