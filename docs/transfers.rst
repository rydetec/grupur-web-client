.. _transfers:
============
Transfers
============
A transfer is an asset_ that has tokens_ that can be divided and distributed to other accounts_. The only two current transferable assets_ are called relevance_ and status_. Transfers help increase broadcasts_ positions in the Relevant and Top sorted feeds. Any transfer made to a broadcast_ not made in the current week_ will not change the position but will still transfer to the account for their usage.

.. _relevance:
Relevance
-----------
When an account goes to a **#** handle_ tag page, that has not been claimed, they can type a permanant description for the tag and claim the ownership for themselves. When the tag is claimed, 240 relevance points are awarded to the owner for the tag. Any broadcast_ that has the tag in the message will be displayed on the page in cronological order. The owner of relevance points for the group can award quality broadcasts with points to increase it's position on the Relevance sorted feed. The users who have been rewarded relevance points can then award other users posts to increase their positions.

Relevance Asset

.. code-block:: javascript

    //This is a group asset
    const group = {
        appId: APP_ID, 
        year: "year-" + new Date().getWeekYear(),
        week: "week-" + new Date().getWeek().doubleDigitString(),
        type: ASSET_RELEVANCE,
        tag: "",
        account: user.publicKey,
        timestamp: new Date().toString()
    }
    
Relevance Data

.. code-block:: javascript

    //This is a vote data type
    const vote = {
        appId: APP_ID, 
        year: "year-" + new Date().getWeekYear(),
        week: "week-" + new Date().getWeek().doubleDigitString(),
        type: DATA_TRANSFER,
        data: {
            description: "Group vote share transfer",
            timestamp: new Date().toString()
        }
    }
    

.. _status: 
Status
---------
When an account sends a broadcast of any kind they will generate a single status point for their account. This can be used to transfer to other accounts for their quality posts which will increase the broadcast position in top-sorted feeds. The broadcast assets themselves are used as the tokens for status as opposed to creating a completly seperate asset each time a broadcast is made.

.. _asset: ./assets.html
.. _tokens: ./transactions.html#tokens
.. _accounts: ./account.html
.. _assets: ./assets.html
.. _broadcasts: ./broadcasts.html
.. _broadcast: ./broadcasts.html
.. _week: ./week.html
.. _handle: ./handles.html