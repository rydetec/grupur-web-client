=============
Web Client
=============

Recent Changes
----------
- Now hosting all files on ipfs directly

To host web client on local node with ipfs do
ipfs get /ipns/www.grupur.com
ipfs add -r ./www.grupur.com


Hosting
----------

Official HTTPS
    The official build is hosted on the `IPFS <https://ipfs.io/>`_ Phantom dApp which is a decentralized website hosting solution. Our files are uploaded to the IPFS filemanager. The official link and currently the only way to reach the official client via HTTPS is at https://ipfs.io/ipns/www.grupur.com/
    
Official Domain
    Our official domain GruPur.com was purchased from GoDaddy and the DNS A record @ points to 184.168.131.241 **which is a centralized DNS host at GoDaddy** the root domain is forwarded to `http://www.grupur.com` which is a CNAME record pointing to gateway.ipfs.io our official domain should be considered centralized and for the most secure connection you should use the Official HTTPS domain.

Unofficial Domains
    To point your own domain at our web client, create a CNAME record of any name and point it to gateway.ipfs.io then create a TXT record named _dnslink.**<CNAME name>** and point it to dnslink=/ipns/www.grupur.com
    
    At this point there are no Unofficial Domains that have been registered.

Unofficial Clients
    Users may `download the source <source>`_ and modify it as they like while still choosing to use GruPur.com's official database. Applications range from secure private hosting of the client, to heavily modifiying the client to suit your needs, to experimenting with programming. If you find bugs or make improvements you would like to see implemented into the official client, you can make a pull request on the dev branch. Users can also choose a different app id from the official client's to create their own database for other communities that don't use any of the official GruPur data.

Interface
----------

The web client interface consist of a few main areas: home, login, register, account and feed pages.

Home
    The home page is only seen by non-logged in users. It's purpose is to give new users a summary of what the website is about and incentive to login.
    
Login
    The login page is where users can authenticate themselves verifying their idenity using their 12 word mnemonic passphrase. This will then send them to their account page.
    
Register
    The register page is where users can obtain a randomly generated 12 word mnemonic passphrase which can be downloaded to your computer.
    
Account
    The account page when visted by the owner of the account will show your recent broadcasts and mentions as well as controls for you to modify your account. By adding the name= parameter along with a user handle you will pull up their account page showing their broadcasts, mentions and other data.
    
Feed
    The feed page by default will show all global broadcasts from the current calendar week. By adding the tag= parameter you will pull up the associated group from the tag. By adding the week= parameter you can pull up the global broadcasts from that calendar week. By adding both tags you can view just the broadcasts using a tag from a certian week. By adding the sort= parameter you can sort the post by new, top or relevant. You can pull up a single post and all replies with the post= parameter.
    
Code
-------
The code is written in Javascript and HTML and hosted at `BitBucket <source>`_

Dependencies
-------------
    - AllOrigins - We use AllOrigins to get around the Cross-Origin policy of modern browsers.
    - Nude.JS - Internal nudity filter
    - FontAwesome - Icons
    - BigChainDB - Database storage
    - IPFS - Interplanetary File System
    - textance.herokuapp.com - Generate titles from websites.
    - api.letsvalidate.com - Generate website thumbnails.
    - nsfw.haschek.at - External nudity filter
    - GoDaddy - DNS for grupur.com

.. _source: https://bitbucket.org/rydetec/grupur-web-client/src