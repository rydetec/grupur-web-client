.. _account:

====================
Account
====================
Description
--------------------------------------
Accounts are `public-private key pairs <Ed25519Keypair>`_ generated using a 12 word `Mnemonic <mnemonic>`_. The public key is used as the account identifier with the user name handle_ being the first 10 characters of the public key. The private key is used to sign transactions to the `BigChainDB Blockchain <database>`_ assets_.

An account asset_ is created on the register_ page when a user presses the "Register" button. The *doRegister()* method is called which generates a random 12 word mnemonic_ which is displayed to the user along with a link for the user to download the file with the passphrase. 

An account asset_ is created with the app id, year, week, timestamp, asset type, and full public key. The metadata_ of the asset_ creation transaction_ consists of a description, timestamp, week and year.

Everytime an account is logged in, a self transfer transaction_ is created updating the metadata of the login timestamp. The public key is stored as a cookie in the browser and if the user toggled to stay logged in the private key is also stored as a cookie in the browser.

Asset Structure
---------------------

Asset

.. code-block:: javascript
    :linenos:

    // This is an account asset
    var accountAsset = {
        appId: APP_ID, // APP_ID is a global in grupur.js
        year: "year-" + new Date().getWeekYear(),
        week: "week-" + new Date().getWeek().doubleDigitString(),
        type: ASSET_ACCOUNT, // Asset types are globals in grupur.js
        account: user.publicKey, // You will need the user public key generated from the mnemonic
        timestamp: new Date().toString()
    };
    
Creation Metadata

.. code-block:: javascript
    :linenos:

    // This is the account creation metadata
    var creationMetadata = {
        appId: APP_ID, 
        year: "year-" + new Date().getWeekYear(), 
        week: "week-" + new Date().getWeek().doubleDigitString(), 
        type: DATA_LOGIN, 
        description: "Account creation", 
        timestamp: new Date().toString()
    };
    
Login Metadata

.. code-block:: javascript
    :linenos:
    
    // This is the account login metadata
    var loginMetadata = {
        appId: APP_ID, 
        year: "year-" + new Date().getWeekYear(), 
        week: "week-" + new Date().getWeek().doubleDigitString(), 
        type: DATA_LOGIN, 
        description: "Login timestamp", 
        timestamp: new Date().toString()
    };
    
    
Account Asset

.. code-block:: javascript

    // This is an account asset
    const accountAsset = {
        appId: APP_ID, 
        year: "year-" + new Date().getWeekYear(),
        week: "week-" + new Date().getWeek().doubleDigitString(),
        type: ASSET_ACCOUNT, 
        account: user.publicKey, 
        timestamp: new Date().toString()
    };
    
    
Account Data

.. code-block:: javascript

    // This is a login data type
    const loginTimestamp = {
        appId: APP_ID, 
        year: "year-" + new Date().getWeekYear(),
        week: "week-" + new Date().getWeek().doubleDigitString(),
        type: DATA_LOGIN,
        data: {
            description: "Login timestamp", 
            timestamp: new Date().toString()
        }
    }
    
    // This is a user info data type
    const userInfo = {
        appId: APP_ID, 
        year: "year-" + new Date().getWeekYear(),
        week: "week-" + new Date().getWeek().doubleDigitString(),
        type: DATA_USER,
        data: {
            description: "User data",
            timestamp: new Date().toString(),
            displayName: "",
            avatar: "",
            bitcoin: ""
        }
    }

.. _handle: ./handles.html
.. _assets: _asset
.. _asset: ./assets.html
.. _register: www.grupur.com/register.html
.. _metadata: ./transactions.html#metadata
.. _transaction: ./transactions.html
.. _database: ./database.html
.. _Ed25519Keypair ./database.html#Ed25519Keypair
.. _mnemonic ./database.html#mnemonic