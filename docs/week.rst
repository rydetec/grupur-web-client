.. _week:

====================
Week
====================

GruPur weeks span between Monday and Sunday every week. Activity in these weeks will be recorded. Posts made in a week will be displayed on that weeks global feed. Only Relevance and Status gained in a week will increase it's position in the feed, but all Relevance and Status will still be counted in totals no matter the time frame of the transaction. GruPur new year will always start on the first Monday of every January. Any week can be refrenced by it's handle for example #2019-12 would span the week of Monday March 18, 2019 - Sunday March 24, 2019.