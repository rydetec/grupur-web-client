==============
Transactions
==============
There are two different possible transactions. Asset Creation and Asset Transfer. Using these two types of transactions you can create assets you own and transfer their values to other accounts for them to transfer themselves to others. This is core to the immutability of the block-chain database that BigChainDB is.

.. _metadata:
Metadata
---------
Metadata is data that is included in the creation and transfer transactions that allows a developer to use these transactions to store and manipulate data.