===============
Asset 
===============
Assets are created by the web client and are transfered to the BigchainDB network
---------------

asset_
    An asset is a javascript object that holds static information about the asset including individual identifiers and maximum tokens available. Once created, an asset can never be deleted nor can any more tokens be created. However, ownership of the indivdual tokens can be transferred from one peer to another.
    
metadata_
    Metadata is the extra data attached to the transactions of on asset. Meaning this data is new for each transfer. This is how you would update dynamic variables in an asset. To create an updated metadata, create a self-transfer transaction.

broadcast_
    A broadcast is an asset that is created by an account. It is essentially a signed message with an array of hashtags. Any tag used in a broadcast will place that post in the group feed. Owners of group relevance can transfer their relevance assets to increase the position of a post in the global and group feeds. The author of the post will now be able to transfer their earned relevance points to other users. Each broadcast made generates one status asset which can also bre transferred to other users to increase their broadcasts positions in the top sorting.
    
group_
    A group is created when a user claims a group which has not already been claimed. Searching for a hashtag or going directly to the group page will show you a feed of all broadcasts using the hashtag sorted by relevance.
    
relevance_
    The creator of a group will recieve 240 relevance tokens for the group. Users with Relevance tokens can transfer those tokens to users who have posted relevant information to improve the positions of their post. Once a user has spent all of their relevance, they can only gain more by them being transferred back from the new owners.

status_
    Status is generate once per broadcast. Users can transfer these points to other users to increase their broadcasts positions in the Top sorting pages. They can then retransfer those status points to whomever they desire.

.. _asset: ./asset.html
.. _metadata: ./transactions.html#metadata
.. _broadcast: ./broadcasts.html
.. _group: ./group.html
.. _relevance: ./transfers.html#relevance
.. _status: ./transfers.html#status