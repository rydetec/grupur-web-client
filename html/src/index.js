

// Get the header

var feed = document.getElementById("feed");
var options = document.getElementById("options");

if (readCookie("publicKey")) {
    document.location = "./feed.html"
}


var welcomeElement = document.createElement("div");
    welcomeElement.id = "welcomeElement";
    welcomeElement.className = "feedElement";
    welcomeElement.innerHTML = "<h2>GruPur</h2> \
    <h3>Broadcast your thoughts to the world, stay relevant.</h3>\
\
    <p>GruPur's technology insures the individual right to privacy and free speech.</p> \
\
    <p>Join the worldwide conversation and speak your mind.</p> \
    <p><b>Welcome to <u>YOUR</u> social platform.</b></p>\
<br>";
//var welcome = document.createTextNode();
//testElement.appendChild(welcome);
feed.appendChild(welcomeElement);

var spacerElement = document.createElement("div");
            spacerElement.id = "spacer"
            spacerElement.className = "feedSpacer";
            feed.appendChild(spacerElement)


var detailsElement = document.createElement("div");
    detailsElement.id = "detailsElement";
    detailsElement.className = "feedElement";
    detailsElement.innerHTML = "<h2>What is GruPur?</h2> \
    <p>The GruPur platform is an open source serverless community engine that is built to power any content-based economy. With GruPur's network, all content is anonymous, public and immutable with no authority other than you. <a style=\"text-decoration: underline;\" href=\"./why.html\">Why?</a></p> \
\
    <p>The GruPur web client is hosted on the <a style=\"text-decoration: underline;\" href=\"https://ipfs.io\">IPFS</a> network. Data can be pinned - based on a tokenomic services model - to be permanently stored on the cluster. IPFS enables decentralized web hosting and supports the management of real top level domains.</p> \
\
    <p>As for our database, we store all data using the <a style=\"text-decoration: underline;\" href=\"https://www.etherscan.io/\">Ethereum blockchain and smart contracts</a>. Ethereum is a \"world computer\". Harking back to the days of the mainframe, and probably about as fast, Ethereum can be viewed as a single computer that the whole world can use.</p>\
\
    <p>All of our website source code <a style=\"text-decoration: underline;\" href=\"https://bitbucket.org/rydetec/grupur-web-client/src\">is available through git</a>, the <a style=\"text-decoration: underline;\" href=\"https://web-client.readthedocs.io/\">documentation can be accessed by anyone</a>. Contribute to your social platform.</p>";
//var welcome = document.createTextNode();
//testElement.appendChild(welcome);
feed.appendChild(detailsElement);


//var spacerElement2 = document.createElement("div");
//            spacerElement2.id = "spacer"
//            spacerElement2.className = "feedSpacer";
            //feed.appendChild(spacerElement2)


//var detailsElement2 = document.createElement("div");
////    detailsElement2.id = "detailsElement2";
//    detailsElement2.className = "feedElement";
//    detailsElement2.innerHTML = "<h2>How to access?</h2> \
 //   <p>To access the GruPur network you will need a few things. Please install the IPFS browser extension <a href=\"https://github.com/ipfs-shipyard/ipfs-companion\">here.</a> You will also need an Etherum wallet extension on the Ropsten testnet. You can do this with the MetaMask extension <a href=\"https://metamask.io/download.html\">here.</a></p> \
//\
 //   <p>Once you have downloaded and installed these extensions or similar ones, please refresh the page. MetaMask allows your browser to send transactions directly to the Etherum network. IPFS allows you to upload and download files over a decentralized network.</p>";
//var welcome = document.createTextNode();
//testElement.appendChild(welcome);
//feed.appendChild(detailsElement2);

var newAccount = document.createElement("div");
    newAccount.id = "dbTest";
    newAccount.className = "optionsElement";
    newAccount.innerHTML = "<a class=\"a-light\" href=\"./register.html\">New Account</a>";

options.appendChild(newAccount);

var loginButton = document.createElement("div");
    loginButton.id = "dbTest";
    loginButton.className = "optionsElement";
    loginButton.innerHTML = "<a class=\"a-light\" href=\"./login.html\">Login</a>";

options.appendChild(loginButton);



