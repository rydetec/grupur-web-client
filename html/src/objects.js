/*
Filename: objects.js
Description: Responsible for declaration all objects to be used throughout the grupur platform. 
*/

// Ethereum smart contract connector
class Blockchain {

	constructor() {

		this.queueDelay = 1000
		this.lastCall = new Date().getTime()

		this.net = {
			ropsten: {
				account: {
					pub: "0x0b89dFa138B7258AfDa7d21E47dc0149C4f88287",
					priv: "4BD761964C751E8193E4FCFB5848EC8D2C3CF6A625CD58EF7C45BA32D939CEB2"
				},
				address: {
					grupur: "0xAEA122D87274871fD2cfCABa49085010A709AEB2",
					credentialStorage: "0xca6B055C376A1C75a589c674f38acA27DE8586c9",
					vanityStorage: "0x3218A191971f65c5Aa9fb0F1c21c8F0aBB81Ac06",
					walletStorage: "0x0BdABfE217303AD2d0E23A623A9d05b638277bc6",
					broadcastStorage: "0xB7914e13515b41e31d5Ae0f37Efdc11c375A8080"
				},
				contract: [
					{
						"constant": true,
						"inputs": [
							{
								"internalType": "address",
								"name": "_contract",
								"type": "address"
							},
							{
								"internalType": "string",
								"name": "_publicKey",
								"type": "string"
							}
						],
						"name": "getRelevanceBalance",
						"outputs": [
							{
								"internalType": "uint256[]",
								"name": "",
								"type": "uint256[]"
							}
						],
						"payable": false,
						"stateMutability": "view",
						"type": "function"
					},
					{
						"constant": true,
						"inputs": [],
						"name": "getVersion",
						"outputs": [
							{
								"internalType": "string",
								"name": "",
								"type": "string"
							}
						],
						"payable": false,
						"stateMutability": "view",
						"type": "function"
					},
					{
						"constant": false,
						"inputs": [
							{
								"internalType": "address",
								"name": "_contract",
								"type": "address"
							},
							{
								"internalType": "address",
								"name": "_wallet",
								"type": "address"
							},
							{
								"internalType": "string",
								"name": "_username",
								"type": "string"
							},
							{
								"internalType": "string",
								"name": "_pub",
								"type": "string"
							},
							{
								"internalType": "string",
								"name": "_priv",
								"type": "string"
							},
							{
								"internalType": "string",
								"name": "_rev",
								"type": "string"
							}
						],
						"name": "registerCredential",
						"outputs": [],
						"payable": false,
						"stateMutability": "nonpayable",
						"type": "function"
					},
					{
						"constant": true,
						"inputs": [
							{
								"internalType": "address",
								"name": "_contract",
								"type": "address"
							},
							{
								"internalType": "uint256",
								"name": "_year",
								"type": "uint256"
							},
							{
								"internalType": "uint256",
								"name": "_week",
								"type": "uint256"
							},
							{
								"internalType": "string",
								"name": "_tag",
								"type": "string"
							}
						],
						"name": "getTaggedBroadcastsDuring",
						"outputs": [
							{
								"internalType": "uint256[]",
								"name": "",
								"type": "uint256[]"
							}
						],
						"payable": false,
						"stateMutability": "view",
						"type": "function"
					},
					{
						"constant": false,
						"inputs": [
							{
								"internalType": "address",
								"name": "_contract",
								"type": "address"
							},
							{
								"internalType": "address",
								"name": "_wallet",
								"type": "address"
							},
							{
								"internalType": "string",
								"name": "_public",
								"type": "string"
							},
							{
								"internalType": "uint256",
								"name": "_year",
								"type": "uint256"
							},
							{
								"internalType": "uint256",
								"name": "_week",
								"type": "uint256"
							},
							{
								"internalType": "string",
								"name": "_timestamp",
								"type": "string"
							},
							{
								"internalType": "string",
								"name": "_message",
								"type": "string"
							},
							{
								"internalType": "string",
								"name": "_filename",
								"type": "string"
							},
							{
								"internalType": "string",
								"name": "_hash",
								"type": "string"
							}
						],
						"name": "addBroadcast",
						"outputs": [],
						"payable": false,
						"stateMutability": "nonpayable",
						"type": "function"
					},
					{
						"constant": true,
						"inputs": [
							{
								"internalType": "address",
								"name": "_contract",
								"type": "address"
							},
							{
								"internalType": "string",
								"name": "_public",
								"type": "string"
							}
						],
						"name": "getLatestBroadcastsBy",
						"outputs": [
							{
								"internalType": "uint256[]",
								"name": "",
								"type": "uint256[]"
							}
						],
						"payable": false,
						"stateMutability": "view",
						"type": "function"
					},
					{
						"constant": true,
						"inputs": [
							{
								"internalType": "address",
								"name": "_contract",
								"type": "address"
							},
							{
								"internalType": "string",
								"name": "_publicKey",
								"type": "string"
							},
							{
								"internalType": "uint256",
								"name": "_index",
								"type": "uint256"
							}
						],
						"name": "getRelevanceTag",
						"outputs": [
							{
								"internalType": "string",
								"name": "",
								"type": "string"
							}
						],
						"payable": false,
						"stateMutability": "view",
						"type": "function"
					},
					{
						"constant": true,
						"inputs": [
							{
								"internalType": "address",
								"name": "_contract",
								"type": "address"
							},
							{
								"internalType": "string",
								"name": "_username",
								"type": "string"
							}
						],
						"name": "getCredentialRevokeCert",
						"outputs": [
							{
								"internalType": "string",
								"name": "",
								"type": "string"
							}
						],
						"payable": false,
						"stateMutability": "view",
						"type": "function"
					},
					{
						"constant": true,
						"inputs": [
							{
								"internalType": "address",
								"name": "_contract",
								"type": "address"
							},
							{
								"internalType": "uint256",
								"name": "_index",
								"type": "uint256"
							}
						],
						"name": "getBroadcastDetails",
						"outputs": [
							{
								"internalType": "string",
								"name": "",
								"type": "string"
							},
							{
								"internalType": "uint256",
								"name": "",
								"type": "uint256"
							},
							{
								"internalType": "uint256",
								"name": "",
								"type": "uint256"
							},
							{
								"internalType": "string",
								"name": "",
								"type": "string"
							},
							{
								"internalType": "string",
								"name": "",
								"type": "string"
							},
							{
								"internalType": "string",
								"name": "",
								"type": "string"
							},
							{
								"internalType": "string",
								"name": "",
								"type": "string"
							}
						],
						"payable": false,
						"stateMutability": "view",
						"type": "function"
					},
					{
						"constant": true,
						"inputs": [
							{
								"internalType": "address",
								"name": "_contract",
								"type": "address"
							},
							{
								"internalType": "string",
								"name": "_username",
								"type": "string"
							}
						],
						"name": "getCredentialPublicKey",
						"outputs": [
							{
								"internalType": "string",
								"name": "",
								"type": "string"
							}
						],
						"payable": false,
						"stateMutability": "view",
						"type": "function"
					},
					{
						"constant": true,
						"inputs": [
							{
								"internalType": "address",
								"name": "_contract",
								"type": "address"
							},
							{
								"internalType": "string",
								"name": "_index",
								"type": "string"
							}
						],
						"name": "getBroadcastsUnder",
						"outputs": [
							{
								"internalType": "uint256[]",
								"name": "",
								"type": "uint256[]"
							}
						],
						"payable": false,
						"stateMutability": "view",
						"type": "function"
					},
					{
						"constant": true,
						"inputs": [
							{
								"internalType": "address",
								"name": "_contract",
								"type": "address"
							},
							{
								"internalType": "string",
								"name": "_publicKey",
								"type": "string"
							}
						],
						"name": "getStatusBalance",
						"outputs": [
							{
								"internalType": "uint256",
								"name": "",
								"type": "uint256"
							}
						],
						"payable": false,
						"stateMutability": "view",
						"type": "function"
					},
					{
						"constant": true,
						"inputs": [
							{
								"internalType": "address",
								"name": "_contract",
								"type": "address"
							},
							{
								"internalType": "string",
								"name": "_publicKey",
								"type": "string"
							}
						],
						"name": "getLatestVanity",
						"outputs": [
							{
								"internalType": "string",
								"name": "",
								"type": "string"
							},
							{
								"internalType": "string",
								"name": "",
								"type": "string"
							},
							{
								"internalType": "string",
								"name": "",
								"type": "string"
							},
							{
								"internalType": "string",
								"name": "",
								"type": "string"
							}
						],
						"payable": false,
						"stateMutability": "view",
						"type": "function"
					},
					{
						"constant": true,
						"inputs": [
							{
								"internalType": "address",
								"name": "_contract",
								"type": "address"
							},
							{
								"internalType": "string",
								"name": "_username",
								"type": "string"
							}
						],
						"name": "getCredentialPrivateKey",
						"outputs": [
							{
								"internalType": "string",
								"name": "",
								"type": "string"
							}
						],
						"payable": false,
						"stateMutability": "view",
						"type": "function"
					},
					{
						"constant": false,
						"inputs": [
							{
								"internalType": "address",
								"name": "_contract",
								"type": "address"
							},
							{
								"internalType": "string",
								"name": "_publicKey",
								"type": "string"
							},
							{
								"internalType": "string",
								"name": "_avatar",
								"type": "string"
							},
							{
								"internalType": "string",
								"name": "_vanity",
								"type": "string"
							},
							{
								"internalType": "string",
								"name": "_bio",
								"type": "string"
							}
						],
						"name": "addVantiy",
						"outputs": [],
						"payable": false,
						"stateMutability": "nonpayable",
						"type": "function"
					},
					{
						"constant": true,
						"inputs": [
							{
								"internalType": "address",
								"name": "_contract",
								"type": "address"
							},
							{
								"internalType": "string",
								"name": "_username",
								"type": "string"
							}
						],
						"name": "getCredentialId",
						"outputs": [
							{
								"internalType": "uint256",
								"name": "",
								"type": "uint256"
							}
						],
						"payable": false,
						"stateMutability": "view",
						"type": "function"
					},
					{
						"constant": true,
						"inputs": [
							{
								"internalType": "address",
								"name": "_contract",
								"type": "address"
							},
							{
								"internalType": "uint256",
								"name": "_year",
								"type": "uint256"
							},
							{
								"internalType": "uint256",
								"name": "_week",
								"type": "uint256"
							}
						],
						"name": "getBroadcastsDuring",
						"outputs": [
							{
								"internalType": "uint256[]",
								"name": "",
								"type": "uint256[]"
							}
						],
						"payable": false,
						"stateMutability": "view",
						"type": "function"
					},
					{
						"payable": true,
						"stateMutability": "payable",
						"type": "fallback"
					}
				]
			}
		}

		this.web3Provider = null

		this.conn = null

		this.account = null

		this.address = null

		this.gasPrice = null

		this.contract = null

		this.version = null

		this.requestResponse = null

		this.RESPONSE_getBroadcastsDuring = null

		this.RESPONSE_getLatestBroadcastsBy = null

		this.RESPONSE_getBroadcastDetails = null

		this.RESPONSE_getAccountData = null

		this.paymentProcessing = false


	}



	async GruPurContract() {
		console.log("Setting up GruPur contract connection")
		if (window.ethereum) {
			blockchain.web3Provider = window.ethereum;
			try {
			  // Request account access
			  await window.ethereum.enable();
			} catch (error) {
			  // User denied account access...
			  console.error("User denied account access")
			}
		  } else if (window.web3) {
			blockchain.web3Provider = window.web3.currentProvider;
		  } else {
			blockchain.web3Provider = new Web3.providers.HttpProvider(ETH_PATH)
		  }


		//if (typeof web3 !== 'undefined') {
		//	blockchain.conn = await new Web3(web3.currentProvider);
		//} else {
			
		//}

		blockchain.conn = await new Web3(blockchain.web3Provider, {
			headers: {
				Origin: "GruPur"
			}
		});

		console.log(blockchain.conn)

		web3.version.getNetwork(function (err, network) {
			console.log(err, network);

			console.log(blockchain.net)

			switch (network) {
				//case "1":
				//networkName = "Main";
				//break;
				//case "2":
				//networkName = "Morden";
				//break;
				case "3": //Ropsten
					console.log("Connected to the Ropsten testnet")

					web3.eth.getAccounts(function(error, accounts) {
						if(error) {
						  console.log(error);
						}

						console.log(accounts)
						
						//web3.eth.getBalance(accounts[0]).then(function(result){
						 //console.log( "Balance : " ,web3.utils.fromWei(result, 'ether'));
						//});

						if (accounts.length > 0) {
							blockchain.account = {
								pub: accounts[0],
								priv: null
							}
						} else {
							blockchain.account = blockchain.net.ropsten.account
						}

						blockchain.address = blockchain.net.ropsten.address

						blockchain.contract = (new blockchain.conn.eth.Contract(blockchain.net.ropsten.contract, blockchain.address.grupur)).methods
					

					   });

					break;
				//case "4":
				//networkName = "Rinkeby";
				//break;
				//case "42":
				//networkName = "Kovan";
				//break;
				default:
					notify("Please connect to the Ropsten testnet", 2);
					break;
				//networkName = "Unknown";
			}

			console.log(blockchain.contract)
		});

		
	}

	async getBroadcastDetails(_index) {
		if (blockchain.version == null) {
			setTimeout(function() {
				blockchain.getBroadcastDetails(_index)
			}, CALL_DELAY);
			return;
		}
		

		blockchain.RESPONSE_getBroadcastDetails = null
		blockchain.RESPONSE_getBroadcastDetails = await blockchain.contract.getBroadcastDetails(this.address.broadcastStorage, _index).call()
		console.log(blockchain.RESPONSE_getBroadcastDetails)
        /*blockchain.contract.getBroadcastDetails(this.address.broadcastStorage, _index, function(error, result) {
            if (error == null) {
                console.log(result)
                blockchain.RESPONSE_getBroadcastDetails = result
            } else {
                console.log(error)
            }
        })*/
	}

	async addBroadcast(_public, _message, _filename, _hash) {
		if (blockchain.version == null) {
			setTimeout(function() {
				blockchain.addBroadcast(_public, _message, _filename, _hash)
			}, CALL_DELAY);
			return;
		}

		var funcData = this.contract.addBroadcast(this.address.broadcastStorage, this.address.walletStorage, _public, (new Date().getWeekYear()), (new Date().getWeek().doubleDigitString()), new Date().toString(), _message, _filename || "", _hash || "").encodeABI();
		await this.payTransaction(funcData);
	}

	async getBroadcastsUnder(_index) {
		if (blockchain.version == null) {
			setTimeout(function() {
				blockchain.getBroadcastsUnder(_index)
			}, CALL_DELAY);
			return;
		}

		console.log(_index)
		blockchain.RESPONSE_getBroadcastsDuring = null
		blockchain.RESPONSE_getBroadcastsDuring = await blockchain.contract.getBroadcastsUnder(this.address.broadcastStorage, _index).call()
		console.log(blockchain.RESPONSE_getBroadcastsDuring)
        /*blockchain.contract.getBroadcastsUnder(this.address.broadcastStorage, _index, function(error, result) {
            if (error == null) {
                console.log(result)
                blockchain.RESPONSE_getBroadcastsDuring = result
            } else {
                console.log(error)
            }
        })*/
	}

	async getTaggedBroadcastsDuring(_year, _week, _tag) {
		if (blockchain.version == null) {
			setTimeout(function() {
				blockchain.getTaggedBroadcastsDuring(_year, _week, _tag)
			}, CALL_DELAY);
			return;
		}

		blockchain.RESPONSE_getBroadcastsDuring = null
		blockchain.RESPONSE_getBroadcastsDuring = await blockchain.contract.getTaggedBroadcastsDuring(this.address.broadcastStorage, _year, _week, _tag).call()
		console.log(blockchain.RESPONSE_getBroadcastsDuring)
        /*blockchain.contract.getTaggedBroadcastsDuring(this.address.broadcastStorage, _year, _week, _tag, function(error, result) {
            if (error == null) {
                console.log(result)
                blockchain.RESPONSE_getBroadcastsDuring = result
            } else {
                console.log(error)
            }
        })*/
	}

	async getBroadcastsDuring(_year, _week) {
		if (blockchain.version == null) {
			setTimeout(function() {
				blockchain.getBroadcastsDuring(_year, _week)
			}, CALL_DELAY);
			return;
		}

		blockchain.RESPONSE_getBroadcastsDuring = null
		blockchain.RESPONSE_getBroadcastsDuring = await blockchain.contract.getBroadcastsDuring(this.address.broadcastStorage, _year, _week).call()
		console.log(blockchain.RESPONSE_getBroadcastsDuring)
        /*blockchain.contract.getBroadcastsDuring(this.address.broadcastStorage, _year, _week, function(error, result) {
            if (error == null) {
                console.log(result)
                blockchain.RESPONSE_getBroadcastsDuring = result
            } else {
                console.log(error)
            }
        })*/
	}

	async getLatestBroadcastsBy(_public) {
		if (blockchain.version == null) {
			setTimeout(function() {
				blockchain.getLatestBroadcastsBy(_public)
			}, CALL_DELAY);
			return;
		}

		blockchain.RESPONSE_getLatestBroadcastsBy = null
		blockchain.RESPONSE_getLatestBroadcastsBy = await blockchain.contract.getLatestBroadcastsBy(this.address.broadcastStorage, _public).call()
		console.log(blockchain.RESPONSE_getLatestBroadcastsBy)
        /*blockchain.contract.getLatestBroadcastsBy(this.address.broadcastStorage, _public, function(error, result) {
            if (error == null) {
                console.log(result)
                blockchain.RESPONSE_getLatestBroadcastsBy = result
            } else {
                console.log(error)
            }
        })*/
	}

	async getRelevanceTag(_public, _index) {
		if (blockchain.version == null) {
			setTimeout(function() {
				blockchain.getRelevanceTag(_public, _index)
			}, CALL_DELAY);
			return;
		}

		blockchain.requestResponse = null
		blockchain.requestResponse = await blockchain.contract.getRelevanceTag(this.address.walletStorage, _public, _index).call()
		console.log(blockchain.requestResponse)
        /*blockchain.contract.getRelevanceTag(this.address.walletStorage, _public, _index, function(error, result) {
            if (error == null) {
                console.log(result)
                blockchain.requestResponse = result
            } else {
                console.log(error)
            }
        })*/
	}

	async getRelevanceBalance(_public) {
		if (blockchain.version == null) {
			setTimeout(function() {
				blockchain.getRelevanceBalance(_public)
			}, CALL_DELAY);
			return;
		}

		blockchain.requestResponse = null
		blockchain.requestResponse = await blockchain.contract.getRelevanceBalance(this.address.walletStorage, _public).call()
		console.log(blockchain.requestResponse)
        /*blockchain.contract.getRelevanceBalance(this.address.walletStorage, _public, function(error, result) {
            if (error == null) {
                console.log(result)
                blockchain.requestResponse = result
            } else {
                console.log(error)
            }
        })*/
	}

	async getStatusBalance(_public) {
		if (blockchain.version == null) {
			setTimeout(function() {
				blockchain.getStatusBalance(_public)
			}, CALL_DELAY);
			return;
		}

		blockchain.requestResponse = null
		blockchain.requestResponse = await blockchain.contract.getStatusBalance(this.address.walletStorage, _public).call()
		console.log(blockchain.requestResponse)
        /*blockchain.contract.getStatusBalance(this.address.walletStorage, _public, function(error, result) {
            if (error == null) {
                console.log(result)
                blockchain.requestResponse = result.c[0]
            } else {
                console.log(error)
            }
        })*/
	}

	async saveVanity(_public, _avatar, _vanity, _bio) {
		if (blockchain.version == null) {
			setTimeout(function() {
				blockchain.saveVanity(_public, _avatar, _vanity, _bio)
			}, CALL_DELAY);
			return;
		}

		var funcData = this.contract.addVantiy(this.address.vanityStorage, _public, _avatar, _vanity, _bio).encodeABI()
		await this.payTransaction(funcData);
	}

	async getAccountData(publicKey) {
		if (blockchain.version == null) {
			setTimeout(function() {
				blockchain.getAccountData(publicKey)
			}, CALL_DELAY);
			return;
		}

		blockchain.RESPONSE_getAccountData = null
		try {
			blockchain.RESPONSE_getAccountData = await blockchain.contract.getLatestVanity(this.address.vanityStorage, publicKey).call()
		} catch (err) {
			blockchain.RESPONSE_getAccountData = [publicKey, "", "", ""]
		}
		console.log(blockchain.RESPONSE_getAccountData)
        /*blockchain.contract.getLatestVanity(this.address.vanityStorage, publicKey, function(error, result) {
            if (error == null) {
                console.log(result)
                blockchain.RESPONSE_getAccountData = result
            } else {
                blockchain.RESPONSE_getAccountData = [publicKey,"","",""]
                console.log(error)
            }
        })*/
	}

	async getVersion() {
		if (blockchain.contract == null) {
			console.log("Contract null")
			setTimeout(blockchain.getVersion, CALL_DELAY);
			return;
		}

		try {
			console.log("Trying request for contract")
			var ver = await blockchain.contract.getVersion().call()
			blockchain.version = ver
		} catch (err) {
			console.log(err.message)
			console.log("Delaying request for contract")
			setTimeout(blockchain.getVersion, CALL_DELAY);
		}

	}

	async accountId(username) {
		if (blockchain.version == null) {
			setTimeout(function() {
				blockchain.accountId(username)
			}, CALL_DELAY);
			return;
		}

		blockchain.requestResponse = null
		blockchain.requestResponse = await blockchain.contract.getCredentialId(this.address.credentialStorage, username).call()
		console.log(blockchain.requestResponse)
        /*blockchain.contract.getCredentialId(this.address.credentialStorage, username, function(error, result) {
            if (error == null) {
                console.log(result)
                blockchain.requestResponse = result.c[0]
            } else {
                console.log(error)
            }
        })*/
	}

	async accountPublicKey(username) {
		if (blockchain.version == null) {
			setTimeout(function() {
				blockchain.accountPublicKey(username)
			}, CALL_DELAY);
			return;
		}

		blockchain.requestResponse = null
		blockchain.requestResponse = await blockchain.contract.getCredentialPublicKey(this.address.credentialStorage, username).call()
		console.log(blockchain.requestResponse)
        /*blockchain.contract.getCredentialPublicKey(this.address.credentialStorage, username, function(error, result) {
            if (error == null) {
                console.log(result)
                blockchain.requestResponse = result
            } else {
                console.log(error)
            }
        })*/
	}

	async accountPrivateKey(username) {
		if (blockchain.version == null) {
			setTimeout(function() {
				blockchain.accountPrivateKey(username)
			}, CALL_DELAY);
			return;
		}

		blockchain.requestResponse = null
		blockchain.requestResponse = await blockchain.contract.getCredentialPrivateKey(this.address.credentialStorage, username).call()
		console.log(blockchain.requestResponse)
        /*blockchain.contract.getCredentialPrivateKey(this.address.credentialStorage, username, function(error, result) {
            if (error == null) {
                console.log(result)
                blockchain.requestResponse = result
            } else {
                console.log(error)
            }
        })*/
	}

	async registerCredential(username, pub, priv, rev) {
		if (blockchain.version == null) {
			setTimeout(function() {
				blockchain.registerCredential(username, pub, priv, rev)
			}, CALL_DELAY);
			return;
		}

		console.log("Sending transaction")
		var funcData = this.contract.registerCredential(this.address.credentialStorage, this.address.walletStorage, username, pub, priv, rev).encodeABI()

		this.payTransaction(funcData)

	}

	async waitForFinish() {
		if (blockchain.paymentProcessing) {
			notify("Your transaction is processing...")
			setTimeout(blockchain.waitForFinish, CALL_DELAY * 10)
		} else {
			notify("Transaction complete", 0)
		}
	}

	async payTransaction(funcData) {
		if (blockchain.version == null) {
			setTimeout(function() {
				blockchain.payTransaction(funcData)
			}, CALL_DELAY);
			return;
		}

		blockchain.paymentProcessing = true

		blockchain.waitForFinish()
		blockchain.conn.eth.getTransactionCount(blockchain.account.pub, async function (err, nonce) {

			
			let txParams = {
				nonce: nonce,
				gasPrice: blockchain.conn.utils.toHex(blockchain.gasPrice),
				gasLimit: 3141592,
				to: blockchain.address.grupur,
				from: blockchain.account.pub,
				value: 0,
				data: funcData
			}

			if (blockchain.account.priv != null) {
				let privateKey = new ethereumjs.Buffer.Buffer(blockchain.account.priv, 'hex')

				let tx = new ethereumjs.Tx(txParams)
				tx.sign(privateKey)
				let serializedTx = '0x' + tx.serialize().toString('hex')

				blockchain.conn.eth.sendSignedTransaction(serializedTx, function (error, result) {
					blockchain.paymentProcessing = false
					if (error == null) {
						console.log(result)
					} else {
						console.log(error)
					}
				})
			} else {
				blockchain.conn.eth.sendTransaction(txParams, function(error, result){
					blockchain.paymentProcessing = false
					if(error){
						console.log(error)
					}
					else{
						console.log(result)
					}
				 });
			}



		})
	}


	async registerAccount(username, password) {
		if (blockchain.version == null) {
			setTimeout(function() {
				blockchain.registerAccount(username, password)
			}, CALL_DELAY);
			return;
		}

		var username = username;

		var options = {
			userIds: [{ username: username }], // multiple user IDs
			rsaBits: 4096,                                            // RSA key size
			passphrase: password         // protects the private key
		};

		openpgp.generateKey(options).then(async function (key) {
			var privkey = key.privateKeyArmored; // '-----BEGIN PGP PRIVATE KEY BLOCK ... '
			var pubkey = key.publicKeyArmored;   // '-----BEGIN PGP PUBLIC KEY BLOCK ... '
			var revocationCertificate = key.revocationCertificate; // '-----BEGIN PGP PUBLIC KEY BLOCK ... '
			console.log("Key Generated")

			// Save public key to IPFS
			await filesystem.files.write("/keys/pubKey", new Blob([pubkey]), { create: true, parents: true })
			// Save privkey to IPFS
			await filesystem.files.write("/keys/privKey", new Blob([privkey]), { create: true, parents: true })
			// Save revocation cert to IPFS
			await filesystem.files.write("/keys/revCert", new Blob([revocationCertificate]), { create: true, parents: true })
			// Save public key ipfs hash to blockchain
			var pubHash = await filesystem.files.stat("/keys/pubKey")
			//console.log(pubHash)
			// Save privkey ipfs hash to blockchain
			var privHash = await filesystem.files.stat("/keys/privKey")
			//console.log(privHash)
			// Save recovation cert hash to blockchain
			var revHash = await filesystem.files.stat("/keys/revCert")

			await filesystem.pin.add("/ipfs/" + pubHash.hash)
			await filesystem.pin.add("/ipfs/" + privHash.hash)
			await filesystem.pin.add("/ipfs/" + revHash.hash)
			//console.log(revHash)

			console.log("Registering " + username)
			blockchain.registerCredential(username, pubHash.hash, privHash.hash, revHash.hash);
		});
	}
}


console.log("Objects loaded")