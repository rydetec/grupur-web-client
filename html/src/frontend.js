/* 
Filename: frontend.js
Description: This is the first and only file called by the HTML code. This is meant to be the GruPur bootloader that will call all other initial platform scripts. All HTML code really needs to be wrapped into this file as javascript objects and abstracted for modular use throughout the rest of the platform.
*/

// Cache busting versioning for javascript loading
const APP_VERSION = function () {
    var x = Math.sin((new Date()).getMilliseconds()) * 10000;
    x = x - Math.floor(x);
    return "0.0.2a1b" + x 
} ()

// Get parameters in url like ?parm=this&param2=that
var urlParams;
(window.onpopstate = function () {
    var match,
        pl     = /\+/g,  // Regex for replacing addition symbol with a space
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
        query  = window.location.search.substring(1);

    urlParams = {};
    while (match = search.exec(query))
       urlParams[decode(match[1])] = decode(match[2]);
})();

// The Scene is meant to control the meta properties of the current scene within the app
class Scene {
    constructor() {
        this.document = document
        this.address = this.document.location.href;
        this.header = new Header();
        this.body = null;
    }
    
    getName() {
        let name = this.address.substring(0, (this.address.indexOf("#") == -1) ? this.address.length : this.address.indexOf("#"))
        name = name.substring(0, (name.indexOf("?") == -1) ? name.length : name.indexOf("?"))
        name = name.substring(name.lastIndexOf("/") + 1, name.length)
        return name
    }
    
    startBody() {
        this.body = new Body();
    }

}

// The Header is responsible for the HTML code within the header of the file
class Header {
    constructor() {
        this.document = document
        this.element = this.document.getElementsByTagName('head')[0]
    }
    
    addMeta() {
        let viewportElement = this.document.createElement("meta");
        viewportElement.name = "viewport"
        viewportElement.content = "width=device-width, initial-scale=1.0"
        
        this.element.appendChild(viewportElement)
        
    }
    
    setTitle(title) {
        let t = this.document.getElementById("page-title")
        if (!t) {
            t = this.document.createElement("title");
            t.id = "page-title"
        }
        t.innerHTML = title
        
        this.element.appendChild(t)
    }
    
    addLink(rel, href, type) {
        var linkElement = scene.document.createElement("link")
        linkElement.rel = rel
        linkElement.href = href
        linkElement.type = type
        this.element.appendChild(linkElement)
    }
    
    addStyle(filepath) {
        var linkElement = scene.document.createElement("link")
        linkElement.rel = "stylesheet"
        linkElement.type = "text/css"
        linkElement.href = filepath + "?version=" + APP_VERSION
        this.element.appendChild(linkElement)
    }
    
    addScript(filepath) {
        var script = this.document.createElement("script")
        script.src = filepath + "?version=" + APP_VERSION
        script.async=false 
        script.defer=false
        this.element.appendChild(script)
    }

    addRemoteScript(url) {
        var script = this.document.createElement("script")
        script.src = url
        script.async=false 
        script.defer=false
        this.element.appendChild(script)
    }
    
    addTheme() {
        this.addLink("icon", "./media/favicon.ico", "image/x-icon")
        this.addLink("shortcut icon", "./media/favicon.ico", "image/x-icon")
        this.addLink("apple-touch-icon", "./media/apple-icon.png", "image/x-icon")
        this.addStyle("styles/3rd-party/font-awesome.min.css")
        this.addStyle("styles/styles.css")
        this.addStyle("styles/popover.css")
    }
    
    add3rdPartyScripts() {
        this.addRemoteScript("https://www.googletagmanager.com/gtag/js?id=G-HWSG97TNMP")
        this.addScript("src/3rd-party/gtag.js")
        this.addScript("src/3rd-party/ipfs.min.js")
        this.addScript("src/3rd-party/web3.min.js")
        this.addScript("src/3rd-party/openpgp.min.js")
        this.addScript("src/3rd-party/eth-tx.min.js")
        this.addScript("src/3rd-party/jquery.min.js")
        this.addScript("src/3rd-party/jsbip39.js")
        this.addScript("src/3rd-party/sjcl-bip39.js")
        this.addScript("src/3rd-party/wordlist_english.js")
        this.addScript("src/3rd-party/bootstrap.min.js")
        //this.addScript("src/3rd-party/webtorrent.min.js")
        this.addScript("src/3rd-party/nude/compressed/nude.min.js")
        this.addScript("src/3rd-party/notify.min.js")
    }
    
}

// The Body is responsible for the HTML code which is put in the body of the file.
class Body {
    constructor() {
        this.document = document
        this.element = this.document.getElementsByTagName('body')[0]
        this.element.innerHTML = ""
    }
    
    createDiv(divId, divClass) {
        let div = this.document.createElement("div")
        div.id = divId
        div.className = divClass
        return div
    }
    
    createImg(imgId, imgClass) {
        let div = this.document.createElement("img")
        div.id = imgId
        div.className = imgClass
        return div
    }
}

// This code is ran when the frontend.js is called as the header is constructed.
let scene = new Scene()

scene.header.addMeta()
scene.header.addTheme()
scene.header.add3rdPartyScripts()

// This function is called once the body of the file has been loaded.
async function buildBody() {
    scene.startBody()
    
    let modal = scene.body.createDiv("myModal", "modal")
    modal.appendChild(scene.body.createImg("img01","modal-content"))
    modal.appendChild(scene.body.createDiv("caption",""))
    scene.body.element.appendChild(modal)
    
    
    let header = scene.body.createDiv("top-header", "header")
    header.innerHTML = "<a href=\"./index.html\"><div class=\"header-title\" id=\"header-title\"><h2>GruPur</h2></div></a><div class=\"header-login\" id=\"header-options\">"
    scene.body.element.appendChild(header)
    
    let content = scene.body.createDiv("main-content", "content")
    
    switch (scene.getName()) {
        case "feed.html":
            content.innerHTML = "<div class=\"broadcast-area\" id=\"main-broadcast\"></div><div style=\"float:left;width:25vw\"><p></p></div><div style=\"float:right;width:25vw\"><p></p></div><div class=\"left-area\" id=\"feed\"></div><div class=\"right-area\" id=\"options\"></div>"
            scene.header.setTitle("GruPur | Global Feed")
            break;
        case "account.html":
            content.innerHTML = "<div class=\"broadcast-area\" id=\"main-broadcast\"></div><div class=\"account-panel\" id=\"account-panel\"></div><div style=\"float:left;width:25vw\"><p></p></div><div style=\"float:right;width:25vw\"><p></p></div><div class=\"right-area\" id=\"options\"></div><div class=\"left-area\" id=\"feed\"></div>"
            scene.header.setTitle("GruPur | Account Page")
            break;
        case "login.html":
            content.innerHTML = "<div class=\"center-area\" id=\"login-content\"><h3>Login</h3><p><form id=\"login-form\" onsubmit=\"return false;\"><input type=\"text\" name=\"username\" placeholder=\"Username\"></input></p><p><input placeholder=\"Password\" type=\"password\" name=\"password\"><p><input type=\"checkbox\" name=\"login-save\"> Remember my password <i>(Not recommended)</i></p><p><input type=\"submit\" value=\"Login\" onclick=\"doLogin();\"></p></form></p></div><h4><a class=\"register-link\" href=\"./register.html\">Register a new account</a></h4>"
            scene.header.setTitle("GruPur | Login")
            break;
        case "register.html":
            content.innerHTML = "<div class=\"center-area\"><h5>Register</h5><p>To register an account, create a username and password below.</p><p>If you lose your password, there is no way possible to retrieve it. <a href=\"./why.html\">Why?</a></p><p><input type=\"text\" id=\"username\" placeholder=\"Username\"></input></p><p><input type=\"password\" id=\"password\" placeholder=\"Password\"></input></p><p><button onclick=\"doRegister()\">Register</button></p><div id=\"afterRegister\"></div></div>"
            scene.header.setTitle("GruPur | Register")
            break
        case "mature.html":
            content.innerHTML = "<div class=\"center-area-wide\" style=\"padding: 10px;\"><h1>GRUPUR PLATFORM DISCLAIMER</h1><h2>Please read and agree to all statements below</h2><h3>GruPur is an uncensored public social hub. GruPur does not take any responsibility for user submitted content and does not host any media on servers by utilizing a decentralized combination of IPFS and Ethereum. Linked media and websites may contain mature content including profanity, gore, nudity, drugs, weapons and other adult topics. You must be at least 18 years old or older to enter GruPur.com. Please verify that you are of age to use our site.</h3><h3>The GruPur platform uses cookies where necessary to enhance your experience. <a href=\"https://bitbucket.org/rydetec/grupur-web-client/src\" >You can view our source code here.</a> Please verify that you agree to the use of cookies on our site.</h3><p><button onclick=\"doVerify()\">I agree to all statements.</button></p></div>"
            scene.header.setTitle("GruPur | Verify Age")
            break;
        case "why.html":
            content.innerHTML = "<div class=\"center-area-wide\"><p><i>GruPur is a free open-source decentralized anonymous social media network.</i></p><h2>...but why so extra?</h2><p>At the turn of the 21st century, when the internet was rapidly growing and becoming more personal we were constantly told \"Don't use your real name!\" and \"Don't use your real number!\".</p><p>This was the <b>#1</b> rule of the internet, until social media came around, then it became \"okay\" to upload every personal detail of yourself to someone else's server.</p><br><p>\"Yeah so if you ever need info about anyone at Harvard, I have over 4,000 emails, pictures, and addresses. People just submitted it. I don't know why. They 'trust me' Dumb fucks\" - <b>Mark Zuckerberg, CEO and Founder of Facebook</b></p><br><p>With the <a href=\"https://en.wikipedia.org/wiki/Facebook%E2%80%93Cambridge_Analytica_data_scandal\">Cambridge Analytica scandal</a>, we learned that Facebook was actively sabatoging their own software to allow companies to get access to individual's private data <u>without their knowledge or consent.</u></p><p>Every single thing you do on these social media sites is tracked and sold to companies so they can better figure out how to make you buy things from them. Cambridge Analytica was only one company, from one website, that just so happened to get caught.</p><br><h3><a href=\"https://thestack.com/security/2017/09/04/up-to-six-million-instagram-accounts-affected-by-data-breach/\">Up to six million Instagram accounts affected by data breach</a></h3><h3><a href=\"https://channels.theinnovationenterprise.com/articles/google-s-second-data-leak-impacts-52-million-users\">Google's Second Data Leak Impacts 52 Million Users</a></h3><h3><a href=\"https://www.pbs.org/newshour/nation/46-million-snapchat-users-information-leaked\">Snapchat says ‘sorry’ after 4.6 million users’ data was leaked</a></h3><h3><a href=\"https://www.recode.net/2018/4/11/17177842/facebook-advertising-ads-explained-mark-zuckerberg\">This is how Facebook uses your data for ad targeting</a></h3><br><h2>Just imagine the things these companies are doing that we don't even know about!</h2><br><p>Without access to a website's code and database there is no bulletproof way to know that you are not being taken advantage of.</p><p>It's your data, yet you have no idea how they are collecting it, what all information they have on you, or what they plan to do with it.</p><br><h4>Data mining, which appeared around the 1990s, is the computational process to discover patterns in large datasets. <a href=\"https://www.flydata.com/blog/a-brief-history-of-data-analysis/\">This used to require huge teams of analyst</a> and years of pouring through files one at a time. With today's computers, combined with the developments in machine learning, this data can be analyzed and implemented into any kind of software within hours <a href=\"https://futurism.com/googles-machine-learning-software-has-learned-to-replicate-itself\">and make copies of itself as needed.</a></h4><br><p>At GruPur.com, everyone is individually in charge of their own data, not some big company trying to sqeeze every last dollar out of you. All of our website source code <a href=\"https://bitbucket.org/rydetec/grupur-web-client/src\">is available through git</a>, the <a href=\"https://etherscan.io/\">entire database can be accessed by anyone</a> at any time, and everyone is allowed to use that data as they please. Users host their files from their own computers with IPFS and all website processing is done in your browser and on the Ethereum blockchain.</p><br><p>You may not care about privacy, data, decentralization, blockchains or anything else but...</p><h3><u>We all hate being robbed by greedy people!</u></h3></div><h4><a class=\"register-link\" href=\"./register.html\">Register a new account</a></h4>"
            scene.header.setTitle("GruPur | Why?")
            break;
        default:
            content.innerHTML = "<div class=\"broadcast-area\" id=\"main-broadcast\"></div><div style=\"float:left;width:5vw\"><p></p></div><div style=\"float:right;width:5vw\"><p></p></div><div class=\"right-area\" id=\"options\"></div><div class=\"left-area\" id=\"feed\"></div>"
            scene.header.setTitle("GruPur")
            break;
    }
    
    
    
    scene.body.element.appendChild(content)

    scene.header.addScript("src/globals.js")
    scene.header.addScript("src/mods.js")
    scene.header.addScript("src/functions.js")
    scene.header.addScript("src/objects.js")
    scene.header.addScript("src/grupur.js")

    switch (scene.getName()) {
        case "feed.html":
            //scene.header.addStyle("styles/feed.css")
            scene.header.addScript("src/feed.js")
            break;
        case "account.html":
            scene.header.addStyle("styles/account.css")
            scene.header.addScript("src/account.js")
            break;
        case "login.html":
            scene.header.addStyle("styles/login.css")
            scene.header.addScript("src/login.js")
            break;
        case "register.html":
            //scene.header.addStyle("styles/register.css")
            scene.header.addScript("src/register.js")
            break;
        case "mature.html":
            scene.header.addScript("src/mature.js")
            break;
        case "why.html":
            break;
        default:
            scene.header.addStyle("styles/index.css")
            scene.header.addScript("src/index.js")
            break;
    }
    
    scene.body.element.appendChild(scene.body.createDiv("bottom-footer","footer"))

    console.log("Welcome to " + scene.getName() + " version: " + APP_VERSION)
}




