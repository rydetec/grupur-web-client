var headerOptions = document.getElementById("header-options");
//const conn = new BigchainDB.Connection(API_PATH);

var requestDate = new Date();
if (urlParams["week"]) {
    requestDate = new Date().setWeek(urlParams["week"].substr(5,2),urlParams["week"].substr(0,4))
}

start_broadcast_service();
start_image_service();





/*function cacheAssets(assets) {
    for (a = 0; a < assets.length; a++) {
        var asset = assets[a];
        
        
        cachedAssets.unshift(asset);
    }
    
    readyToLoad = true;
    loadNextPosts();
    
    return
    
    var assets = passedAssets
    conn.searchMetadata("\"" + APP_ID + "\"and=\"week-" + requestDate.getWeek().doubleDigitString() + "\"and=\"year-" + requestDate.getWeekYear() + "\"") .then(meta => { 
        for (a = 0; a < assets.length; a++) {
            var asset = assets[a];
            
            if (asset.data.appId != APP_ID) {
                continue;
            }

            // Parse every cached message before caching it. 
            if (asset.data.type === ASSET_BROADCAST) {
                for (m = meta.length - 1; m >= 0; m--) {
                    var tran = meta[m];
                    
                    if (asset.id == tran.id) {
                        if (tran.metadata.message) {
                            parseRawMessage(tran.metadata.message);
                        }
                        break;
                    }
                }
            }

            cachedAssets.unshift(asset);
        }
        
        
        readyToLoad = true;
        loadNextPosts();
        document.getElementById("loading-feed").style = "display: none;"
    })
    
}*/

/*function loadNextPosts() {
   
    var nextAssets = []
    console.log("Cached: " + cachedAssets.length)
    for (c = 0; c < 10; c++) {
        var nextA = cachedAssets.shift();
        if (nextA) {
            lastAsset = nextA;
            nextAssets.unshift(nextA);
        }
    }
    //alert("Buffer: " + nextAssets.length)
    //alert(nextAssets[0].id)
    
    if (nextAssets.length == 0) {
        readyToLoad = false;
        var left = document.createElement("div");
        left.innerHTML = "<a id=\"last-week-bottom\" href=\"./feed.html?week=" + (new Date(requestDate.lastWeek()).getWeekString()) + "\">Last Week</a>";
        var right = null
        
        if ((new Date(requestDate).getWeekString()) != (new Date().getWeekString())) {
            right = true
            left.innerHTML += " | <a id=\"next-week-bottom\" href=\"./feed.html?week=" + (new Date(requestDate.nextWeek()).getWeekString()) + "\">Next Week</a>"
        }
        
        var old_boot = document.getElementById("feed-boot");
        if (old_boot) {
            old_boot.style = "display: none;"
        }
        
        var boot = document.createElement("div");
        boot.id = "feed-boot"
        boot.innerHTML = "<a class=\"a-light\" href=\"#\"><h2>No more posts!</h2><p>/\\ Click here to scroll to the top /\\</p></a>"
        boot.appendChild(left);
        //if (right) {
            //boot.appendChild(right);
        //}
        
        feed.appendChild(boot);
    } else {
        try {
            setAssets(nextAssets)
        } catch(err) {
            console.log(err.message)
        }
        readyToLoad = true;
        
    }
}*/

function setWeekControls() {
    return
    //conn.searchAssets("\"" + APP_ID + "\"and=\"week-" + requestDate.getWeek().doubleDigitString() + "\"and=\"year-" + requestDate.getWeekYear() + "\"") .then(assets => {
        //assets = assets.filter(asset => asset.data.appId === APP_ID)
        conn.searchAssets("\"" + APP_ID + "\"and=\"week-" + requestDate.lastWeek().getWeek().doubleDigitString() + "\"and=\"year-" + requestDate.lastWeek().getWeekYear() + "\"") .then(assets => {
            assets = assets.filter(asset => asset.data.appId === APP_ID)
            var posts = 0;
            for (i = 0; i < assets.length; i++) {
                if (assets[i].data.type === ASSET_BROADCAST) {
                    posts += 1;
                }
            }
            //alert(posts)
            var ele = document.getElementById("last-week");
            ele.innerHTML = "(" + posts + ") Last Week";
        })
        
        if ((new Date(requestDate).getWeekString()) != (new Date().getWeekString())) {
                conn.searchAssets("\"" + APP_ID + "\"and=\"week-" + requestDate.nextWeek().getWeek().doubleDigitString() + "\"and=\"year-" + requestDate.nextWeek().getWeekYear() + "\"") .then(assets => {
                    assets = assets.filter(asset => asset.data.appId === APP_ID)
                    var posts = 0;
                    for (i = 0; i < assets.length; i++) {
                        if (assets[i].data.type === ASSET_BROADCAST) {
                            posts += 1;
                        }
                    }
                    //alert(posts)
                    var ele = document.getElementById("next-week");
                    ele.innerHTML = "Next Week (" + posts + ")";
            })
        }
    //})
}

window.onscroll = function(ev) {
    react();
    
    if (lastAsset) {
        var ele = document.getElementById("spacer-" + lastAsset.id);
        if (ele != null) {
            if (isElementInViewport(ele)) {
                if (readyToLoad) {
                    readyToLoad = false;
                    loadNextPosts();
                }
            }
        } else {
            if (readyToLoad) {
                readyToLoad = false;
                loadNextPosts();
            }
        }
    }
}

if (urlParams["post"]) {
    if (readCookie("publicKey")) {
        toggleBroadcastArea()
    }
    
    var optionsElement = document.createElement("div");
            optionsElement.id = "relevant-tags";
            optionsElement.className = "optionsElement";

            options.appendChild(optionsElement);
    
    
    blockchain.getBroadcastsUnder(urlParams["post"]);
    
    function checkBroadcasts() {
        if (blockchain.RESPONSE_getBroadcastsDuring != null) {
            var broadcastIds = blockchain.RESPONSE_getBroadcastsDuring
                console.log("Broadcasts here")
                console.log(broadcastIds)
                var trueIds = []
                trueIds.push(urlParams["post"])
                for (i = 0; i < broadcastIds.length; i++) {
                    if (broadcastIds[i] > 0) {
                        trueIds.push(broadcastIds[i])
                    }
                }
                cacheAssets(trueIds);
                readyToLoad = true;
                loadNextPosts();
        } else {
            setTimeout(checkBroadcasts, CALL_DELAY)
        }
    }
    
    checkBroadcasts()
    
} 
else if (urlParams["tag"]) {
    scene.header.setTitle("GruPur | #" + urlParams["tag"] + " Feed")
    
    if (!document.getElementById("loading-element")) {
        var loadingElement = document.createElement("div");
        loadingElement.id = "loading-element"
        loadingElement.className = "optionsElement";
        loadingElement.innerHTML = "<center><i style=\"padding: 15px;\" class=\"fa fa-refresh fa-spin fa-3x fa-fw\"></i></center>"
        options.appendChild(loadingElement)
    } else {
        document.getElementById("loading-element").style = "display: block;"
    }
    
    var optionsElement = document.createElement("div");
            optionsElement.id = "relevant-tags";
            optionsElement.className = "optionsElement";

            options.appendChild(optionsElement);
    
    var relevanceElement = document.createElement("div");
            relevanceElement.id = "relevance";
            relevanceElement.className = "top-bar";
            relevanceElement.innerHTML = "<h2 class=\"top-bar-line\">#" + urlParams["tag"] + " " + parseRawMessage("#" + (new Date(requestDate).getWeekString())) + "</h2>";

            relevanceElement.innerHTML += "<p class=\"top-bar-line\">" + (new Date(requestDate).getWeekRangeString()) + "</p>"
            var HTML = "<p class=\"top-bar-line\"><a id=\"last-week\" href=\"./feed.html?tag=" + urlParams["tag"] + "&week=" + (new Date(requestDate.lastWeek()).getWeekString()) + "\"> Last Week</a>"

            if ((new Date(requestDate).getWeekString()) != (new Date().getWeekString())) {
                HTML += " | <a id=\"next-week\" href=\"./feed.html?tag=" + urlParams["tag"] + "&week=" + (new Date(requestDate.nextWeek()).getWeekString()) + "\">Next Week </a>"
            }

            relevanceElement.innerHTML += HTML + "</p>"
    
        filterTag(relevanceElement)
    
        blockchain.getTaggedBroadcastsDuring(requestDate.getWeekYear(), requestDate.getWeek().doubleDigitString(),urlParams["tag"])
        
        function checkBroadcasts() {
            if (blockchain.RESPONSE_getBroadcastsDuring != null) {
                var broadcastIds = blockchain.RESPONSE_getBroadcastsDuring
                console.log("Broadcasts here")
                console.log(broadcastIds)
                var trueIds = []
                for (i = 0; i < broadcastIds.length; i++) {
                    if (broadcastIds[i] > 0) {
                        trueIds.push(broadcastIds[i])
                    }
                }
                cacheAssets(trueIds);
                readyToLoad = true;
                loadNextPosts();
            } else {
                setTimeout(checkBroadcasts, CALL_DELAY)
            }
        }
        
        checkBroadcasts()
    
} 
else {
    
    var optionsElement = document.createElement("div");
            optionsElement.id = "relevant-tag";
            optionsElement.className = "optionsElement";
            optionsElement.innerHTML = "<div style=\"font-size:20px\"><a class=\"a-light\" href=\'?tag=Relevant\'><b>#Relevant</b></a></div><div id=\"relevant-tags\"></div>"

            options.appendChild(optionsElement);
    
    optionsElement = document.createElement("div");
            optionsElement.id = "pics-tag";
            optionsElement.className = "optionsElement";
            optionsElement.innerHTML = "<div style=\"font-size:20px\"><a class=\"a-light\" href=\'?tag=Pics\'><b>#Pics</b></a></div><div><a href=\'?tag=gif\'>#Gif</a> <a href=\'?tag=cute\'>#Cute</a> <a href=\'?tag=selfie\'>#Selfie</a> <a href=\'?tag=Meme\'>#Meme</a> <a href=\'?tag=Funny\'>#Funny</a> <a href=\'?tag=Dogs\'>#Dogs</a> <a href=\'?tag=Cats\'>#Cats</a> <a href=\'?tag=Pets\'>#Pets</a> <a href=\'?tag=Anime\'>#Anime</a> <a href=\'?tag=Random\'>#Random</a></div>";

            options.appendChild(optionsElement);
    
    optionsElement = document.createElement("div");
            optionsElement.id = "active-tag";
            optionsElement.className = "optionsElement";
            optionsElement.innerHTML = "<div style=\"font-size:20px\"><a class=\"a-light\" href=\'?tag=Activisim\'><b>#Activisim</b></a></div><div><a href=\'?tag=LGBTQ\'>#LGBTQ</a> <a href=\'?tag=MeToo\'>#MeToo</a> <a href=\'?tag=SlutPride\'>#SlutPride</a> <a href=\'?tag=Pride\'>#Pride</a> <a href=\'?tag=CivilRights\'>#CivilRights</a> <a href=\'?tag=HumanRights\'>#HumanRights</a> <a href=\'?tag=Climate\'>#Climate</a> <a href=\'?tag=GunDebate\'>#GunDebate</a></div>";

            options.appendChild(optionsElement);
   
    optionsElement = document.createElement("div");
            optionsElement.id = "news-tag";
            optionsElement.className = "optionsElement";
            optionsElement.innerHTML = "<div style=\"font-size:20px\"><a class=\"a-light\" href=\'?tag=News\'><b>#News</b></a></div><div><a href=\'?tag=breakingnews\'>#BreakingNews</a> <a href=\'?tag=USA\'>#USA</a> <a href=\'?tag=politics\'>#Politics</a> <a href=\'?tag=VoteLeft\'>#VoteLeft</a> <a href=\'?tag=VoteRight\'>#VoteRight</a></div>";

            options.appendChild(optionsElement);
    
    optionsElement = document.createElement("div");
            optionsElement.id = "exchange-tag";
            optionsElement.className = "optionsElement";
            optionsElement.innerHTML = "<div style=\"font-size:20px\"><a class=\"a-light\" href=\'?tag=Finance\'><b>#Finance</b></a></div><div><a href=\'?tag=Stocks\'>#Stocks</a> <a href=\'?tag=Buy\'>#Buy</a> <a href=\'?tag=Sell\'>#Sell</a> <a href=\'?tag=Trade\'>#Trade</a> <a href=\'?tag=Budget\'>#Budget</a></div>";

            options.appendChild(optionsElement);
    
    optionsElement = document.createElement("div");
            optionsElement.id = "chat-tag";
            optionsElement.className = "optionsElement";
            optionsElement.innerHTML = "<div style=\"font-size:20px\"><a class=\"a-light\" href=\'?tag=Chat\'><b>#Chat</b></a></div><div><a href=\'?tag=Cars\'>#Cars</a> <a href=\'?tag=Request\'>#Request</a> <a href=\'?tag=Torrents\'>#Torrents</a> <a href=\'?tag=Games\'>#Games</a> <a href=\'?tag=Wallpaper\'>#Wallpaper</a> <a href=\'?tag=Retro\'>#Retro</a> <a href=\'?tag=Movies\'>#Movies</a> <a href=\'?tag=TV\'>#TV</a> <a href=\'?tag=TrueCrime\'>#TrueCrime</a> <a href=\'?tag=Fun\'>#Fun</a></div>";

            options.appendChild(optionsElement);
    
    optionsElement = document.createElement("div");
            optionsElement.id = "world-tag";
            optionsElement.className = "optionsElement";
            optionsElement.innerHTML = "<div style=\"font-size:20px\"><a class=\"a-light\" href=\'?tag=World\'><b>#World</b></a></div><div><a href=\'?tag=WorldNews\'>#WorldNews</a> <a href=\'?tag=Europe\'>#Europe</a> <a href=\'?tag=Africa\'>#Africa</a> <a href=\'?tag=MiddleEast\'>#MiddleEast</a> <a href=\'?tag=SouthAmerica\'>#SouthAmerica</a></div> <a href=\'?tag=LatinAmerica\'>#LatinAmerica</a></div> <a href=\'?tag=Canada\'>#Canada</a></div> <a href=\'?tag=Aussie\'>#Aussie</a></div> <a href=\'?tag=Asia\'>#Asia</a></div> <a href=\'?tag=China\'>#China</a></div> <a href=\'?tag=Korea\'>#Korea</a></div> <a href=\'?tag=India\'>#India</a></div> <a href=\'?tag=Japan\'>#Japan</a></div> <a href=\'?tag=Russia\'>#Russia</a></div>";

            options.appendChild(optionsElement);

    optionsElement = document.createElement("div");
            optionsElement.id = "grupur-tag";
            optionsElement.className = "optionsElement";
            optionsElement.innerHTML = "<div style=\"font-size:20px\"><a class=\"a-light\" href=\'?tag=GruPur\'><b>#GruPur</b></a></div><div><a href=\'?tag=bug\'>#Bug</a> <a href=\'?tag=source\'>#Source</a></div> <div><a href=\'?tag=GruPurDev\'>#GruPurDev</a></div> <div><a href=\'?tag=Decentralized\'>#Decentralized</a> <a href=\'?tag=IPFS\'>#IPFS</a> <a href=\'?tag=Ethereum\'>#Ethereum</a></div>";

            options.appendChild(optionsElement);
    
    //optionsElement = document.createElement("div");
            //optionsElement.id = "mature-tag";
            //optionsElement.className = "optionsElement";
            //var HTML = "<div style=\"font-size:20px\"><a class=\"a-light\" href=\'?tag=Mature\'><b>#Mature</b></a></div>";
            //HTML += "<br><div style=\"font-size:18px\"><a class=\"a-light\"href=\'?tag=Porn\'>#Porn</a></div><div><a href=\'?tag=NSFW\'>#NSFW</a> <a href=\'?tag=Sex\'>#Sex</a> <a href=\'?tag=GirlsGoneWild\'>#GirlsGoneWild</a> <a href=\'?tag=8teen\'>#8teen</a> <a href=\'?tag=Nudity\'>#Nudity</a> <a href=\'?tag=QoS\'>#QoS</a> <a href=\'?tag=CamGirl\'>#CamGirl</a> <a href=\'?tag=SheFucks\'>#SheFucks</a></div>"
            //HTML += "<br><div style=\"font-size:18px\"><a class=\"a-light\"href=\'?tag=Gore\'>#Gore</a></div><div><a href=\'?tag=NSFL\'>#NSFL</a> <a href=\'?tag=Death\'>#Death</a> <a href=\'?tag=Suicide\'>#Suicide</a> <a href=\'?tag=1000WaysToDie\'>#1000WaysToDie</a> <a href=\'?tag=DarwinAward\'>#DarwinAward</a> <a href=\'?tag=StayingAlive\'>#StayingAlive</a> <a href=\'?tag=Weapons\'>#Weapons</a></div>"
            //HTML += "<br><div style=\"font-size:18px\"><a class=\"a-light\"href=\'?tag=Personals\'>#Personals</a></div><div><a href=\'?tag=DTF\'>#DTF</a> <a href=\'?tag=SeekMale\'>#SeekMale</a> <a href=\'?tag=SeekFemale\'>#SeekFemale</a></div> <a href=\'?tag=SeekTrans\'>#SeekTrans</a></div> <a href=\'?tag=SeekGay\'>#SeekGay</a> <a href=\'?tag=SeekStraight\'>#SeekStraight</a> <a href=\'?tag=SeekBi\'>#SeekBi</a> <a href=\'?tag=NoStrings\'>#NoStrings</a> <a href=\'?tag=NeedSex\'>#NeedSex</a> <a href=\'?tag=DickYouDown\'>#DickYouDown</a> <a href=\'?tag=WithBenefits\'>#WithBenefits</a></div>"
            //optionsElement.innerHTML = HTML;

            //options.appendChild(optionsElement);

        if (readCookie("publicKey")) {
            options.appendChild(accountOptions);
        }
            
        
        
        
        var globalElement = document.createElement("div");
        globalElement.id = "global";
        globalElement.className = "top-bar";
        feed.appendChild(globalElement);
    
        var feedsElement = document.createElement("div");
        feedsElement.id = "global-feeds";
        feed.appendChild(feedsElement);
    
    
        //feed.appendChild(document.createElement("hr"));
        globalElement.innerHTML = "<h2 class=\"top-bar-line\">Global " + parseRawMessage("#" + (new Date(requestDate).getWeekString())) + "</h2>"
        
        //globalElement.innerHTML += 
        globalElement.innerHTML += "<p class=\"top-bar-line\">" + (new Date(requestDate).getWeekRangeString()) + "</p>"
        var HTML = "<p class=\"top-bar-line\"><a id=\"last-week\" href=\"./feed.html?week=" + (new Date(requestDate.lastWeek()).getWeekString()) + "\"> Last Week</a>"
        
        if ((new Date(requestDate).getWeekString()) != (new Date().getWeekString())) {
            HTML += " | <a id=\"next-week\" href=\"./feed.html?week=" + (new Date(requestDate.nextWeek()).getWeekString()) + "\">Next Week </a>"
        }
            
        globalElement.innerHTML += HTML + "</p>"
    
    if (urlParams["sort"]) {
        switch (urlParams["sort"]) {
            case "relevant":
                feedsElement.innerHTML = "<h2>Relevant | <a href=\"./feed.html?week=" + (new Date(requestDate).getWeekString()) + "&sort=top\">Top</a> | <a href=\"./feed.html?week=" + (new Date(requestDate).getWeekString()) + "&sort=new\">New</a></h2>"
                
                conn.searchAssets("\"" + APP_ID + "\"and=\"week-" + requestDate.getWeek().doubleDigitString() + "\"and=\"year-" + requestDate.getWeekYear() + "\"") .then(assets => {
                    assets = assets.filter(asset => asset.data.appId === APP_ID)
                    sortTransfer(assets, "Relevance")
                })
                break;
            case "top":
                feedsElement.innerHTML = "<h2><a href=\"./feed.html?week=" + (new Date(requestDate).getWeekString()) + "&sort=relevant\">Relevant</a> | Top | <a href=\"./feed.html?week=" + (new Date(requestDate).getWeekString()) + "&sort=new\">New</a></h2>"
                
                conn.searchAssets("\"" + APP_ID + "\"and=\"week-" + requestDate.getWeek().doubleDigitString() + "\"and=\"year-" + requestDate.getWeekYear() + "\"") .then(assets => {
                    assets = assets.filter(asset => asset.data.appId === APP_ID)
                    sortTransfer(assets, "Status")
                })
                break;
            case "new":
                feedsElement.innerHTML = "<h2><a href=\"./feed.html?week=" + (new Date(requestDate).getWeekString()) + "&sort=relevant\">Relevant</a> | <a href=\"./feed.html?week=" + (new Date(requestDate).getWeekString()) + "&sort=top\">Top</a> | New</h2>"
                
                conn.searchAssets("\"" + APP_ID + "\"and=\"week-" + requestDate.getWeek().doubleDigitString() + "\"and=\"year-" + requestDate.getWeekYear() + "\"") .then(assets => {
                    assets = assets.filter(asset => asset.data.appId === APP_ID)
                    sortNew(assets)
                })
                break;
        }
    } else {
        feedsElement.innerHTML = "<hr>"//"<h2>Relevant | <a href=\"./feed.html?week=" + (new Date(requestDate).getWeekString()) + "&sort=top\">Top</a> | <a href=\"./feed.html?week=" + (new Date(requestDate).getWeekString()) + "&sort=new\">New</a></h2>"
        
        blockchain.getBroadcastsDuring(requestDate.getWeekYear(), requestDate.getWeek().doubleDigitString())
        
        function checkBroadcasts() {
            if (blockchain.RESPONSE_getBroadcastsDuring != null) {
                var broadcastIds = blockchain.RESPONSE_getBroadcastsDuring
                console.log("Broadcasts here")
                console.log(broadcastIds)
                var trueIds = []
                for (i = 0; i < broadcastIds.length; i++) {
                    if (broadcastIds[i] > 0) {
                        trueIds.push(broadcastIds[i])
                    }
                }
                cacheAssets(trueIds);
                readyToLoad = true;
                loadNextPosts();
            } else {
                setTimeout(checkBroadcasts, CALL_DELAY)
            }
        }
        
        checkBroadcasts()
        
    }
    
        
        
        //globalElement.innerHTML += "<p>Week: " + (new Date().getWeekString()) + "</p>";
        
        //globalElement.innerHTML += "<p>Range: " + monday.getDateString() + " - " + sunday.getDateString() + "</p>";
        
    
    setWeekControls()
    
    feed.innerHTML += "<div id=\"loading-feed\"><center><i style=\"padding: 15px;\" class=\"fa fa-refresh fa-spin fa-3x fa-fw\"></i></center></div>"
}



function sortTransfer(assets, transferType) {
    
        console.log("Assets")
        var statusAssets = []
        
        conn.searchMetadata("\"" + APP_ID + "\"and=\"week-" + requestDate.getWeek().doubleDigitString() + "\"and=\"year-" + requestDate.getWeekYear() + "\"and=\"" + transferType + "\" transfer\"") . then(metas => {
            
            console.log("Metas: " + metas.length)
            for (var a = 0; a < assets.length; a++) {
                if (assets[a].data.appId != APP_ID) {
                    continue;
                }
                
                for (var m = 0; m < metas.length; m++) {
                    //console.log("Asset: " + assets[a].id + " Context: " + metas[m].metadata.context)
                    if (assets[a].id == metas[m].metadata.context) {
                        if (urlParams["tag"]) {
                            if (metas[m].metadata.tag != urlParams["tag"]) {
                                // console.log("Not same tag")
                                continue;
                            }
                        }
                        
                        var counted = false
                        for (var s = 0; s < statusAssets.length; s++) {
                            if (statusAssets[s].asset.id == assets[a].id) {
                                statusAssets[s].count += 1
                                counted = true
                            }
                        }
                        if (!counted) {
                            statusAssets.push({asset: assets[a], count: 1})
                        }
                        
                    }
                }
            }
            
            function comp(a,b) {
                if (a.count > b.count) return 1;
                if (a.count < b.count) return -1;
                
                return 0;
            }
            
            statusAssets.sort(comp)
            
            assets = sort_assets_cron(assets)
            
            for (var s = 0; s < statusAssets.length; s++) {
                console.log("Asset: " + statusAssets[s].asset.id + " Count: " + statusAssets[s].count)
                // Remove Duplicates
                assets = $.grep(assets, function(el, idx) {return el.id == statusAssets[s].asset.id}, true)
                assets.push(statusAssets[s].asset)
                console.log(assets.length)
            }
            
            cacheAssets(assets);
        })
}

function sortNew(assets) {
    cacheAssets(sort_assets_cron(assets));
}


function updateGroupDescription() {
        var previewArea = document.getElementById("groupMessage");
        var broadcastForm = document.getElementById("relevance-form");
        
        message = broadcastForm.elements[0].value;

        if (message.length > 0) {
            document.getElementById("message-area").style = "width:90%;height:150px;"
        } else {
            document.getElementById("message-area").style = "";
        }

        previewArea.innerHTML = styleMessage(parseRawMessage(message, true));
}

function filterTag(relevanceElement) {
    /*relevanceElement.innerHTML += "\
        <form id=\"relevance-form\">\
                <p><textarea id=\"message-area\" rows=4 cols=50 maxlength=240 placeholder=\"Write a relevant description...\" oninput=\"updateGroupDescription();\"></textarea></p>\
                <p><input id=\"password-area\" type=\"password\" placeholder=\"Passphrase Signature\" size=50></p>\
                </form>\
                <p><button  onclick=\"claimOwnership()\">Claim #" + urlParams["tag"] + "</button></p>"
        
        var optionsElement = document.createElement("div");
            optionsElement.id = "groupMessage";
            optionsElement.className = "optionsElement";
            optionsElement.innerHTML = "<h3 class=\"group-message\">#" + urlParams["tag"] + " is an unclaimed tag. Claim it for yourself to help maintain it's relevancy.</h3>";

            options.appendChild(optionsElement);*/
        
        document.getElementById("loading-element").style = "display: none;"
    
    
    feed.appendChild(relevanceElement);
    
    var feedsElement = document.createElement("div");
        feedsElement.id = "global-feeds";
        feed.appendChild(feedsElement);
    
    if (readCookie("privateKey")) {
        var ele = document.getElementById("password-area")
        if (ele) {
            ele.style.display = "none";
        }
    }
    
    feedsElement.innerHTML = "<hr>"//"<div id=\"loading-feed\"><center><i style=\"padding: 15px;\" class=\"fa fa-refresh fa-spin fa-3x fa-fw\"></i></center></div>"
    
    
    
    return
    
    
    var relevanceObjects = assets.filter(asset => asset.data.type === ASSET_RELEVANCE && asset.data.tag.toLowerCase() === urlParams["tag"].toLowerCase())
    
    if (relevanceObjects.length === 0) {
        relevanceElement.innerHTML += "\
        <form id=\"relevance-form\">\
                <p><textarea id=\"message-area\" rows=4 cols=50 maxlength=240 placeholder=\"Write a relevant description...\" oninput=\"updateGroupDescription();\"></textarea></p>\
                <p><input id=\"password-area\" type=\"password\" placeholder=\"Passphrase Signature\" size=50></p>\
                </form>\
                <p><button onclick=\"claimOwnership()\">Claim #" + urlParams["tag"] + "</button></p>"
        
        var optionsElement = document.createElement("div");
            optionsElement.id = "groupMessage";
            optionsElement.className = "optionsElement";
            optionsElement.innerHTML = "<h3 class=\"group-message\">#" + urlParams["tag"] + " is an unclaimed tag. Claim it for yourself to help maintain it's relevancy.</h3>";

            options.appendChild(optionsElement);
        
        document.getElementById("loading-element").style = "display: none;"
    } else {
        
        conn.listTransactions(relevanceObjects[0].id) .then(transactions => {
            
            var optionsElement = document.createElement("div");
            optionsElement.id = "dbTest";
            optionsElement.className = "optionsElement";
            optionsElement.innerHTML = "<h3 class=\"group-message\">" + parseRawMessage(transactions[0].metadata.purpose, false) + "</h3>";

            options.appendChild(optionsElement);
            
            var ownerElement = document.createElement("div");
            ownerElement.id = "dbTest";
            ownerElement.className = "optionsElement";
            ownerElement.innerHTML = "<h3 id=\"owner-area\">Founder</h3><p>" + parseRawMessage("@" + getUsername(relevanceObjects[0].data.account), false) + " (<i id=\"" + relevanceObjects[0].data.account + "\">0</i>)</p>"

            options.appendChild(ownerElement);
            
            var usersElement = document.createElement("div");
            usersElement.id = "dbTest";
            usersElement.className = "optionsElement";
            usersElement.innerHTML = "<h3>Relevant</h3><p id=\"users-area\"></p>"

            options.appendChild(usersElement);
            
            //relevanceElement.innerHTML += "<h3 class=\"group-message\">" + transactions[0].metadata.purpose + "</h3>"
            //relevanceElement.innerHTML += "<p id=\"users-area\">Relevant Users: " + "</p>"
            //relevanceElement.innerHTML += "<p id=\"owner-area\">Owner: " + parseRawMessage("@" + getUsername(relevanceObjects[0].data.account)) + " (<i id=\"" + relevanceObjects[0].data.account + "\">0</i>)</p>"
            var public_keys = [relevanceObjects[0].data.account]
            
            for (i = transactions.length - 1; i >= 0; i--) {
                for (p = 0; p < transactions[i].outputs.length; p++) {
                    public_keys.push(transactions[i].outputs[p].public_keys[0])
                }
            }
            let unique = [...new Set(public_keys)];
            for (let u = 0; u < unique.length; u++) {
                conn.listOutputs(unique[u], false) .then( output => {
                    //for (let o = output.length - 1; o >= 0; o--) {
                        //alert(1000 * o)
                        
                        function setR (output, timeout) {
                            if (output.length == 0) {
                                document.getElementById("loading-element").style = "display: none;"
                                return;
                            }
                            conn.getTransaction(output.pop().transaction_id) .then( transaction => {
                                    if ( (transaction.asset.id || transaction.id)  === relevanceObjects[0].id) {
                                        var myOutput = transaction.outputs.filter( t => t.public_keys[0] === unique[u])[0].amount
                                        if (unique[u] != relevanceObjects[0].data.account) {
                                            if (!document.getElementById(unique[u])) {
                                                document.getElementById("users-area").innerHTML += "<p>" + parseRawMessage("@" + getUsername(unique[u])) + " (<i id=\"" + unique[u] + "\">0</i>)</p>";
                                            }
                                        }
                                        //alert(parseRawMessage("@" + getUsername(unique[u])) + " (" + myOutput + ")";)
                                        var newAmount = parseInt(document.getElementById(unique[u]).innerHTML) + parseInt(myOutput);
                                        document.getElementById(unique[u]).innerHTML = newAmount;
                                    }
                                
                                setTimeout(function() {
                                    setR(output, timeout)

                                }, timeout) 
                            })
                        }
                        
                        let timeout = 100;
                        
                        setTimeout(function() {
                            setR(output, timeout)
                                
                        }, timeout)    
                    //}
                })
            }
        })
    }
    
    //feed.innerHTML += ""
    feed.appendChild(relevanceElement);
    //feed.appendChild(document.createElement("hr"));
    
    var feedsElement = document.createElement("div");
        feedsElement.id = "global-feeds";
        feed.appendChild(feedsElement);
    
    if (readCookie("privateKey")) {
        var ele = document.getElementById("password-area")
        if (ele) {
            ele.style.display = "none";
        }
    }
    
    feedsElement.innerHTML += "<div id=\"loading-feed\"><center><i style=\"padding: 15px;\" class=\"fa fa-refresh fa-spin fa-3x fa-fw\"></i></center></div>"
    
    conn.searchMetadata("\"#" + urlParams["tag"] + "\"and=\"" + APP_ID + "\"and=\"week-" + requestDate.getWeek().doubleDigitString() + "\"and=\"year-" + requestDate.getWeekYear() + "\"") .then(metas => {
        
            var metas = metas
            var filtered = []
            for (i = 0; i < assets.length; i++) {
                var check = metas.filter(meta => assets[i].id === meta.id)
                if (check.length > 0) {
                    if (check[0].metadata.message){
                        if (parseRawMessage(check[0].metadata.message.toLowerCase(), true).includes(">#" + urlParams["tag"].toLowerCase() + "<")) {
                            var doubleCheck = filtered.filter(meta => assets[i].id === meta.id);
                            if (doubleCheck.length === 0) {
                                filtered.push(assets[i])
                            }
                        }
                    }
                }
            }
            
            //cacheAssets(filtered)
        if (urlParams["sort"]) {
            switch (urlParams["sort"]) {
                case "relevant":
                    feedsElement.innerHTML = "<h2>Relevant | <a href=\"./feed.html?tag=" + urlParams["tag"] + "&week=" + (new Date(requestDate).getWeekString()) + "&sort=top\">Top</a> | <a href=\"./feed.html?tag=" + urlParams["tag"] + "&week=" + (new Date(requestDate).getWeekString()) + "&sort=new\">New</a></h2>"
                    
                    
                    feedsElement.innerHTML += "<div id=\"loading-feed\"><center><i style=\"padding: 15px;\" class=\"fa fa-refresh fa-spin fa-3x fa-fw\"></i></center></div>"
                        sortTransfer(filtered, "Relevance")
                    
                    break;
                case "top":
                    feedsElement.innerHTML = "<h2><a href=\"./feed.html?tag=" + urlParams["tag"] + "&week=" + (new Date(requestDate).getWeekString()) + "&sort=relevant\">Relevant</a> | Top | <a href=\"./feed.html?tag=" + urlParams["tag"] + "&week=" + (new Date(requestDate).getWeekString()) + "&sort=new\">New</a></h2>"

                    feedsElement.innerHTML += "<div id=\"loading-feed\"><center><i style=\"padding: 15px;\" class=\"fa fa-refresh fa-spin fa-3x fa-fw\"></i></center></div>"
                        sortTransfer(filtered, "Status")
                    
                    break;
                case "new":
                    feedsElement.innerHTML = "<h2><a href=\"./feed.html?tag=" + urlParams["tag"] + "&week=" + (new Date(requestDate).getWeekString()) + "&sort=relevant\">Relevant</a> | <a href=\"./feed.html?tag=" + urlParams["tag"] + "&week=" + (new Date(requestDate).getWeekString()) + "&sort=top\">Top</a> | New</h2>"

                    feedsElement.innerHTML += "<div id=\"loading-feed\"><center><i style=\"padding: 15px;\" class=\"fa fa-refresh fa-spin fa-3x fa-fw\"></i></center></div>"
                        sortNew(filtered)
                    
                    break;
            }
        } else {
            feedsElement.innerHTML = "<h2>Relevant | <a href=\"./feed.html?tag=" + urlParams["tag"] + "&week=" + (new Date(requestDate).getWeekString()) + "&sort=top\">Top</a> | <a href=\"./feed.html?tag=" + urlParams["tag"] + "&week=" + (new Date(requestDate).getWeekString()) + "&sort=new\">New</a></h2>"
            
            feedsElement.innerHTML += "<div id=\"loading-feed\"><center><i style=\"padding: 15px;\" class=\"fa fa-refresh fa-spin fa-3x fa-fw\"></i></center></div>"
                sortTransfer(filtered, "Relevance")
            
        }
        
        
        
            //cacheAssets(sort_assets_cron(filtered));
            //readyToLoad = true;
            //loadNextPosts();
        })
    
    conn.searchMetadata("\"#" + urlParams["tag"] + "\"and=\"" + APP_ID + "\"and=\"week-" + requestDate.lastWeek().getWeek().doubleDigitString() + "\"and=\"year-" + requestDate.lastWeek().getWeekYear() + "\"") .then(metas => {
            var ele = document.getElementById("last-week");
            ele.innerHTML = "(" + metas.length + ") Last Week";
        })
    

        
        if ((new Date(requestDate).getWeekString()) != (new Date().getWeekString())) {
                conn.searchMetadata("\"#" + urlParams["tag"] + "\"and=\"" + APP_ID + "\"and=\"week-" + requestDate.nextWeek().getWeek().doubleDigitString() + "\"and=\"year-" + requestDate.nextWeek().getWeekYear() + "\"") .then(metas => {
            var ele = document.getElementById("next-week");
            ele.innerHTML = "Next Week (" + metas.length + ")";
        })
        }
    
    
}

function claimOwnership() {
    message = document.getElementById("relevance-form").elements[0].value;
    words = null;
    if (!readCookie("privateKey")) {
        words = document.getElementById("relevance-form").elements[1].value;
    }
    
    
    if (readCookie("publicKey") != null) {
        const user = {publicKey: readCookie("publicKey"), privateKey: readCookie("privateKey")}
              
        if (!readCookie("privateKey")) {
            user = login(words);
        }
        
        if (user.publicKey == readCookie("publicKey")) {
            var relevance_amount = "240"
            
            const relevance_asset = {appId: APP_ID, year: "year-" + new Date().getWeekYear(), week: "week-" + new Date().getWeek().doubleDigitString(), type: ASSET_RELEVANCE, account: user.publicKey, tag: urlParams['tag'], timestamp: new Date().toString()};
            
            const broadcast_asset = {appId: APP_ID, year: "year-" + new Date().getWeekYear(), week: "week-" + new Date().getWeek().doubleDigitString(), type: ASSET_BROADCAST, account: user.publicKey, timestamp: new Date().toString()};
            
    
            // Create new relevance asset
            const tx = BigchainDB.Transaction.makeCreateTransaction(
                relevance_asset,
                {appId: APP_ID, year: "year-" + new Date().getWeekYear(), week: "week-" + new Date().getWeek().doubleDigitString(), type: DATA_GROUP, description: "Group creation", timestamp: new Date().toString(), purpose: message},
                [BigchainDB.Transaction.makeOutput(BigchainDB.Transaction.makeEd25519Condition(user.publicKey), relevance_amount)],
                user.publicKey
            )
            
            // Create new broadcast asset
            const btx = BigchainDB.Transaction.makeCreateTransaction(
                broadcast_asset,
                {appId: APP_ID, year: "year-" + new Date().getWeekYear(), week: "week-" + new Date().getWeek().doubleDigitString(), type: DATA_MESSAGE, description: "Message creation", timestamp: new Date().toString(), message: "#" + urlParams['tag'] + " was claimed by @" + getUsername(user.publicKey) + " for " + relevance_amount + " relevance."},
                [BigchainDB.Transaction.makeOutput(BigchainDB.Transaction.makeEd25519Condition(user.publicKey))],
                user.publicKey
            )
            

            const txSigned = BigchainDB.Transaction.signTransaction(tx, user.privateKey)
            
            const btxSigned = BigchainDB.Transaction.signTransaction(btx, user.privateKey)
            

            conn.postTransactionCommit(txSigned).then(res => {
                conn.postTransactionCommit(btxSigned).then(res => {
                    document.location = "./feed.html?tag=" + urlParams["tag"]
                }).catch(function(err) {
                    if (err["status"] == "500 INTERNAL SERVER ERROR") {
                        alert("Our database api is unresponsive. Please try again later.")
                        console.log(err)
                    } else {
                        alert("There was an error sending your broadcast! Please try again later.")
                        console.log(err)
                    }
                })
            }).catch(function(err) {
                if (err["status"] == "500 INTERNAL SERVER ERROR") {
                    alert("Our database api is unresponsive. Please try again later.")
                    console.log(err)
                } else {
                    alert("There was an error sending your broadcast! Please try again later.")
                    console.log(err)
                }
            })
            
            
            
            
        } else {
            alert("Passphrase signature invalid")
        }
    } else {
        alert("You are not logged in.")
        document.location = "./login.html"
    }
}

