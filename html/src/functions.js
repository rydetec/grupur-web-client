/*
Filename: functions.js
Description: Responsible for declaration of system functions for use within the GruPur Platform
*/

// MATH

// Return a random integer between 2 numbers including both numbers supplied
function randomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1) ) + min;
}

// SYSTEM FUNCTIONS

//Supply keyphrase and this will return KeyPair
function login(words) {
    var seed = mnemonic.toSeed(words).slice(0,32);
    var keypair = new BigchainDB.Ed25519Keypair(encoder.encode(seed));
    return keypair
}

// Erase browser stored cookies and return to index
function logout() {
    eraseCookie("publicKey");
    eraseCookie("privateKey");
    eraseCookie("password");
    //removeCookies();
    document.location = "./index.html"
}

// Return a word passphrase of specified length from login mnemonic
function register(security=3) {
    var words = null
    switch (security) {
        case 0:
            words = mnemonic.generate(32)
            break;
        case 1:
            words = mnemonic.generate(32 * 2)
            break;
        case 2:
            words = mnemonic.generate(32 * 3)
            break;
        default:
            words = mnemonic.generate()
            break;
    }
    return words
}

// Return first 10 characters of supplied publicKey
function getUsername(publicKey) {
    return publicKey.substr(publicKey.length-11,publicKey.length-1)
}

// Return filtered vanity data string
function getVanity(vanityString) {
    if (vanityString == null) {
        return ""
    }
    return vanityString.substr(7,vanityString.length - 1)
}

// Return filtered avatar data string
function getAvatar(avatarString) {
    if (avatarString == null) {
        return ""
    }
    return avatarString.substr(7,avatarString.length - 1)
}

// Return first 12 characters of the supplied asset id
function getAssetHandle(id) {
    return id.substring(0,12)
}

// Copy supplied string to clipboard
function clipboard(text) {
    var element = document.createElement('input');
    element.value = text
    document.body.appendChild(element);
    
    element.select()
    document.execCommand('copy')
    document.body.removeChild(element);
    
    console.log("Coppied text to clipboard.")
}

// Download filename with suppiled text as data
function download(filename, text) {
  var element = document.createElement('a');
  element.setAttribute('href', 'data:file;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download', filename);
  element.removeAttribute('target');

  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}

// Create browser cookie of value with expiration
function createCookie(name, value, days) {
    var expires = '',
        date = new Date();
    if (days) {
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = '; expires=' + date.toGMTString();
    }
    document.cookie = name + '=' + value + expires + '; path=/';
}

// Read browser cookie value
function readCookie(name) {
    var cookies = document.cookie.split(';'),
        length = cookies.length,
        i,
        cookie,
        nameEQ = name + '=';
    for (i = 0; i < length; i += 1) {
        cookie = cookies[i];
        while (cookie.charAt(0) === ' ') {
            cookie = cookie.substring(1, cookie.length);
        }
        if (cookie.indexOf(nameEQ) === 0) {
            return cookie.substring(nameEQ.length, cookie.length);
        }
    }
    return null;
}

// Erase browser cookie value
function eraseCookie(name) {
    createCookie(name, '', -1);
}

// Erase all cookies for GruPur
function removeCookies() {
	var res = document.cookie;
    var multiple = res.split(";");
    for(var i = 0; i < multiple.length; i++) {
    	var key = multiple[i].split("=");
        document.cookie = key[0]+" =; expires = Thu, 01 Jan 1970 00:00:00 UTC";
    }
}

// Send user notification
function notify(message, type) {
    var classType = "info"
    switch (type) {
        case 0:
            classType = "success"
            break;
        case 1:
            classType = "warn"
            break;
        case 2:
            classType = "error"
            break;
        default:
            classType = "info"
            break;
    }
    $.notify(message,{ clickToHide: true, globalPosition: "bottom right", className: classType });
    console.log(message);
}

// Escape HTML in string to avoid parsing it later
function escapeHtml(str) {
    var div = document.createElement('div');
    div.appendChild(document.createTextNode(str));
    return div.innerHTML;
}

// Return in plaintext how long a number of seconds is
function getDurationString(deltaTime) {
    var timeString = deltaTime + " seconds"
    if (deltaTime > 59) {
        timeString = Math.round(deltaTime / 60) + " minutes"
        if (deltaTime / 60 > 60) {
            timeString = Math.round(deltaTime / 60 / 60) + " hours"
            if (deltaTime / 60 / 60 > 24) {
                timeString = Math.round(deltaTime / 60 / 60 / 24) + " days"
                if (deltaTime / 60 / 60 / 24 > 7) {
                    timeString = Math.round(deltaTime / 60 / 60 / 24 / 7) + " weeks"
                    if (deltaTime / 60 / 60 / 24 / 7 > 4) {
                        timeString = Math.round(deltaTime / 60 / 60 / 24 / 4) + " months"
                        if (deltaTime / 60 / 60 / 24 / 7 / 4 > 12) {
                            timeString = Math.round(deltaTime / 60 / 60 / 24 / 4 / 12) + " years"
                            if (deltaTime / 60 / 60 / 24 / 7 / 4 / 12 > 10) {
                                timeString = Math.round(deltaTime / 60 / 60 / 24 / 4 / 12 / 10) + " decades"
                                if (deltaTime / 60 / 60 / 24 / 7 / 4 / 12 / 10 > 10) {
                                    timeString = Math.round(deltaTime / 60 / 60 / 24 / 4 / 12 / 10 / 10) + " centuries"
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return timeString
}

// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
function react() {
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
    content.classList.add("lower");
  } else {
    header.classList.remove("sticky");
    content.classList.remove("lower");
  }
}

// Return fully qualified hostname of url
function extractHostname(url) {
    var hostname;
    //find & remove protocol (http, ftp, etc.) and get hostname

    if (url.indexOf("//") > -1) {
        hostname = url.split('/')[2];
    }
    else {
        hostname = url.split('/')[0];
    }

    //find & remove port number
    hostname = hostname.split(':')[0];
    //find & remove "?"
    hostname = hostname.split('?')[0];

    return hostname;
}

// To address those who want the "root domain," use this function:
function extractRootDomain(url) {
    var domain = extractHostname(url),
        splitArr = domain.split('.'),
        arrLen = splitArr.length;

    //extracting the root domain here
    //if there is a subdomain 
    if (arrLen > 2) {
        domain = splitArr[arrLen - 2] + '.' + splitArr[arrLen - 1];
        //check to see if it's using a Country Code Top Level Domain (ccTLD) (i.e. ".me.uk")
        if (splitArr[arrLen - 2].length == 2 && splitArr[arrLen - 1].length == 2) {
            //this is using a ccTLD
            domain = splitArr[arrLen - 3] + '.' + domain;
        }
    }
    
    
    
    return domain;
}

// Return extension of suppiled url
function extractExtension(url) {
    return url.split(/\#|\?/)[0].split('.').pop().trim();
}

// Cache url blacklist
function setBlacklist() {
    var txtFile = new XMLHttpRequest();
    txtFile.open("GET", "./src/3rd-party/blacklist.csv", true);
    txtFile.responseType = 'text';
    txtFile.onreadystatechange = function ()
    {
        if(txtFile.readyState === 4)
        {
            if(txtFile.status === 200 || txtFile.status == 0)
            {
                var result = txtFile.responseText.toString();

                var d = result.split('\n');
                for (i = 0; i < d.length; i++){
                    //alert(d[i]);
                    blacklist.push(d[i].trim());
                }

            }
        }
    }
    txtFile.send(null);
}

// Return JSON of URL
function getJSON(url, base, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.origin = "Anonymous";
    xhr.onload = function() {
      var status = xhr.status;
      if (status === 200) {
        callback(base, null, xhr.response);
      } else {
        callback(base, status, xhr.response);
      }
    };
    xhr.send();
};

//Function should be added to img onload property
function isNude(url) {
    url.classList.add("fitImage");
    $(url).fadeIn(500);
    
    getJSON( ORIGIN_PATH + "https://nsfw.haschek.at/api.php?url=" + url.id, url.id, function(base, err, data) {
        if (err !== null) {
            console.log('Something went wrong: ' + err);
        } else {
            if (data) {
                if (data.porn_probability >= 30) {
                    alertNudity(true, base, "[External Filter] NSFW: true Rating: " + data.porn_probability + " ")
                } else {
                    alertNudity(false, base, "[External Filter] NSFW: false Rating: " + data.porn_probability + " ")
                }
            } else {
                //console.log(url.id + " cannot be used with the haschek.at external filter.");
            }
        }
    })
    
    
    // Callback function for nudity detection
    function cb(e, an) {

        var ele = document.getElementById(e);

        nude.load(ele)


        nude.scan(function(result) {
            an(result, e, "[Internal Filter] NSFW: " + result + " ");


        });

    }
    
    
    cb(url.id, alertNudity);
    
    get_url_title(url.id.trim(), "title-" + url.id.trim());
    
}

// Remove blur from NSFW image
function unBlur(ele) {
    document.getElementById("div-" + ele.id).innerHTML = "";
    ele.classList.remove("nsfwBlur");
    ele.onmousedown = function() {
        //document.location = this.id;
    }
}

//Add blur filter to element id if result is true
function alertNudity(result,e,title){ 
    
    var ele = document.getElementById(e);
    
    if (!ele) {
        return;
    }
    
    ele.title += title
    if (result) {
        ele.classList.add("nsfwBlur");
        ele.onmousedown = function() {
            unBlur(this);
        }
        document.getElementById("div-" + ele.id).innerHTML = "<p><b>This image was deemed explicit. Click to remove blur.</b></p>";
        
        if (is_touch_device()) {
            document.getElementById("div-" + ele.id).innerHTML += "<p>" + ele.title + "</p>";
        }
        
    } else {
        unBlur(ele)
    }
    
    
}

// Returns youtube video id from URL
function getYoutubeId(url) {
    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    var match = url.match(regExp);

    if (match && match[2].length == 11) {
        return match[2];
    } else {
        return 'error';
    }
}

// Returns Grycat id from url
function getGfycat(url) {
    var regExp = /^.*(gfycat.com\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    var match = url.match(regExp);
    
    if (match && match[2].length > 0) {
        return match[2];
    } else {
        return 'error';
    }
}

// Returns true if magnet url
function isMagnet(url) {
    var regExp = /magnet:\?xt=urn:[a-z0-9]+:[a-z0-9]{32}/i;
    var match = url.match(regExp);
    return match
}

// Returns true if element is partially in viewport
var isInViewport = function (elem) {
    var bounding = elem.getBoundingClientRect();
    return (
        bounding.top >= -elem.clientHeight &&
        bounding.left >= -elem.clientWidth &&
        bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) + elem.clientHeight &&
        bounding.right <= (window.innerWidth || document.documentElement.clientWidth) + elem.clientWidth
    );
};

// I'm an idiot and made two of these
function isElementInViewport(el) {
    if (el == null) {
        return false;
    }
  var rect = el.getBoundingClientRect();
  return (
    rect.top >= 0 &&
    rect.left >= 0 &&
    rect.bottom <= (window.innerHeight || document. documentElement.clientHeight) &&
    rect.right <= (window.innerWidth || document. documentElement.clientWidth)
  );
}

function bufferToBase64(buf) {
    var binstr = Array.prototype.map.call(buf, function (ch) {
        return String.fromCharCode(ch);
    }).join('');
    return btoa(binstr);
}

// Returns media element based on IPFS file path
async function addFileFromIPFS(asset, filename, hash, caption, callback) {
    let url = '/' + asset
    if (filename === undefined || hash === undefined) {
        return ""
    }
    
    let returnString = "";
    
        stats = await filesystem.files.stat("/ipfs/" + hash)
        console.log(stats)
        file = null
        download_button = "<div style=\"overflow:hidden;width:100%;max-height:75vh;\"><a target=\"_blank\" href=\"" + IPFS_PATH + hash + "\"> <button type=\"button\" id=\"" + url + "\">Download " + extractExtension(filename).toLowerCase() + "</button></a></div><div id=\"div-" + url + "\"></div>"
        
        //console.log("Found " + filename + " at " + hash)
        filesystem.pin.add(hash, { recursive: false })
        //console.log("Added file to node")
        
        let testElement = "";
        
        switch (extractExtension(filename).toLowerCase()) {
            case "gif":
            case "jpg":
            case "jpeg":
            case "fif":
            case "ief":
            case "naplps":
            case "pict":
            case "tiff":
            case "xbm":
            case "xpm":
            case "bmp":
            case "png":
                if (stats.size < 300000000) {
                    file = await filesystem.get(hash)
                    
                    fileBuffer = file[0].content
                    testElement = "data:image/" + extractExtension(filename).toLowerCase() + ";base64," + bufferToBase64(fileBuffer);

                    returnString = "<div style=\"overflow:hidden;width:100%;max-height:75vh;\"><img class=\"feedImg\" id=\"source-" + url + "\" style=\"-webkit-user-select: none; display: none; \" src=\"" + testElement + "\" onload=\"isNude(this);\" onclick=\"openImage(this);\" alt=\"" + caption + "\" crossorigin=\"anonymous\"></img></div><div id=\"div-" + url + "\"></div>";
                } else {
                    testElement = IPFS_PATH + hash;
                    returnString = "<div style=\"overflow:hidden;width:100%;max-height:75vh;\"><img class=\"feedImg\" id=\"source-" + url + "\" style=\"-webkit-user-select: none; display: none; \" src=\"" + testElement + "\" onload=\"isNude(this);\" onclick=\"openImage(this);\" alt=\"" + caption + "\" crossorigin=\"anonymous\"></img></div><div id=\"div-" + url + "\"></div>";
                }
                
                
                returnString += download_button;
                break;
            case "avi":
            case "fli":
            case "gl":
            case "vivo":
            case "mkv":
            case "mp4":
                if (stats.size < 300000000) {
                    file = await filesystem.get(hash)
                    
                    fileBuffer = file[0].content
                    testElement = "data:video/" + extractExtension(filename).toLowerCase() + ";base64," + bufferToBase64(fileBuffer);
                    returnString = "<div style=\"overflow:hidden;width:100%;max-height:75vh;\"><video id=\"source-" + url + "\" controls=\"true\" muted loop autoplay src=\"" + testElement + "\" onloadstart=\"isNude(this);\" loaded=\"true\"></video></div><div id=\"div-" + url + "\"></div>"
                } else {
                    testElement = IPFS_PATH + hash;
                    returnString = "<div style=\"overflow:hidden;width:100%;max-height:75vh;\"><video id=\"source-" + url + "\" controls=\"true\" muted loop autoplay src=\"" + testElement + "\" onloadstart=\"isNude(this);\" loaded=\"true\"></video></div><div id=\"div-" + url + "\"></div>"
                }
                
                
                returnString += download_button;
                
                break;
            case "qt":
            case "mov":
                if (stats.size < 300000000) {
                    file = await filesystem.get(hash)
                    
                    fileBuffer = file[0].content
                    testElement = "data:video/quicktime;base64," + bufferToBase64(fileBuffer);
                    returnString = "<div style=\"overflow:hidden;width:100%;max-height:75vh;\"><video id=\"source-" + url + "\" preload=\"yes\" controls=\"true\" muted loop autoplay  onloadstart=\"isNude(this);\" src=\"" + testElement + "\"  loaded=\"true\"></video></div><div id=\"div-" + url + "\"></div>"
                } else {
                    testElement = IPFS_PATH + hash;
                    returnString = "<div style=\"overflow:hidden;width:100%;max-height:75vh;\"><video id=\"source-" + url + "\" controls=\"true\" muted loop autoplay src=\"" + testElement + "\" onloadstart=\"isNude(this);\" loaded=\"true\"></video></div><div id=\"div-" + url + "\"></div>"
                }
                
                
                returnString += download_button;
                
                break;
            case "pdf":
                if (stats.size < 300000000) {
                    file = await filesystem.get(hash)
                    
                    fileBuffer = file[0].content
                    testElement = "data:application/pdf;base64," + bufferToBase64(fileBuffer);
                    returnString = "<div style=\"overflow:hidden;width:100%;max-height:75vh;height:60vh;\"><embed style=\"height: 100% !important;\" id=\"source-" + url + "\" src=\"" + testElement + "\" onload=\"isNude(this);\" loaded=\"true\"></embed></div><div id=\"div-" + url + "\"></div>"
                } else {
                    testElement = IPFS_PATH + hash;
                    returnString = "<div style=\"overflow:hidden;width:100%;max-height:75vh;height:60vh;\"><embed style=\"height: 100% !important;\" id=\"source-" + url + "\" src=\"" + testElement + "\" onload=\"isNude(this);\" loaded=\"true\"></embed></div><div id=\"div-" + url + "\"></div>"
                }
                
                returnString += download_button;
                break;
            default:
                
                returnString = download_button;
                

                break;
        }   
    
        // Unload source when out of viewport
        var thisInterval = null
        thisInterval = setInterval(function() {
            var e = document.getElementById("source-" + url);

            if (e) {
                if (e.getAttribute("loaded") == null) {
                    clearInterval(thisInterval)
                    console.log("Cleared " + url + " source check interval.")
                    return
                }

                if (!isInViewport(e)) {
                    if (e.getAttribute("loaded") != "false") {
                        e.display = "none";
                        e.setAttribute("loaded", "false")
                        console.log("Unloaded " + url)
                    }
                } else {
                    if (e.getAttribute("loaded") != "true") {
                        e.display = "block";
                        e.setAttribute("loaded", "true")
                        console.log("Loaded " + url)
                    }
                }
            } else {
                clearInterval(thisInterval)
                console.log("Cleared " + url + " source check interval.")
                return
            }
        }, 1000);
    
        if (callback != null) {
            callback(asset, returnString)
        }
    
    return returnString;
}

// Returns media element based on URL
function findImageFromURL(url, caption) {
    var returnString = "";
    
    let testElement = "";
    
    if (!url.includes("://")) {
        url = "http://" + url
    }
    
    let domain = extractRootDomain(url)
    
    if (blacklist.indexOf(domain) > -1) {
        return "";
    }
    
    if (domain == "spotify.com") {
        testElement = url;
        
        
        if (testElement.toLowerCase().includes("open.spotify.com")) {
            testElement = testElement.replace('https://','');
            testElement = testElement.replace('http://','');
            testElement = testElement.replace('open.spotify.com','');
            
            testElement = "https://open.spotify.com/embed" + testElement;
            
            var source =  testElement + "&view=coverart"
            
            returnString = "<div style=\"overflow:hidden;width:100%;max-height:75vh;\"><iframe id=\"source-" + url + "\" src=\""+ source + "\" style=\"height: 50vw; width: 100%;\" frameborder=\"0\" allowtransparency=\"true\" loaded=\"true\"></iframe></div>"
            
            
            
            
            
        }
        
        
        
    }
    
    if (domain == "soundcloud.com") {
        testElement = url
        
        var source = "https://w.soundcloud.com/player/?url=" + testElement
        returnString = "<div style=\"overflow:hidden;width:100%;max-height:75vh;\"><iframe id=\"source-" + url + "\" style=\"height: 50vw; width: 100%;\" scrolling=\"no\" allow=\"autoplay\" src=\"" + source + "\"  frameborder=\"no\" loaded=\"true\"></iframe></div>"
        
        
    }
    
    if (domain == "apple.com") {
        testElement = url;
        if (testElement.toLowerCase().includes("itunes.apple.com")) {
            testElement = testElement.replace('https://','');
            testElement = testElement.replace('http://','');
            testElement = testElement.replace('itunes.apple.com','');
            
            var source = "https://embed.music.apple.com" + testElement
            
            returnString = "<div style=\"overflow:hidden;width:100%;max-height:75vh;\"><iframe id=\"source-" + url + "\" allow=\"autoplay *; encrypted-media *;\" frameborder=\"0\" style=\"max-width:660px;overflow:hidden;background:transparent;max-height:75vh; width: 100%;\" sandbox=\"allow-forms allow-popups allow-same-origin allow-scripts allow-storage-access-by-user-activation allow-top-navigation-by-user-activation\" src=\"" + source + "\" loaded=\"true\"></iframe></div>"
            
            
        }
    }
    
    var youtubeId = getYoutubeId(url);
    
    if (domain == "gfycat.com") {
        testElement = url
        var gfycat = getGfycat(url);
    
        
        url = "https://giant." + domain + "/" + gfycat + ".mp4"
    }
    
    
    
    if (youtubeId == "error" && returnString == "") {
        switch (extractExtension(url).toLowerCase()) {
            case "gif":
            case "jpg":
            case "jpeg":
            case "png":
                testElement = ORIGIN_PATH + url;
                
                returnString = "<div style=\"overflow:hidden;width:100%;max-height:75vh;\"><img class=\"feedImg\" id=\"" + url + "\" style=\"-webkit-user-select: none; display: none; \" src=\"" + testElement + "\" onload=\"isNude(this);\" onclick=\"openImage(this);\" alt=\"" + caption + "\" crossorigin=\"anonymous\"></img></div><div id=\"div-" + url + "\"></div>";
                

                break;
            case "mp4":
                var source = url
                returnString = "<div style=\"overflow:hidden;width:100%;max-height:75vh;\"><video id=\"source-" + url + "\" preload=\"yes\" controls=\"true\" muted loop autoplay  onloadstart=\"isNude(this);\" src=\"" + url + "\"  loaded=\"true\"></video></div><div id=\"div-" + testElement + "\"></div>"
                
                
                
                break;
            default:
                var stripped_ = url.replace(/\s/g,'')
                var domain_ = extractRootDomain(stripped_)

                if (blacklist.indexOf(domain_) > -1) {
                    returnString = "<div id=\"div-" + url + "\"></div>"
                    break;
                } else {
                    $.ajax({
                        type: 'GET',
                        url: ORIGIN_PATH + stripped_,
                        success: function(res) {
                                // page exists
                            if (res) {
                                if (res["status"]["error"]) {
                                    console.log(stripped_ + " did not return a valid response.");

                                    var d = document.getElementById("img-" + url);
                                    d.style = "display:none";
                                }
                            }

                        }
                    });

                    var source = url
                    
                    returnString = "<div id=\"div-" + url + "\"></div>"
                    
                    
                }
                

                break;
        }
    } else if (returnString == "") {
        var source = "https://www.youtube.com/embed/" + youtubeId
        
        returnString = '<div><iframe id=\"source-' + url + '\" style=\'height: 55vw; width: 100%; max-height: 75vh;\'  src=\"' + source + '\" frameborder="0" allowfullscreen loaded=\"true\"></iframe></div>';
        
        
    }
    
    // Unload source when out of viewport
    var thisInterval = null
    thisInterval = setInterval(function() {
        var e = document.getElementById("source-" + url);
        
        if (e) {
            if (e.getAttribute("loaded") == null) {
                clearInterval(thisInterval)
                console.log("Cleared " + url + " source check interval.")
                return
            }
            
            if (!isInViewport(e)) {
                if (e.getAttribute("loaded") != "false") {
                    e.src = ""
                    e.setAttribute("loaded", "false")
                    console.log("Unloaded " + url)
                }
            } else {
                if (e.getAttribute("loaded") != "true") {
                    e.src = source
                    e.setAttribute("loaded", "true")
                    console.log("Loaded " + url)
                }
            }
        } else {
            clearInterval(thisInterval)
            console.log("Cleared " + url + " source check interval.")
            return
        }
    }, 1000);
    
    return returnString;
}

// Appends URL tags to all URLs in message
function urlify(text) {
    var urlRegex = /(([a-z]+:\/\/)?(([a-z0-9\-]+\.)+([a-z]{2}|aero|arpa|biz|com|coop|edu|gov|info|int|jobs|mil|museum|name|nato|net|org|pro|travel|local|internal))(:[0-9]{1,5})?(\/[a-z0-9_\-\.~]+)*(\/([a-z0-9_\-\.]*)(\?[a-z0-9+_\-\.%=&amp;]*)?)?(#[a-zA-Z0-9!$&'()*+.=-_~:@/?]*)?)(\s+|$)/gi;
    return text.replace(urlRegex, function(url) {
        var stripped = url.replace(/\s/g,'')
        if (!stripped.includes("://")) {
            stripped = "http://" + stripped
        }
        var domain = extractRootDomain(stripped)
        if (blacklist.indexOf(domain) > -1) {
            return '<a id=\"link-' + stripped + '\" class=\"blacklist\"  href="' + stripped + '">' + domain + '<i class=\"fa fa-ban\" style=\"font-size:10px;padding-bottom:0px;line-height: 0px;\"></i></a> ';
        } else {
            $.ajax({
                type: 'GET',
                url: ORIGIN_PATH + stripped,
                success: function(res) {
                        // page exists
                    if (res) {
                        if (res["status"]) {
                            if (res["status"]["error"]) {
                                console.log(stripped + " did not return a valid response.");

                                var d = document.getElementById("link-" + stripped);
                                d.classList.add("blacklist");
                            }
                        }
                    }
                    
                }
            });
            
            return '<a id=\"link-' + stripped + '\" href="' + stripped + '">' + domain + '<i class=\"fa fa-external-link\" style=\"font-size:10px;padding-bottom:0px;line-height: 0px;\"></i></a> ';
        }
        
        
    })
}

// Finds links in string of text and returns array
function linkList(text) {
    var urlRegex = /(([a-z]+:\/\/)?(([a-z0-9\-]+\.)+([a-z]{2}|aero|arpa|biz|com|coop|edu|gov|info|int|jobs|mil|museum|name|nato|net|org|pro|travel|local|internal))(:[0-9]{1,5})?(\/[a-z0-9_\-\.~]+)*(\/([a-z0-9_\-\.]*)(\?[a-z0-9+_\-\.%=&amp;]*)?)?(#[a-zA-Z0-9!$&'()*+.=-_~:@/?]*)?)(\s+|$)/gi;
    
    return text.match(urlRegex);
}

// Returns local URL for tagline
function getTagUrl(tagline, count_tags, relevant) {
    var escapeChar = tagline[0]
    var partial = tagline.substr(1,tagline.length)
    
    if (partial == "" ) {
        return null;
    }
    
    var url = null;
    switch (escapeChar) {
        case '#':
            partial = partial.toLowerCase()
            if (partial.length === 7 && partial.indexOf('-') === 4 && !isNaN(partial.substr(0, 4)) && !isNaN(partial.substr(5,2))) {
                url = "\"./feed.html?week=" + partial + "\""
                if (count_tags && !relevant) {
                    add_relevant_tag(escapeChar + partial)
                }
                break;
            }
            url = "\"./feed.html?tag=" + partial + "\""
            if (count_tags && !relevant) {
                add_relevant_tag(escapeChar + partial)
            }
            break;
        case '@':
            url = "\"./account.html?name=" + partial + "\""
            if (count_tags && !relevant) {
                add_relevant_tag(escapeChar + partial)
            }
            break;
        case '>':
            url = "\"./feed.html?post=" + partial + "\""
            break;
    }
    return url;
}

// Pass a raw message and get returned a fully coded text element with anchor tags and links
function parseRawMessage(rawMessage,relevant=false) {
    var displayMessage = ""
    var escapeChar = ""
    var openLink = false
    var partial = ""
    
    var breakChars = [':', ' ', '#', '@', '~', '!', '$', '%', '^', '&', '*', '(', ')', '+', '=', '{', '}', ';', '"', '\'', '<', '>', ',', '.', '?', '/', '\\', '|']
    
    if (!rawMessage) {
        return "";
    }
    
    for (j = 0; j < rawMessage.length; j++) {
        var char = rawMessage.charAt(j)

        if (breakChars.includes(char) || j === rawMessage.length - 1) {
            if (openLink) {
                if (!breakChars.includes(char)) {
                    partial += char
                }
                openLink = false
                url = getTagUrl(escapeChar + partial, true, relevant)
                
                if (url) {
                    displayMessage += "<a href=" + url + ">" + escapeChar + partial + "</a>"
                } else {
                    displayMessage += escapeChar + partial
                }
                
                partial = ""
                if (!breakChars.includes(char)) {
                    break
                }
            }
        }
        

        if (openLink) {
            partial += char
        } else {
            if (char === '#' && !openLink) {
                if (displayMessage.length > 0) {
                    if (displayMessage[displayMessage.length - 1] === ' ') {
                        openLink = true
                        escapeChar = '#'
                    }
                } else {
                    openLink = true
                    escapeChar = '#'
                }
                
            } else if (char === '@' && !openLink) {
                if (displayMessage.length > 0) {
                    if (displayMessage[displayMessage.length - 1] === ' ') {
                        openLink = true
                        escapeChar = '@'
                    }
                } else {
                    openLink = true
                    escapeChar = '@'
                }
                
            } else if (char === '>' && !openLink) {
                if (displayMessage.length > 0) {
                    if (displayMessage[displayMessage.length - 1] === ' ') {
                        openLink = true
                        escapeChar = '>'
                    }
                } else {
                    openLink = true
                    escapeChar = '>'
                }
                
            }
            
            if (!openLink) {
                displayMessage += escapeHtml(char)
            }
                
            
        }
    }
    
    var wrappedMessage = urlify(displayMessage)
    var plainText = wrappedMessage.stripHTML();
    var overflow = plainText.substr(240, plainText.length);
    
    var strippedMessage = wrappedMessage.replace(overflow, '');
    
    var returnMessage = strippedMessage;
    
    if (overflow.length > 0) {
        if (strippedMessage.length === wrappedMessage.length) {
            returnMessage = parseRawMessage(rawMessage.substr(0,240));
        }
        returnMessage += "<font color='red'>[" + overflow + "]</font>"
    }
    
    return returnMessage;
}

// Style message based on character length
function styleMessage(message) {
    var strippedMessage = message.stripHTML();
                    
    if (strippedMessage.length <= 75) {
        return "<div class=\"feedSpacer\"></div><div class=\"post-message\"><h2>" + message + "</h2></div>"
    } else if (strippedMessage.length <= 150) {
        return "<div class=\"feedSpacer\"></div><div class=\"post-message\"><h3>" + message + "</h3></div>"
    } else {
        return "<div class=\"feedSpacer\"></div><div class=\"post-message\"><p><b>" + message + "<b></p></div>"
    } 
}

// Finds the vanity for the transactionid and sets the account div with whats found
async function setVanity(asset, public_hash) {
    document.getElementById("vanity-" + asset).href += public_hash
    blockchain.getAccountData(public_hash)
    
    filesystem.pin.add("/ipfs/" + public_hash).then(function() {
        console.log("Pinned " + public_hash)
    })
    
    function checkData() {
        if (blockchain.RESPONSE_getAccountData != null) {
            var avatar = blockchain.RESPONSE_getAccountData[1]
            var vanity = blockchain.RESPONSE_getAccountData[2]
            
            if (vanity != "") {
                        if (avatar == "") {
                            
                            document.getElementById("vanity-" + asset).innerHTML = "<img src=\"" + DEFAULT_AVATAR + "\" width=25px height=25px align=left style=\"margin: 5px; margin-top: 0px; margin-bottom: 1px; margin-left: 0px;border-radius: 50%;\"></img><div style=\"margin: 10px; width: 250px; margin-top: 3px; margin-bottom: 0px;\"><b>" + vanity + "</b></div>"
                        } else {
                            document.getElementById("vanity-" + asset).innerHTML = "<img src=\"" + avatar + "\" width=25px height=25px align=left style=\"margin: 5px; margin-top: 0px; margin-bottom: 1px; margin-left: 0px;border-radius: 50%;\"></img><div style=\"margin: 10px; width: 250px; margin-top: 3px; margin-bottom: 0px;\"><b>" + vanity + "</b></div>"
                        }
                        
                    } else {
                        if (avatar != "") {
                            
                            document.getElementById("vanity-" + asset).innerHTML = "<img src=\"" + avatar + "\" width=25px height=25px align=left style=\"margin: 5px; margin-top: 0px; margin-bottom: 1px; margin-left: 0px;border-radius: 50%;\"></img><div style=\"margin: 10px; width: 250px; margin-top: 3px; margin-bottom: 0px;\"><b>@" + getUsername(public_hash) + "</b></div>"
                        } else {
                            document.getElementById("vanity-" + asset).innerHTML = "<img src=\"" + DEFAULT_AVATAR + "\" width=25px height=25px align=left style=\"margin: 5px; margin-top: 0px; margin-bottom: 1px; margin-left: 0px;border-radius: 50%;\"></img><div style=\"margin: 10px; width: 250px; margin-top: 3px; margin-bottom: 0px;\"><b>@" + getUsername(public_hash) + "</b></div>"
                        }
                    }
            vanityReady = true
        
        } else {
            setTimeout(checkData, CALL_DELAY)
        }
    }
    checkData()
    
    return
}

var readyForNextBroadcast = true
var vanityReady = true
// Load next broadcast in queue
function nextBroadcastTransaction() {
    if (!readyForNextBroadcast || !vanityReady) {
        return
    }
    var broadcast = broadcasts.pop();
    
    if (!broadcast) {
        return;
    }
    readyForNextBroadcast = false
    var asset = broadcast.asset;
    var assetElement = broadcast.assetElement;
    
    blockchain.getBroadcastDetails(asset);
    
    async function checkDetails() {
        if (blockchain.RESPONSE_getBroadcastDetails != null) {
            
            var byUser = blockchain.RESPONSE_getBroadcastDetails[0]
            var year = blockchain.RESPONSE_getBroadcastDetails[1]
            var week = blockchain.RESPONSE_getBroadcastDetails[2]
            var timestamp = blockchain.RESPONSE_getBroadcastDetails[3]
            var message = blockchain.RESPONSE_getBroadcastDetails[4]
            var filename = blockchain.RESPONSE_getBroadcastDetails[5]
            var hash = blockchain.RESPONSE_getBroadcastDetails[6]
            
            var HTML = ""
            
            HTML += "<div id=\"meta-" + asset + "\" class=\"metadata\">"
            
            var lts = Math.round((new Date(timestamp)).getTime() / 1000)
                    var tts = Math.round((new Date()).getTime() / 1000)
                    var timeSince = tts - lts;
                    var timeString = getDurationString(timeSince)
                    
            try {
                async function addToImages(asset, im) {
                    if (im != "") {
                        var obj = {id: "meta-" + asset, image: im}
                        images.push(obj)
                    }
                }

                addFileFromIPFS(asset, filename, hash, message, addToImages);
                
                HTML += '<div id=\"title-' + asset + '\"></div>'
            } catch(error) {
                console.error(error);

                console.log("Checking link list")
                    let links = linkList(message) || [];


                    if (links.length > 0) {
                        var im = findImageFromURL(links[0].trim(), message);
                        if (im != "") {
                            var obj = {id: "meta-" + asset, image: im}
                            images.push(obj)
                            HTML += '<div id=\"title-' + links[0].trim() + '\"></div>'
                        }

                    }
            }
            
            var finalMessage = parseRawMessage(message)
                    
                    
                    
                    HTML += styleMessage(finalMessage);
                    
                    HTML += "<div class=\"post-options\">"
                    
                    HTML += "<a class=\"a-light\" href=\"./feed.html?post=" + asset + "\"><i class=\"fa fa-comments-o\" style=\"font-size:15px;padding-bottom:0px;line-height: 0px; padding-right: 10px;\"></i></a>";
            
                    if (byUser != readCookie("publicKey")) {
                        //HTML += "<a class=\"a-light\" href=\"#!\" id=\"status-" + asset + "\" data-toggle=\"popover\" title=\"Status\" data-content=\"You have none\" onmousedown=\"activateStatus(\'" + asset + "\');\"><i class=\"fa fa-exchange\" style=\"font-size:15px;padding-bottom:0px;line-height: 0px; padding-right: 10px;\"></i></a><a class=\"a-light\" href=\"#!\" id=\"relevance-" + asset + "\" data-toggle=\"popover\" title=\"Relevance\" data-content=\"You have none\" onmousedown=\"activateRelevance(\'" + asset + "\');\"><i class=\"fa fa-hashtag\" style=\"font-size:15px;padding-bottom:0px;line-height: 0px; padding-right: 10px;\"></i></a>"
                    }
                    
                    HTML += "</div><div class=\"pops\"></div>"
                    
                    HTML += "<div class=\"post-time\" style=\"font-size:12px;\">" + parseRawMessage("#" + new Date(timestamp).getWeekString()) + " Posted " + timeString + " ago</div>"
            
            vanityReady = false
            setVanity(asset,byUser)
            
            HTML += "</div>";
    
            var loading = document.getElementById("loading-" + asset)
                loading.style = "display: none;"
                loading.id = "done-loading"

                assetElement.innerHTML += HTML;
            
            readyForNextBroadcast = true
        } else {
            setTimeout(checkDetails, CALL_DELAY);
        }
    }
    checkDetails()
    
    
    
    return
    
}

// Start loading broadcasts every second
function start_broadcast_service() {
    feed.addEventListener('loadBroadcasts', function(e) {
        if (broadcasts.length === 0) {
            if (document.getElementById("loading-feed")) {
                document.getElementById("loading-feed").style = "display: none;"
            }
            return;
        }
        
        nextBroadcastTransaction();
        
    }, false);
    
    setInterval(function() {
            var event = new Event('loadBroadcasts');
            feed.dispatchEvent(event);
    }, 1000);
}

// Cache broadcast transaction
function setBroadcastTransaction (asset, assetElement) {
    broadcasts.unshift({asset: asset, assetElement: assetElement})
}

// Set the cached assets to be parsed by the broadcasts service for the page
function setAssets(assets) {
    
    
    for (i = 0; i < assets.length; i++) { 
            var asset = assets[i]

            var assetElement = document.createElement("div");
                assetElement.id = asset;
                assetElement.className = "feedElement";
                assetElement.innerHTML += "<div class=\"feed-title\" id=\"title-" + asset + "\"><a id=\"vanity-" + asset + "\" class=\"a-light\" href=\"./account.html?name=\"><b>Loading...</b></a></div>"; //" + getUsername(asset.data.account) + "
                //assetElement.innerHTML += "<div class=\"feed-id\"><a class=\"a-light\" href=\"./feed.html?post=" + getAssetHandle(asset.id) + "\">Reply <i class=\"fa fa-reply\"></i></a></div>";

            assetElement.innerHTML += "<div id=\"loading-" + asset + "\"><center><i style=\"padding: 15px;\" class=\"fa fa-refresh fa-spin fa-3x fa-fw\"></i></center></div>"
            //assetElement.innerHTML += "<p><b>Broadcast Message</b></p>";
            //assetElement.innerHTML += "<p><i>This is a broadcast message asset, it is created by account assets.</i></p>"
            feed.appendChild(assetElement);

            var spacerElement = document.createElement("div");
            spacerElement.id = "spacer-" + asset
            spacerElement.className = "feedSpacer";
            feed.appendChild(spacerElement)
            setBroadcastTransaction(asset, assetElement)



        }
    
    
}

// Start loading remote images when cached
function start_image_service() {
    feed.addEventListener('loadImages', function(e) {
        if (images.length === 0) {
            return;
        }
        //alert("starting to load images " + images.length);
        var im = images.shift();
        //alert(im.image);
        var element = document.getElementById(im.id);
        if (!element) {
            return;
        }
        element.innerHTML = im.image + element.innerHTML;
        
    }, false);
    
    setInterval(function() {
            var event = new Event('loadImages');
            feed.dispatchEvent(event);
    }, 1000);
}

// Adds relevant tag to the global tag list
function add_relevant_tag(tag) {
    var count_tag = tag
    //tag = tag.lowercase()
    if (tags.filter(t => t.tag == count_tag).length > 0) {
        //var to = 
        //if (to.length > 0) {
            tags.filter(t => t.tag == count_tag)[0].count += 1
        //}
    } else {
        tags.push({tag: count_tag, count: 1})
    }
    //console.log(tags.length)
    
    var ele = document.getElementById("relevant-tags");
    if (!ele) {
        return;
    }
    ele.innerHTML = ""
    
    tags.sort(function(a,b) {
        if (a.count < b.count) {
            return 1
        } else {
            return -1
        }
    });
    
    
    for (t = 0; t < tags.length; t++) {
        if (t >= 10) {
            break;
        }
        
        var requestDate = new Date();
        if (urlParams["week"]) {
            requestDate = new Date().setWeek(urlParams["week"].substr(5,2),urlParams["week"].substr(0,4))
        }
        if (tags[t].tag == "#" + (new Date(requestDate).getWeekString())) {
            continue;
        }
        
        var url = getTagUrl(tags[t].tag, false)
        
        if (url) {
            ele.innerHTML += "<b><a href=" + url + ">" + tags[t].tag + "</a></b> ";
        }
        
    }
    
}

// Called when a user signs the confirmation of the status transfer
function sendStatus(forAsset, otherUser, spendingTransaction) {
    
    words = null
    if (!readCookie("privateKey")) {
        words = document.getElementById("status-form").elements[0].value;
    }
    
    var element = document.getElementById("status-" + forAsset)
    
    if (readCookie("publicKey") != null) {
        var user = {publicKey: readCookie("publicKey"), privateKey: readCookie("privateKey")}
              
        if (!readCookie("privateKey")) {
            user = login(words);
        }
        
        if (user.publicKey == readCookie("publicKey")) {
            conn.getTransaction(spendingTransaction) .then(transaction => {
                
                var outputs = [BigchainDB.Transaction.makeOutput(BigchainDB.Transaction.makeEd25519Condition(otherUser),"1")]
                
                const transferTransaction = BigchainDB.Transaction.makeTransferTransaction(
                    [{tx: transaction, output_index: 0}],
                    outputs,
                    {appId: APP_ID, year: "year-" + new Date().getWeekYear(), week: "week-" + new Date().getWeek().doubleDigitString(), type: DATA_TRANSFER, description: "Status transfer", timestamp: new Date().toString(), context: forAsset}
                )
                const txSigned = BigchainDB.Transaction.signTransaction(transferTransaction, user.privateKey)

                conn.postTransactionCommit(txSigned).then(res => {
                    element.setAttribute("data-content", "Transaction Confirmed!")
                    $(activeElement).popover("show");
                }).catch(function(err) {
                    if (err["status"] == "500 INTERNAL SERVER ERROR") {
                        notify("Our database api is unresponsive. Please try again later.", 2)
                        console.log(err)
                    } else {
                        notify("There was an error sending your broadcast! Please try again later.", 2)
                        console.log(err)
                    }
                })
            })
        } else {
            notify("Passphrase signature invalid", 2)
        }
    } else {
        notify("You are not logged in.", 2)
        document.location = "./login.html"
    }
}

// Called when user signs the confirmation of the relevance transfer
function sendRelevance(forAsset, otherUser, spendingTransaction, tag, amountLeft, index) {
    words = null
    if (!readCookie("privateKey")) {
        words = document.getElementById("relevance-form").elements[0].value;
    }
    
    var element = document.getElementById("relevance-" + forAsset)
    
    if (readCookie("publicKey") != null) {
        var user = {publicKey: readCookie("publicKey"), privateKey: readCookie("privateKey")}
              
        if (!readCookie("privateKey")) {
            user = login(words);
        }
        
        if (user.publicKey == readCookie("publicKey")) {
            conn.getTransaction(spendingTransaction) .then(transaction => {
                
                var outputs = []
                
                if (parseInt(amountLeft) === 0) {
                    outputs = [BigchainDB.Transaction.makeOutput(BigchainDB.Transaction.makeEd25519Condition(otherUser),"1")]
                } else {
                    outputs = [BigchainDB.Transaction.makeOutput(BigchainDB.Transaction.makeEd25519Condition(user.publicKey),amountLeft), BigchainDB.Transaction.makeOutput(BigchainDB.Transaction.makeEd25519Condition(otherUser),"1")]
                }
                
                const transferTransaction = BigchainDB.Transaction.makeTransferTransaction(
                    [{tx: transaction, output_index: parseInt(index)}],
                    outputs,
                    {appId: APP_ID, year: "year-" + new Date().getWeekYear(), week: "week-" + new Date().getWeek().doubleDigitString(), type: DATA_TRANSFER, description: "Relevance transfer", timestamp: new Date().toString(), context: forAsset, tag: tag}
                )
                const txSigned = BigchainDB.Transaction.signTransaction(transferTransaction, user.privateKey)

                conn.postTransactionCommit(txSigned).then(res => {
                    element.setAttribute("data-content", "Transaction Confirmed!")
                    $(activeElement).popover("show");
                }).catch(function(err) {
                    if (err["status"] == "500 INTERNAL SERVER ERROR") {
                        alert("Our database api is unresponsive. Please try again later.")
                        console.log(err)
                    } else {
                        alert("There was an error sending your relevance! Please try again later.")
                        console.log(err)
                    }
                })
            })
        } else {
            alert("Passphrase signature invalid")
        }
    } else {
        alert("You are not logged in.")
        document.location = "./login.html"
    }
}

// Called when a user presses a relevance tag on the relevance transfer window
function confirmRelevance(forAsset, spendingTransaction, tag) {
    if (activeElement != "#relevance-" + forAsset) {
        alert("Couldn't find popover element")
        return
    }
    
    var element = document.getElementById("relevance-" + forAsset)
    
    element.setAttribute("data-content", "Loading " + tag + "...")
    $(activeElement).popover("show");
    
    conn.getTransaction(spendingTransaction) .then( transaction => {
        for (j = 0; j < transaction.outputs.length; j++) {
            if (transaction.outputs[j].public_keys[0] === readCookie("publicKey")) {
                var index = j
                var newAmount = transaction.outputs[j].amount - 1
                conn.searchAssets(forAsset) .then( assets => {
                    if (assets[0].data.appId != APP_ID) {
                        return;
                    }
                    assets = assets.filter(asset => asset.data.appId === APP_ID)
                    var dataContent = parseRawMessage("Do you want to transfer 1 #" + tag + " Relevance to @" + getUsername(assets[0].data.account) + "?"); //This will leave you with " + newAmount + ".")
                    dataContent += "<form id=\"relevance-form\">\
                                    <p><input id=\"password-area\" type=\"password\" placeholder=\"Passphrase Signature\" size=25></p>\
                                    </form>\
                                    <p><a href=\"#!\" onclick=\"sendRelevance(\'" + forAsset + "\',\'" + assets[0].data.account + "\',\'" + spendingTransaction + "\',\'" + tag + "\',\'" + newAmount + "\',\'" + index + "\');\"><button>Sign and Send</button></a></p>"
                    element.setAttribute("data-content", dataContent)
                    $(activeElement).popover("show");
                    
                    if (readCookie("privateKey")) {
                        document.getElementById("password-area").style.display = "none";
                    }
                })
                break
            }
        }
    })
}

// Called when user selects relevance element on a broadcast
function activateRelevance(assetId) {
    
    if (activeElement != "") {
        //document.removeChild(document.getElementsByClassName("popover")[0]);
        $(activeElement).popover("hide");
    }
    
    if (activeElement === "#relevance-" + assetId) {
        //document.removeChild(document.getElementsByClassName("popover")[0]);
        $(activeElement).popover("hide");
        activeElement = "";
        return;
    }
    
    //alert("Active: " + activeElement)
    
    activeElement = "#relevance-" + assetId;
    $(activeElement).popover({html:true});
    $(activeElement).popover("show");
    
    var element = document.getElementById("relevance-" + assetId)
    element.setAttribute("data-content", "Loading Relevance...");
    $(activeElement).popover("show");
    element.setAttribute("data-content", "");
    if (readCookie("publicKey") != null) {
        //dataContent = "#test (1000000)<br>#one (1)<br>This is asset " + assetId
        conn.listOutputs(readCookie("publicKey"),false) .then(outputs => {
                dataContent = element.getAttribute("data-content")
            
                function setR(outputs, timeout) {
                    //alert(relevanceindex + " " + outputs.length)
                        if (outputs.length == 0) {
                            return;
                        }
                        conn.getTransaction(outputs.pop().transaction_id) .then( transaction => {
                            var tran = transaction

                            conn.searchAssets(tran.asset.id ||tran.id) .then( assets => {
                                assets = assets.filter(asset => asset.data.appId === APP_ID)
                                if (assets[0].data.appId != APP_ID) {
                                    return;
                                }
                                //alert(assets.length)
                                if (assets[0].data.type === ASSET_RELEVANCE) {
                                    dataContent = element.getAttribute("data-content")
                                    //alert(assets[0].data.tag)
                                    if (!document.getElementById("relevance-" + assets[0].data.tag)) {
                                        dataContent += "<a href=\"#!\" onclick=\"confirmRelevance(\'" + assetId + "\',\'" + tran.id + "\',\'"+assets[0].data.tag+"\');\">#" + assets[0].data.tag + "</a> (<i id=\"relevance-" + assets[0].data.tag + "\">0</i>) "

                                        element.setAttribute("data-content", dataContent)
                                        $(activeElement).popover("show");
                                    }

                                    for (j = 0; j < tran.outputs.length; j++) {
                                        if (tran.outputs[j].public_keys[0] === readCookie("publicKey")) {
                                            var tagElement = document.getElementById("relevance-" + assets[0].data.tag)
                                            if (!tagElement) {
                                                alert("Couldn't find element")
                                            }
                                            //alert(tagElement.innerHTML + " + " + tran.outputs[j].amount)
                                            var newAmount = parseInt(tagElement.innerHTML) + parseInt(tran.outputs[j].amount)
                                            //alert(newAmount)
                                            tagElement.innerHTML = newAmount
                                            element.setAttribute("data-content", document.getElementsByClassName("popover-content")[0].innerHTML)
                                            break
                                        }
                                    }


                                }
                                
                                setTimeout(function() {
                                    setR(outputs, timeout)
                                }, timeout);
                            })



                        //document.getElementById("relevance-" + assetId).innerHTML += "Test"
                        })
                    }
                
                let timeout = 10// * i
                //alert(timeout)
                setTimeout(function() {
                    setR(outputs, timeout)
                }, timeout);
            //}
            element.setAttribute("data-content", dataContent)
            $(activeElement).popover("show");
        })
    } else {
        dataContent = "You are not logged in."
        element.setAttribute("data-content", dataContent)
        $(activeElement).popover("show");
    }
    
}

// Called when user selects status element on a broadcast
function activateStatus(assetId) {
    
    if (activeElement != "") {
        $(activeElement).popover("hide");
    }
    
    if (activeElement === "#status-" + assetId) {
        //$(activeElement).setAttribute("data-content", "");
        
        $(activeElement).popover("hide");
        //activeElement = "";
        return;
    }
    
    //alert("Active: " + activeElement)
    
    activeElement = "#status-" + assetId;
    $(activeElement).popover({html:true});
    $(activeElement).popover("show");
    
    var element = document.getElementById("status-" + assetId)
    element.setAttribute("data-content", "Loading Status...");
    $(activeElement).popover("show");
    element.setAttribute("data-content", "");
    if (readCookie("publicKey") != null) {
        //dataContent = "#test (1000000)<br>#one (1)<br>This is asset " + assetId
        conn.listOutputs(readCookie("publicKey"),false) .then(outputs => {
            
            dataContent = element.getAttribute("data-content")
            dataContent = "Status Count: <b id=\"status-count\">0</b>"
            
            dataContent += "<form id=\"status-form\">\
                                    <p><input id=\"password-area\" type=\"password\" placeholder=\"Passphrase Signature\" size=25></p>\
                                    </form>\
                                    <p><a id=\"status-confirm\" href=\"#!\" onclick=\"sendStatus();\"><button>Sign and Send</button></a></p>"
                
                function setS(outputs, timeout) {
                    //alert(relevanceindex + " " + outputs.length)
                        if (outputs.length == 0) {
                            return;
                        }
                        conn.getTransaction(outputs.pop().transaction_id) .then( transaction => {
                            var tran = transaction

                            conn.searchAssets(tran.asset.id ||tran.id) .then( assets => {
                                assets = assets.filter(asset => asset.data.appId === APP_ID)
                                if (assets[0].data.appId != APP_ID) {
                                    return;
                                }
                                //alert(assets.length)
                                if (assets[0].data.type === ASSET_BROADCAST) {
                                    statusCount = document.getElementById("status-count")
                                    
                                    var newAmount = parseInt(statusCount.innerHTML) + 1
                                    
                                    statusCount.innerHTML = newAmount
                                    
                                    conn.searchAssets(assetId) .then( assets => {
                                        assets = assets.filter(asset => asset.data.appId === APP_ID)
                                        if (assets[0].data.appId != APP_ID) {
                                            return;
                                        }
                                        
                                        document.getElementById("status-confirm").setAttribute("onclick", "sendStatus(\'" + assetId + "\',\'" + assets[0].data.account + "\',\'" + tran.id + "\');")
                                        console.log("\"sendStatus(\'" + assetId + "\',\'" + assets[0].data.account + "\',\'" + tran.id + "\')")
                                    } )
                                    
                                    
                                    //alert(assets[0].data.tag)
                                    //if (!document.getElementById("status-" + assets[0].data.tag)) {
                                        //dataContent += "<a href=\"#!\" onclick=\"confirmRelevance(\'" + assetId + "\',\'" + tran.id + "\',\'"+assets[0].data.tag+"\');\">#" + assets[0].data.tag + "</a> (<i id=\"\">0</i>) "

                                    //element.setAttribute("data-content", dataContent)
                                    element.setAttribute("data-content", document.getElementsByClassName("popover-content")[0].innerHTML)
                                    //$(activeElement).popover("show");
                                    //}



                                }
                                
                                setTimeout(function() {
                                    setS(outputs, timeout)
                                }, timeout);
                            })



                        //document.getElementById("relevance-" + assetId).innerHTML += "Test"
                        })
                    }
                
                let timeout = 10// * i
                //alert(timeout)
                setTimeout(function() {
                    setS(outputs, timeout)
                }, timeout);
            //}
            element.setAttribute("data-content", dataContent)
            $(activeElement).popover("show");
            
            if (readCookie("privateKey")) {
                document.getElementById("password-area").style.display = "none";
            }
        })
    } else {
        dataContent = "You are not logged in."
        element.setAttribute("data-content", dataContent)
        $(activeElement).popover("show");
    }
    
}

// Returns true if user is on a touch capable device
function is_touch_device() {
  var prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');
  var mq = function(query) {
    return window.matchMedia(query).matches;
  }

  if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
    return true;
  }

  // include the 'heartz' as a way to have a non matching MQ to help terminate the join
  // https://git.io/vznFH
  var query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');
  return mq(query);
}

// Toggle the visibility of the main broadcast area
function toggleBroadcastArea() {
    
    //alert("Start")
    
    var ele = document.getElementById("main-broadcast");
    
    if (!ele) {
        console.log("Ele is null");
        return;
    }
    
    if (ele.innerHTML == "") {
        var bcastArea = "<h3>Broadcast Station</h3><div id=\"preview-area\">All data on GruPur is recorded as permenant public record. This broadcast can never be deleted or modified.</div><form id=\"broadcast-form\"><p><textarea id=\"message-area\" rows=4 cols=50 maxlength=2000 placeholder=\"Message to the world...\" oninput=\"updatePreview();\"></textarea></p>"
        
        bcastArea += "<p><input type=\"file\" id=\"attachment\" name=\"attachment\" ></p>"
        
        if (!readCookie("password")) {
            bcastArea += "<p><input id=\"password-area\" type=\"password\" placeholder=\"Password\" size=30></p>"
        } 
                
        bcastArea += "</form><i id=\"sendLoader\" style=\"padding: 15px;display: none;\" class=\"fa fa-refresh fa-spin fa-3x fa-fw\"></i><p><button id=\"send-button\" onclick=\"doBroadcast()\">Sign and Send</button></p>"
        
        ele.innerHTML = bcastArea;
        
        
    
    } else {
        ele.innerHTML = "";
        return;
    }
    
    var broadcastForm = document.getElementById("broadcast-form");
    
    //setup before functions
        let typingTimer;                //timer identifier
        let doneTypingInterval = 1000;  //time in ms (5 seconds)
        let myInput = broadcastForm.elements[0];

        //on keyup, start the countdown
        myInput.addEventListener('keyup', () => {
            clearTimeout(typingTimer);
            if (myInput.value) {
                typingTimer = setTimeout(doneTyping, doneTypingInterval);
            }
        });

}

// Update the broadcast preview with what the user has typed
function updatePreview() {
        var previewArea = document.getElementById("preview-area");
    var broadcastForm = document.getElementById("broadcast-form");
        
        message = broadcastForm.elements[0].value;
    
    if (!isYourAccountPage() && urlParams["name"]) {
        message = "@" + getUsername(urlParams["name"]) + " " + message;
    }

    if (urlParams["post"]) {
        message = ">" + urlParams["post"] + " " + message;
    }

        if (message.length > 0) {
            document.getElementById("message-area").style = "width:90%;height:150px;"
        } else {
            document.getElementById("message-area").style = "";
        }

        //let links = linkList(message) || [];
        //var im = ""        
        //if (links.length > 0) {
            //alert("Trying " + index)
            //im = findImageFromURL(links[0]);

            //alert(images.length);
            //imageIDs.push(links[0])
        //}

        previewArea.innerHTML = styleMessage(parseRawMessage(message, true));
}

//user is "finished typing," do something
function doneTyping () {
    var previewArea = document.getElementById("preview-area");
    var broadcastForm = document.getElementById("broadcast-form");

    message = broadcastForm.elements[0].value;
    
    if (!isYourAccountPage() && urlParams["name"]) {
        message = "@" + getUsername(urlParams["name"]) + " " + message;
    }

    if (urlParams["post"]) {
        message = ">" + urlParams["post"] + " " + message;
    }


    let links = linkList(message) || [];
    var im = ""  
    var HTML = ""
    if (links.length > 0) {
        //alert("Trying " + index)
        im = findImageFromURL(links[0].trim(), message);

        //alert(images.length);
        //imageIDs.push(links[0])
        if (im != "") {
            HTML += '<div id=\"title-' + links[0].trim() + '\"></div>'
        }

    }

    previewArea.innerHTML = im + HTML + styleMessage(parseRawMessage(message, true));

    if (im != "") {
        var ele = document.getElementById(links[0].trim());
        if (!ele) {
            ele = document.getElementById("source-" + links[0].trim());
            if (!ele) {
                ele = document.getElementById("source-http://" + links[0].trim());
            }
        }
        ele.style = "";
        ele.classList.add("thumbnail");
    }


}

// Called when user signs and confirms broadcast transaction
async function doBroadcast() {
        var broadcastForm = document.getElementById("broadcast-form");
    var goButton = document.getElementById("send-button");
    goButton.style.display = "none";
    
    document.getElementById("sendLoader").style = "display: inline-block;";
    
        message = broadcastForm.elements[0].value;
    
        let attachment = broadcastForm.elements[1].files;
        let filename = broadcastForm.elements[1].value;
        let hash = null
        
        
        
        words = readCookie("password");
        if (!readCookie("password")) {
            words = broadcastForm.elements[2].value;
        }

        if (message == "") {
            notify("You cannot post a blank message.", 2);
            goButton.style.display = "inline-block";
            document.getElementById("sendLoader").style = "display: none;";
            return;
        }    
    
    if (!isYourAccountPage() && urlParams["name"]) {
        message = "@" + getUsername(urlParams["name"]) + " " + message;
    }

    if (urlParams["post"]) {
        message = ">" + urlParams["post"] + " " + message;
    }
    
        if (readCookie("publicKey") != null) {
            var user = {publicKey: readCookie("publicKey"), privateKey: readCookie("privateKey"), password: words}
            

            if ((await "login".encryptWith(user.publicKey,user.privateKey,user.password)) != null) {
                
                if (attachment.length > 0) {
                    const results = await filesystem.add(attachment[0]);
                    hash = results[0].hash;
                    await filesystem.pin.add(hash);
                    console.log("Wrote file")
                    document.getElementById("sendLoader").style = "display: none;";
                }
                
                console.log(hash)
                await blockchain.addBroadcast(user.publicKey,message,filename,hash);
                notify("Your broadcast was sent!", 0);
                toggleBroadcastArea();
            } else {
                notify("Password incorrect", 2)
                broadcastForm.elements[2].value = "";
                document.getElementById("sendLoader").style = "display: none;";
                goButton.style.display = "inline-block";
            }
        } else {
            alert("You are not logged in.")
            document.location = "./login.html"
        }

}

// Hide all password-area elements
function hide_passphrase() {
    if (readCookie("password")) {
        document.getElementById("password-area").style.display = "none";
    }
}

// Return page title of supplied url
function get_url_title(url, elementId) {
    switch (extractExtension(url).toLowerCase()) {
            case "gif":
            case "jpg":
            case "jpeg":
            case "png":
            case "webm":
                return;
            default:
                setTimeout(function() {
                    $.ajax({
                      url: ORIGIN_PATH + "http://textance.herokuapp.com/title/" + url,
                      complete: function(data) {
                          if (data.responseText != "null value not allowed") {
                              document.getElementById(elementId).innerHTML = "<h2 style\"font-family=Impact, Charcoal, sans-serif;\">" + parseRawMessage(data.responseText) + "</h2>";
                          }
                      }
                });
                }, 100);
                break;
        }
}

// Sort all assets in cronological order
function sort_assets_cron(assets) {
    return assets.sort(function(a,b) {
        var fDate = new Date(a.data.timestamp);
        var tDate = new Date(b.data.timestamp);
        
        var lts = Math.round(fDate.getTime() / 1000)
        var tts = Math.round(tDate.getTime() / 1000)
        var timeSince = tts - lts;
        
        if (timeSince < 0.0) {
            return 1;
        } else {
            return -1;
        }
    })
}

// Return true if you are on your account page
function isYourAccountPage() {
    var you = true;
    if (readCookie("publicKey")) {
        if (urlParams["name"] && urlParams["name"] != getUsername(readCookie("publicKey"))) {
            you = false;
        }
    } else {
        you = false;
    }
    return you;
}

// When the user clicks on the image, close the modal
modalImg.onclick = function() { 
  modal.style.display = "none";
}

// When the user clicks on <span> (x), close the modal
//span.onclick = function() { 
  //modal.style.display = "none";
//}

// When user clicks anywhere on the modal, close the modal
modal.onclick = function() { 
  modal.style.display = "none";
}

// Add to onclick for any image element
function openImage(img) {
    modal.style.display = "block";
    modalImg.src = img.src;
    captionText.innerHTML = parseRawMessage(img.alt);
}

function cacheAssets(assets) {
    for (a = 0; a < assets.length; a++) {
        var asset = assets[a];
        
        
        
        cachedAssets.push(asset);
    }
}

function loadNextPosts() {
   
    var nextAssets = []
    console.log("Cached: " + cachedAssets.length)
    for (c = 0; c < 10; c++) {
        var nextA = cachedAssets.shift();
        if (nextA) {
            lastAsset = nextA;
            nextAssets.push(nextA);
        }
    }
    
    if (nextAssets.length == 0) {
        readyToLoad = false;
    } else {
        try {
            setAssets(nextAssets)
        } catch(err) {
            console.log(err.message)
        }
        readyToLoad = true;
        
    }
}


console.log("Functions loaded")