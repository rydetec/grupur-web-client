
//const conn = new BigchainDB.Connection(API_PATH);

if (readCookie("publicKey")) {
    document.location = "./feed.html"
}

function doRegister(security) {
    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;
    
    var ele = document.getElementById("afterRegister");
    ele.innerHTML = "<div>Creating your account on the blockchain. It may take a few moments to verify on the network...</div><br>"
    
    blockchain.registerAccount(username, password);
    
    blockchain.accountId(username)
        
    function checkResponse() {
        console.log("Checking account id for " + username)
        if (blockchain.requestResponse != null) {
            if (blockchain.requestResponse > 0) {
                blockchain.requestResponse = null
                ele.innerHTML = "<div>Your account has been verified. Please login.</div><br>"
            } else {
                blockchain.requestResponse = null
                console.log(username + " not verified yet will check again")
                blockchain.accountId(username)
                setTimeout(checkResponse, 5000);
            }
        } else {
            setTimeout(checkResponse, 1000);
        }
    }
    checkResponse()
    
    
    
}