/*
Filename: globals.js
Description: Responsible for declaration all global variables and constants to be used throughout the grupur platform. 
*/

const DEV_MODE = true
const APP_ID = "grupur-dev-0" // Official Database Forks: grupur-dev-0 grupur-0
const ORIGIN_PATH = 'https://api.allorigins.win/raw?url=';
const ETH_PATH = 'https://ropsten.infura.io/v3/6ea57017a44e415a852f834378af5dff';
const IPFS_PATH = 'https://ipfs.io/ipfs/'
const DEFAULT_AVATAR = './media/default-avatar.png';
// Official ETH node 'http://cors.grupur.com:8545/'
// Official CORS Solution based on AllOrigins: 'http://cors.grupur.com/raw?url=';
// CORS Alternatives: https://api.allorigins.win/raw?url=


const CALL_DELAY = 100

var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
               navigator.userAgent &&
               navigator.userAgent.indexOf('CriOS') == -1 &&
               navigator.userAgent.indexOf('FxiOS') == -1;

// BigChain Constants
const ASSET_ACCOUNT = 0;
const ASSET_BROADCAST = 1;
const ASSET_RELEVANCE = 2;

const DATA_LOGIN = 0;
const DATA_MESSAGE = 1;
const DATA_GROUP = 2;
const DATA_TRANSFER = 3;

//const conn = null;//new BigchainDB.Connection(API_PATH);

var mnemonic = new Mnemonic("english")
var encoder = new TextEncoder();

// Feed variables
var cachedAssets = [];
var lastAsset = null;
var readyToLoad = false

// global arrays used for services
var broadcasts = [];
var images = [];
var tags = [];

// URL Link Blacklists
var blacklist = [];

// Set site-wide elements
var header = document.getElementById("top-header");
var title = document.getElementById("header-title");
var content = document.getElementById("main-content");
var footer = document.getElementById("bottom-footer");
var headerOptions = document.getElementById("header-options");
var accountOptions = null

// Popup Image Browser
var modal = document.getElementById('myModal');
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
// Get the <span> element that closes the modal
//var span = document.getElementsByClassName("close")[0];

// Get the offset position of the navbar
var sticky = header.offsetTop;

// User element interaction
var activeElement = ""

console.log("Globals loaded")