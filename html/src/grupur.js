/*
Filename: grupur.js
Description: Responsible for all grupur platform service bootloaders and objects to be called by app scene objects. 
*/

if (isSafari) {
    
} 

var isChromium = window.chrome;
var winNav = window.navigator;
var vendorName = winNav.vendor;
var isOpera = typeof window.opr !== "undefined";
var isIEedge = winNav.userAgent.indexOf("Edge") > -1;
var isIOSChrome = winNav.userAgent.match("CriOS");

if (isIOSChrome) {
   // is Google Chrome on IOS
} else if(
  isChromium !== null &&
  typeof isChromium !== "undefined" &&
  vendorName === "Google Inc." &&
  isOpera === false &&
  isIEedge === false
) {
   // is Google Chrome
} else { 
   // not Google Chrome 
}



// Database object
var filesystem = null
var blockchain = null

async function openPgpConnect() {
    await openpgp.initWorker({ path:'./src/3rd-party/openpgp.worker.js' })
}
openPgpConnect()

async function ipfsConnect() {
    filesystem = await window.Ipfs.create({
        config: {
            Addresses: {
                API: '/ip4/0.0.0.0/tcp/5001',
                Gateway: '/ip4/0.0.0.0/tcp/9001'
            },
            API: {
                HTTPHeaders: {
                    "Access-Control-Allow-Origin": "['*']"
                }
            }
        }
    })
    
    //await filesystem.config.set("Addresses.API", '/ip4/0.0.0.0/tcp/5001');
    //await filesystem.config.set("Addresses.Gateway", '/ip4/0.0.0.0/tcp/9001');
    //await filesystem.config.set("API.HTTPHeaders.Access-Control-Allow-Origin", '["*"]');
    
    
    console.log("Resolving latest grupur hash")
    hash = await filesystem.resolve("/ipns/grupur.com");
    console.log("Fetching latest version from root " + hash)
    //result = await filesystem.get(hash)
    /*filesystem.get(hash).then(function (result) {
        console.log("Adding root to browser dir " + result.path)
        filesystem.add(result.path)
    })*/
    //console.log("Adding root to browser dir " + result.path)
    await filesystem.pin.add(hash, { recursive: true });
    console.log("Hosting local node")
}
ipfsConnect()

blockchain = new Blockchain()

blockchain.GruPurContract()

blockchain.getVersion()

async function checkVersion(){
    if (blockchain.conn === "undefined" || blockchain.conn == null) {
        setTimeout(checkVersion, CALL_DELAY)
        return
    }
    // do whatever you like here
    if (blockchain.version == null || blockchain.version === undefined) {
        console.log("Waiting for blockchain connection")
        setTimeout(checkVersion, CALL_DELAY);
    } else {
        notify("Connected Successfully", 0)
        if (DEV_MODE) notify("Contract Version: " + blockchain.version)
    }

}

checkVersion();

setInterval(function() {
        
        
    
    if (filesystem == null) {
        notify("Cannot connect to the IPFS network.", 2);
    }
    
    if (blockchain.version == null || blockchain.version === undefined) {
        notify("Cannot connect to the Ethereum network.", 2);
    }
        
}, 5000)

async function setGasPrice() {
    if (blockchain.conn === "undefined" || blockchain.conn == null) {
        setTimeout(setGasPrice, CALL_DELAY)
        return
    }
    blockchain.conn.eth.getGasPrice(function(error, result) {
        if (error == null) {
            blockchain.gasPrice = Math.round(result * 1.1)
            console.log(blockchain.gasPrice)
        } else {
            console.log(error)
        }
    })
}
setGasPrice()


// Refresh cookie
if (readCookie("publicKey") != null) {
    createCookie("publicKey", readCookie("publicKey"), 1)
}

if (readCookie("privateKey") != null) {
    createCookie("privateKey", readCookie("privateKey"), 1)
}

if (readCookie("password") != null) {
    createCookie("password", readCookie("password"), 1)
}

// Set development banner
if (DEV_MODE) content.innerHTML =   "\
    <div class=\"top-bar\" onclick=\"//this.style='display:none';\">\
        <div class=\"top-bar-title\">\
            <h3>NOTICE</h3>\
        </div>\
        <div class=\"top-bar-body\">\
            <h3 class=\"top-bar-line\"><u>GruPur is actively evolving, check back often.</u></h3><h4 class=\"top-bar-line\">Contribute to the GruPur platform with <a href=\"./register.html\">content</a>, <a href=\"https://bitbucket.org/rydetec/grupur-web-client/src\">development</a> or <a href=\"https://etherscan.io/address/0x6e8eb6575c62b9f9c07fa666464c9543e7ecfca4\">donation</a>.</h4><h5 class=\"top-bar-line\"><a href=\"https://bitbucket.org/rydetec/grupur-web-client/issues?status=new&status=open\">Report an issue\</a></h5>\
        </div>\
    </div>" + content.innerHTML;

// Check user has verified disclaimer statements
if (scene.getName() != "mature.html") {
    if (!readCookie("mature")) {
        if (!DEV_MODE) document.location = "./mature.html"
    }
}

// When the user scrolls the page, execute reaction function 
window.onscroll = function() {react()};

// Set grupur icon to header
title.innerHTML = "<img style=\"width:30px;height:30px;padding-bottom:0px;line-height: 0px;padding-top:0px;\" src=\"./media/icon.png\"></img>"

// If user is logged in
if (readCookie("publicKey")) {
    // Logout button
    headerOptions.innerHTML = "<div style=\"float:right;padding-right: 10px;\"><a title=\"Logout and clear all cookies\" class=\"a-light\" href=\"#\" onclick=\"logout();\"><img id=\"header-logout\" class=\"menu-image\" src=\"./media/logout.png\" width=30px height=30px style=\"border-radius: 50%;padding-bottom:0px;padding-top:0px;line-height: 0px;margin:0px;\"></img></a></div>"
    
    // Account button with picture
    headerOptions.innerHTML += "<div style=\"float:right;padding-right: 10px;\"><a title=\"View account page\" class=\"a-light\" href=\"./account.html\"><img id=\"header-account\" class=\"menu-image\" src=\"" + DEFAULT_AVATAR + "\" width=30px height=30px style=\"border-radius: 50%;padding-bottom:0px;padding-top:0px;line-height: 0px;margin:0px;color: darkred;\"></img></a></div>"
    
    // Global Feed button
    headerOptions.innerHTML += "<div style=\"float:right;padding-right: 10px;\"><a title=\"Browse global feed\" class=\"a-light\" href=\"./feed.html\"><img id=\"header-global\" class=\"menu-image\" src=\"./media/feed.png\" width=30px height=30px style=\"border-radius: 50%;padding-bottom:0px;padding-top:0px;line-height: 0px;margin:0px;\"></img></a></div>"
    
    // Toggle Broadcast button
    headerOptions.innerHTML += "<div style=\"float:right;padding-right: 10px;\"><a title=\"Send a broadcast\" class=\"a-light\" href=\"#\" onmousedown=\"toggleBroadcastArea();\"><img id=\"header-global\" class=\"menu-image\" src=\"./media/broadcast.png\" width=30px height=30px style=\"border-radius: 50%;padding-bottom:0px;padding-top:0px;line-height: 0px;margin:0px;\"></img></a></div>"
    
    // Set account picture to account button in header
    /*database.processAccountAssets(readCookie("publicKey"), function(assets) {
        database.processAssetTransactions(assets[0].id, function(transactions) {
            const lastTransaction = transactions[transactions.length-1]

            if (lastTransaction.metadata.avatar && (lastTransaction.metadata.avatar != "" && lastTransaction.metadata.avatar != "avatar-")) {
                document.getElementById("header-account").src = getAvatar(lastTransaction.metadata.avatar)
            } else {
                document.getElementById("header-account").src = DEFAULT_AVATAR
            }

            if (!getVanity(lastTransaction.metadata.vanity) || getVanity(lastTransaction.metadata.vanity) == "" ) {
                if (DEV_MODE) notify("Welcome back @" + getUsername(readCookie("publicKey")), 0);
            } else {
                if (DEV_MODE) notify("Welcome back " + getVanity(lastTransaction.metadata.vanity), 0)
            }
        })
    })*/
    
    // Your Account options panel
    accountOptions = document.createElement("div");
    accountOptions.id = "account-link";
    accountOptions.className = "optionsElement";
    accountOptions.innerHTML = "<div style=\"font-size:20px\"><a class=\"a-light\" href=\"./account.html\"><b>Your Account</b></div><div><a href=\"./account.html?mode=edit\">Edit Info</div>"
    accountOptions.innerHTML += "<div><a href=\"#\" onclick=\"logout();\" style=\"color:orangered;\">Logout</div>";
    
} else {
    headerOptions.innerHTML = "<h3><a class=\"a-light\" href=\"./feed.html\" style=\"padding-right: 10px;\">Feed</a>  <a class=\"a-light\" href=\"./login.html\" style=\"padding-right: 10px;\">Login</a>  <a class=\"a-light\" href=\"./register.html\">Register</a></h3>"
}

footer.innerHTML = "<p style=\"margin: 0;\"><a class=\"a-light\" href=\"./index.html\">GruPur | Your social platform</a></p>" 


console.log("GruPur loaded")