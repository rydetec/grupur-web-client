
if (urlParams["password"]) {
    document.location = "./account.html"
}


if (!readCookie("publicKey") && !urlParams["name"]) {
    document.location = "./login.html"
}
    
start_broadcast_service();
start_image_service();
    
reloadBroadcasts();







window.onscroll = function(ev) {
    react();
    
    if (lastAsset) {
        var ele = document.getElementById("spacer-" + lastAsset.id);
        if (ele != null) {
            if (isElementInViewport(ele)) {
                if (readyToLoad) {
                    readyToLoad = false;
                    loadNextPosts();
                }
            }
        } else {
            if (readyToLoad) {
                readyToLoad = false;
                loadNextPosts();
            }
        }
    }
}

function reloadAccountPanel(publicKey) {
    if (readCookie("publicKey")) {
        options.appendChild(accountOptions);
    }
    if (isYourAccountPage()) {
        console.log("Setting title")
        scene.header.setTitle("GruPur | Your account")
        //if (!document.getElementById("edit-info")) {
            //options.appendChild(accountOptions);
        //}
    }
    
    if (isYourAccountPage() && urlParams["mode"]) {
        scene.header.setTitle("GruPur | Edit your account")
    }
    
    
    if (!document.getElementById("loading-element")) {
        var loadingElement = document.createElement("div");
        loadingElement.id = "loading-element"
        loadingElement.className = "optionsElement";
        loadingElement.innerHTML = "<center><i style=\"padding: 15px;\" class=\"fa fa-refresh fa-spin fa-3x fa-fw\"></i></center>"
        options.appendChild(loadingElement)
    } else {
        document.getElementById("loading-element").style = "display: block;"
    }
    
    var relevantElement = document.getElementById("relevant-tags")
    if (!relevantElement) {
        relevantElement = document.createElement("div");
                        relevantElement.id = "relevant-tags";
                        relevantElement.className = "optionsElement";
                        //optionsElement.innerHTML = "<h3><a href=\"./account.html?mode=edit\">Edit Info</a></h3><p id=\"relevant-tags\"></p>";

                        options.appendChild(relevantElement);
    }
    
    blockchain.getAccountData(publicKey)
    
    async function checkAssetSearch() {
        if (blockchain.RESPONSE_getAccountData != null) {
            console.log("got assets")
            if (isYourAccountPage()) {
                
            } else {
                //notify("This account is not registered.", 2)
                //document.location = "./login.html"
                //return
            }
            
            
            let avatar = blockchain.RESPONSE_getAccountData[1]
            let vanity = blockchain.RESPONSE_getAccountData[2]
            let bio = blockchain.RESPONSE_getAccountData[3]
            
            blockchain.requestResponse = null
            
            var panel = document.getElementById("account-panel")
                panel.innerHTML = ""
                panel.style = "height: auto; border: 1px solid; padding: 5px; display: block; margin-left: 12vw; margin-right: 12vw; word-break: break-all;" 
            
            if (vanity != "") {
                if (!urlParams["mode"]) {
                    panel.innerHTML += "<h3 style=\"margin:0px;\" align=left>" + vanity + "</h3>"
                } else {
                    panel.innerHTML += "<div width=250px align=left style=\"margin:5px;\"><input id=\"vanity-input\" type=\"text\" value=\"" + vanity + "\"></input></div>"
                }
            } else {
                if (!urlParams["mode"]) {
                    panel.innerHTML += "<h3 style=\"margin:0px;\" align=left>" + getUsername(publicKey) + "</h3>"
                } else {
                    panel.innerHTML += "<div width=250px align=left style=\"margin:5px;\"><input id=\"vanity-input\" type=\"text\" value=\"" + getUsername(publicKey) + "\"></input></div>"
                }
            }
            
            if (avatar != "") {
                if (!urlParams["mode"]) {
                    panel.innerHTML += "<img src=\"" + avatar + "\" width=65px height=65px align=left style=\"margin: 5px;border-radius: 50%;\"></img>"
                } else {
                    panel.innerHTML += "<div width=250px align=left style=\"margin:5px;\"><input id=\"avatar-input\" type=\"text\" placeholder=\"" + avatar + "\" ></input></div>"
                }
            } else {
                if (!urlParams["mode"]) {
                    panel.innerHTML += "<img src=\"" + DEFAULT_AVATAR + "\" width=65px height=65px align=left style=\"margin: 5px;border-radius: 50%;\"></img>"
                } else {
                    panel.innerHTML += "<div width=250px align=left style=\"margin:5px;\"><input id=\"avatar-input\" type=\"text\" placeholder=\"Avatar image URL\" ></input></div>"
                }
            }
            
            
            
            
            panel.innerHTML += "<div width=250px align=left style=\"margin:5px;\"><b>Handle</b>: @" + getUsername(publicKey) + " </div>"
                panel.innerHTML += "<div width=250px align=left style=\"margin:5px;\"><b>Public Key</b>: " + publicKey + " </div>"
            
            if (bio != "") {
                if (!urlParams["mode"]) {
                    panel.innerHTML += "<div width=250px align=left style=\"margin:5px;\"><b>Bio Description</b>: " + styleMessage(parseRawMessage(bio)) + "</div>"
                    
                    if (!document.getElementById("account-description")) {
                            var optionsElement = document.createElement("div");
                            optionsElement.id = "account-description";
                            optionsElement.className = "optionsElement";
                            optionsElement.innerHTML = "<b>" + parseRawMessage(bio) + "</b>";

                            options.appendChild(optionsElement);
                        }
                } else {
                    panel.innerHTML += "<div width=250px align=left style=\"margin:5px;\"><b>Bio Description</b>: <p align=center><textarea id=\"bio-input\" rows=4 cols=50 maxlength=2000>" + bio + "</textarea></p></div>"
                }
            } else {
                if (!urlParams["mode"]) {
                    panel.innerHTML += "<div width=250px align=left style=\"margin:5px;\"><b>Bio Description</b>: </div>"
                } else {
                    panel.innerHTML += "<div width=250px align=left style=\"margin:5px;\"><b>Bio Description</b>: <p align=center><textarea id=\"bio-input\" rows=4 cols=50 maxlength=2000></textarea></p></div>"
                }
            }
            
            
            if (isYourAccountPage() && urlParams["mode"]) {
                if (!readCookie("password")) {
                    panel.innerHTML += "<p><input id=\"password-area\" type=\"password\" placeholder=\"Password\" size=30></p>"
                }
                panel.innerHTML += "<div align=right width=90px><a href=\"./account.html\"><button style=\"margin:5px;\">Cancel</button></a><button onclick=\"saveInfo();\">Save Info</button></div>"
            }

            if (!document.getElementById("relevant-assets")) {
                var optionsElement = document.createElement("div");
                optionsElement.id = "relevant-assets";
                optionsElement.className = "optionsElement";
                optionsElement.innerHTML = "<b>Relevant</b><div id=\"account-relevance\"></div>";

                options.appendChild(optionsElement);


            }

            if (!document.getElementById("status-assets")) {
                var optionsElement = document.createElement("div");
                optionsElement.id = "status-assets";
                optionsElement.className = "optionsElement";
                optionsElement.innerHTML = "<b>Status</b><div id=\"account-status\"></div>";

                options.appendChild(optionsElement);


            }
            
            setAccountData(publicKey)
            
        } else {
            console.log("Blockchain hasnt returned a response yet")
            setTimeout(checkAssetSearch, CALL_DELAY)
        }
    }
    
    checkAssetSearch()
    
    /*
    conn.searchAssets(publicKey) .then(assets => {
        assets = assets.filter(asset => asset.data.appId === APP_ID)
        if (assets.length === 0) {
            alert("This account is not registered.")
            document.location = "./feed.html"
            return;
        }  else {
            const user_asset = {appId: APP_ID, year: "year-" + new Date().getWeekYear(), week: "week-" + new Date().getWeek().doubleDigitString(), type: ASSET_ACCOUNT, account: publicKey, timestamp: new Date().toString()};
            
            const account_assets = assets.filter(asset => asset.data.type === ASSET_ACCOUNT)
            
            if (account_assets.length == 0) {
                alert("This account is not registered.")
                document.location = "./login.html"
                return;
            }
            
            conn.listTransactions(account_assets[0].id) .then(transactions => {
                //alert("Transactions: " + transactions.length)
                
                const lastTransaction = transactions[transactions.length-1]
                
                var lts = Math.round((new Date(lastTransaction.metadata.timestamp)).getTime() / 1000)
                var tts = Math.round((new Date(new Date().toString())).getTime() / 1000)
                var timeSince = tts - lts;
                
                
                var panel = document.getElementById("account-panel")
                panel.innerHTML = ""
                panel.style = "height: auto; border: 1px solid; padding: 5px; display: block; margin-left: 12vw; margin-right: 12vw; word-break: break-all;" 
                
                //panel.innerHTML = "<h3 style=\"margin:0px;\" align=left>Account Panel</b>"
                
                
                
                if (lastTransaction.metadata.vanity) {
                    if (!urlParams["mode"]) {
                        panel.innerHTML += "<h3 style=\"margin:0px;\" align=left>" + getVanity(lastTransaction.metadata.vanity) + "</h3>"
                        
                        if (!isYourAccountPage()) {
                            scene.header.setTitle("GruPur | " + getVanity(lastTransaction.metadata.vanity))
                        }
                    } else {
                        panel.innerHTML += "<div width=250px align=left style=\"margin:5px;\"><input id=\"vanity-input\" type=\"text\" value=\"" + getVanity(lastTransaction.metadata.vanity) + "\" ></input></div>"
                    }
                } else {
                    if (!urlParams["mode"]) {
                        panel.innerHTML += "<h3 style=\"margin:0px;\" align=left>" + getUsername(publicKey) + "</h3>"
                    } else {
                        panel.innerHTML += "<div width=250px align=left style=\"margin:5px;\"><input id=\"vanity-input\" type=\"text\" value=\"" + getUsername(publicKey) + "\"></input></div>"
                    }
                }
                
                if (lastTransaction.metadata.avatar) {
                    if (!urlParams["mode"]) {
                        panel.innerHTML += "<img src=\"" + getAvatar(lastTransaction.metadata.avatar) + "\" width=65px height=65px align=left style=\"margin: 5px;border-radius: 50%;\"></img>"
                    } else {
                        panel.innerHTML += "<div width=250px align=left style=\"margin:5px;\"><input id=\"avatar-input\" type=\"text\" value=\"" + getAvatar(lastTransaction.metadata.avatar) + "\"></input></div>"
                    }
                } else {
                    if (!urlParams["mode"]) {
                        panel.innerHTML += "<img src=\"" + DEFAULT_AVATAR + "\" width=65px height=65px align=left style=\"margin: 5px;border-radius: 50%;\"></img>"
                    } else {
                        panel.innerHTML += "<div width=250px align=left style=\"margin:5px;\"><input id=\"avatar-input\" type=\"text\" placeholder=\"Avatar image URL\" ></input></div>"
                    }
                }
                 
                
                
                panel.innerHTML += "<div width=250px align=left style=\"margin:5px;\"><b>Username</b>: @" + getUsername(publicKey) + " </div>"
                panel.innerHTML += "<div width=250px align=left style=\"margin:5px;\"><b>Public Key</b>: " + publicKey + " </div>"
                panel.innerHTML += "<div width=250px align=left style=\"margin:5px;\"><b>Last Seen</b>: " + getDurationString(timeSince) + " ago</div>"
                
                if (lastTransaction.metadata.bio) {
                    if (!urlParams["mode"]) {
                        //panel.innerHTML += "<div width=250px align=left style=\"margin:5px;\">" + styleMessage(parseRawMessage(lastTransaction.metadata.bio)) + "</div>"
                        
                        if (!document.getElementById("account-description")) {
                            var optionsElement = document.createElement("div");
                            optionsElement.id = "account-description";
                            optionsElement.className = "optionsElement";
                            optionsElement.innerHTML = "<b>" + parseRawMessage(lastTransaction.metadata.bio) + "</b>";

                            options.appendChild(optionsElement);
                        }
                        
                    } else {
                        panel.innerHTML += "<div width=250px align=left style=\"margin:5px;\"><b>Bio Description</b>: <p align=center><textarea id=\"bio-input\" rows=4 cols=50 maxlength=2000 >" + lastTransaction.metadata.bio + "</textarea></p></div>"
                    }
                } else {
                    if (!urlParams["mode"]) {
                        panel.innerHTML += "<div width=250px align=left style=\"margin:5px;\"><b>Bio Description</b>: </div>"
                    } else {
                        panel.innerHTML += "<div width=250px align=left style=\"margin:5px;\"><b>Bio Description</b>: <p align=center><textarea id=\"bio-input\" rows=4 cols=50 maxlength=2000></textarea></p></div>"
                    }
                }
                
                if (isYourAccountPage() && urlParams["mode"]) {
                    if (!readCookie("privateKey")) {
                        panel.innerHTML += "<p><input id=\"password-area\" type=\"password\" placeholder=\"Passphrase Signature\" size=30></p>"
                    }
                    panel.innerHTML += "<div align=right width=90px><a href=\"./account.html\"><button style=\"margin:5px;\">Cancel</button></a><button onclick=\"saveInfo();\">Save Info</button></div>"
                }

                if (!document.getElementById("relevant-assets")) {
                    var optionsElement = document.createElement("div");
                    optionsElement.id = "relevant-assets";
                    optionsElement.className = "optionsElement";
                    optionsElement.innerHTML = "<b>Relevant</b><div id=\"account-relevance\"></div>";

                    options.appendChild(optionsElement);
                    
                    
                }
                
                if (!document.getElementById("status-assets")) {
                    var optionsElement = document.createElement("div");
                    optionsElement.id = "status-assets";
                    optionsElement.className = "optionsElement";
                    optionsElement.innerHTML = "<b>Status</b><div id=\"account-status\"></div>";

                    options.appendChild(optionsElement);
                    
                    
                }
                
                setAccountData(publicKey)
                
                
            })
        }
        
    })
    
    */
    
    
    
}



function setAccountData(publicKey) {
    blockchain.getStatusBalance(publicKey)
    
    
    function checkBalance() {
        if (blockchain.requestResponse != null) {
            document.getElementById("account-status").innerHTML = "<b id=\"account-status-count\">" + blockchain.requestResponse + "</b>"
            blockchain.getRelevanceBalance(publicKey)
            
            function checkBalance2() {
                if (blockchain.requestResponse != null) {
                    var balances = blockchain.requestResponse
                    var index = 0;
                    
                    blockchain.getRelevanceTag(publicKey, index)
                    function checkNextTag() {
                        if (balances[index] > 0) {
                            if (blockchain.requestResponse != null) {
                                var t = blockchain.requestResponse
                                console.log("Setting tag " + t)
                                document.getElementById("account-relevance").innerHTML += "<a href=\"./feed.html?tag=" + t + "\">#" + t + "</a> (<i id=\"relevance-" + t + "\">" + balances[index] + "</i>) "
                                
                                index++
                                if (index < balances.length) {
                                    if (balances[index] > 0) {
                                        blockchain.getRelevanceTag(publicKey, index)
                                        setTimeout(checkNextTag, CALL_DELAY)
                                    } else {
                                        checkNextTag();
                                    }
                                } else {
                                    document.getElementById("loading-element").style = "display: none;"
                                }
                            } else {
                                setTimeout(checkNextTag, CALL_DELAY)
                            }
                        } else {
                            index++
                            if (index < balances.length) {
                                if (balances[index] > 0) {
                                    blockchain.getRelevanceTag(publicKey, index)
                                    setTimeout(checkNextTag, CALL_DELAY)
                                } else {
                                    checkNextTag();
                                }
                            } else {
                                document.getElementById("loading-element").style = "display: none;"
                            }
                        }
                    }
                    checkNextTag()
                    
                } else {
                    setTimeout(checkBalance2, CALL_DELAY);
                }
            }
            checkBalance2()
        } else {
            setTimeout(checkBalance, CALL_DELAY);
        }
    }
    
    checkBalance()
    
    
    return
    conn.listOutputs(publicKey,false) .then(outputs => {
        dataContent = document.getElementById("account-relevance").innerHTML
        

        function setR(outputs, timeout) {
            //alert(relevanceindex + " " + outputs.length)
                if (outputs.length == 0) {
                    //setAccountStatus(publicKey)
                    document.getElementById("loading-element").style = "display: none;"
                    return;
                }
                conn.getTransaction(outputs.pop().transaction_id) .then( transaction => {
                    var tran = transaction
                    if (tran.metadata.message) {
                        parseRawMessage(tran.metadata.message)
                    }
                    conn.searchAssets(tran.asset.id ||tran.id) .then( assets => {
                        assets = assets.filter(asset => asset.data.appId === APP_ID)
                        //alert(assets.length)
                        if (assets[0].data.type === ASSET_RELEVANCE) {

                            dataContent = document.getElementById("account-relevance").innerHTML
                            //alert(assets[0].data.tag)
                            if (!document.getElementById("relevance-" + assets[0].data.tag)) {
                                dataContent += "<a href=\"./feed.html?tag=" + assets[0].data.tag + "\">#" + assets[0].data.tag + "</a> (<i id=\"relevance-" + assets[0].data.tag + "\">0</i>) "

                                document.getElementById("account-relevance").innerHTML = dataContent
                            }

                            for (j = 0; j < tran.outputs.length; j++) {
                                if (tran.outputs[j].public_keys[0] === publicKey) {
                                    var tagElement = document.getElementById("relevance-" + assets[0].data.tag)
                                    if (!tagElement) {
                                        alert("Couldn't find element")
                                    }
                                    //alert(tagElement.innerHTML + " + " + tran.outputs[j].amount)
                                    var newAmount = parseInt(tagElement.innerHTML) + parseInt(tran.outputs[j].amount)
                                    //alert(newAmount)
                                    tagElement.innerHTML = newAmount
                                    //element.setAttribute("data-content", document.getElementsByClassName("popover-content")[0].innerHTML)
                                    break
                                }
                            }


                        }
                        
                        if (assets[0].data.type === ASSET_BROADCAST) {
                            
                            statusCount = document.getElementById("account-status-count")

                            var newAmount = parseInt(statusCount.innerHTML) + 1

                            statusCount.innerHTML = newAmount

                        }

                        setTimeout(function() {
                            setR(outputs, timeout)
                        }, timeout);
                    })



                //document.getElementById("relevance-" + assetId).innerHTML += "Test"
                })
            }

        let timeout = 10// * i
        //alert(timeout)
        setTimeout(function() {
            setR(outputs, timeout)
        }, timeout);
        //}
        //document.getElementById("account-relevance").innerHTML = dataContent
    })
}

async function saveInfo() {
    var a = document.getElementById("avatar-input").value
    var v = document.getElementById("vanity-input").value
    var b = document.getElementById("bio-input").value
    
    var publicKeyHash = readCookie("publicKey")
    var privateKeyHash = readCookie("privateKey")
    var password = readCookie("password")
    
    if (!password) {
        password = document.getElementById("password-area").value
    }
    
    if ((await "".encryptWith(publicKeyHash,privateKeyHash,password)) != null) {
        blockchain.paymentProcessing = true
        await blockchain.saveVanity(publicKeyHash,a,v,b)
        
        function checkDone() {
            if (blockchain.paymentProcessing) {
                console.log("Still waiting")
                setTimeout(checkDone, CALL_DELAY)
            } else {
                notify("Your account info has been saved!", 0)
                document.location = "./account.html"
            }
        }
        checkDone()
    } else {
        document.getElementById("password-area").value = ""
        notify("Changes not saved.", 2)
    }
    
    return
    
    
    
    var user = {publicKey: readCookie("publicKey"), privateKey: readCookie("privateKey")}

    if (!readCookie("privateKey")) {
        user = login(document.getElementById("password-area").value);
    }
    
    
    conn.searchAssets(user.publicKey) .then(assets => {
        assets = assets.filter(asset => asset.data.appId === APP_ID)
        if (assets.length === 0) {
            alert("This account is not registered.")
            document.location = "./login.html"
            return;
        } else {
            const user_asset = {appId: APP_ID, year: "year-" + new Date().getWeekYear(), week: "week-" + new Date().getWeek().doubleDigitString(), type: ASSET_ACCOUNT, account: user.publicKey, timestamp: new Date().toString()};
            
            const account_assets = assets.filter(asset => asset.data.type === ASSET_ACCOUNT)
            
            if (account_assets.length == 0) {
                alert("This account is not registered.")
                document.location = "./login.html"
                return;
            }
            
            
            //alert("Asset ID: " + account_assets[0].id)
            
            conn.listTransactions(account_assets[0].id) .then(transactions => {
                //alert("Transactions: " + transactions.length)
                
                const lastTransaction = transactions[transactions.length-1]
                
                //alert(lastTransaction.metadata.description + " " + lastTransaction.metadata.timestamp)
                
                const transferTransaction = BigchainDB.Transaction.makeTransferTransaction(
                    [{tx: lastTransaction, output_index: 0}],
                    [BigchainDB.Transaction.makeOutput(BigchainDB.Transaction.makeEd25519Condition(user.publicKey))],
                    {appId: APP_ID, year: "year-" + new Date().getWeekYear(), week: "week-" + new Date().getWeek().doubleDigitString(), type: DATA_LOGIN, description: "Login timestamp", timestamp: new Date().toString(), vanity: "vanity-" + v.value, bio: b.value, avatar: "avatar-" + a.value}
                )
                
                const txSigned = BigchainDB.Transaction.signTransaction(transferTransaction, user.privateKey)
                
                var lts = Math.round((new Date(lastTransaction.metadata.timestamp)).getTime() / 1000)
                var tts = Math.round((new Date(transferTransaction.metadata.timestamp)).getTime() / 1000)
                var timeSince = tts - lts;
                            
                //alert("Your last login was " + getDurationString(timeSince) + " ago, continue?")
                
                // Uncomment this to record transaction
                conn.postTransactionCommit(txSigned).then(res => {
                    createCookie("publicKey",user.publicKey,1);
                    
                    //alert("Logged in: " + txSigned.id)
                    //loginForm.onSubmit = function() {
                    
                    console.log("Your account info has been saved!")
                    document.location = "./account.html"
                    //}
                    //$('login-form').submit();
                    
                }).catch(function(err) {
                    if (err["status"] == "500 INTERNAL SERVER ERROR") {
                        alert("Our database api is unresponsive. Please try again later.")
                        console.log(err)
                    } else {
                        alert("There was an error sending your broadcast! Please try again later.")
                        console.log(err)
                    }
                })
                
            
            })
            
            
        }
        
    })
}


function reloadAssets() {
    cachedAssets = [];
    lastAsset = null;
    readyToLoad = false
    
    document.getElementById("feed").innerHTML = "<div id=\"account-feeds\"></div>";
    
    var you = true;
    if (readCookie("publicKey")) {
        if (urlParams["name"] && urlParams["name"] != getUsername(readCookie("publicKey"))) {
            you = false;
        }
    } else {
        you = false;
    }
    
    if (you) {
        drawAssets(readCookie("publicKey"))
    } else {
        //document.getElementById("message-area").innerHTML = "@" + urlParams["name"];
        conn.searchAssets(APP_ID) .then(assets => {
            assets = assets.filter(asset => asset.data.appId === APP_ID)
            for (i = 0; i < assets.length; i++) { 
                var asset = assets[i]
                if (asset.data.type === ASSET_ACCOUNT) {
                    if (getUsername(asset.data.account) === urlParams["name"]) {
                        drawAssets(asset.data.account)
                        return
                    }
                }
            }
        })
    }
    
}

function reloadBroadcasts() {
    
    
    cachedAssets = [];
    lastAsset = null;
    readyToLoad = false
    
    document.getElementById("feed").innerHTML = "<div id=\"account-feeds\"></div>";
    
    if (!readCookie("publicKey")) {
        //document.getElementById("message-area").innerHTML = "@" + urlParams["name"];
        /*conn.searchAssets(APP_ID) .then(assets => {
            assets = assets.filter(asset => asset.data.appId === APP_ID)
            for (i = 0; i < assets.length; i++) { 
                var asset = assets[i]
                if (asset.data.type === ASSET_ACCOUNT) {
                    if (getUsername(asset.data.account) === urlParams["name"]) {
                        if (!document.getElementById("loading-element")) {
                            reloadAccountPanel(asset.data.account);
                        }
                        drawBroadcasts(asset.data.account)
                        break
                    }
                }
            }
        })*/
        if (!document.getElementById("loading-element")) {
                reloadAccountPanel(urlParams["name"]);
            }
            drawBroadcasts(urlParams["name"])
    } else {
        if (urlParams["name"] && urlParams["name"] != readCookie("publicKey")) {
            //document.getElementById("message-area").innerHTML = "@" + urlParams["name"];
            /*conn.searchAssets(APP_ID) .then(assets => {
                assets = assets.filter(asset => asset.data.appId === APP_ID)
                for (i = 0; i < assets.length; i++) { 
                    var asset = assets[i]
                    if (asset.data.type === ASSET_ACCOUNT) {
                        if (getUsername(asset.data.account) === urlParams["name"]) {
                            if (!document.getElementById("loading-element")) {
                                reloadAccountPanel(asset.data.account);
                            }
                            drawBroadcasts(asset.data.account)
                            break
                        }
                    }
                }
            })*/
            if (!document.getElementById("loading-element")) {
                reloadAccountPanel(urlParams["name"]);
            }
            drawBroadcasts(urlParams["name"])
        } else {
            if (!document.getElementById("loading-element")) {
                reloadAccountPanel(readCookie("publicKey"));
            }
            drawBroadcasts(readCookie("publicKey"))
        }
    }
}

function reloadMentions() {
    cachedAssets = [];
    lastAsset = null;
    readyToLoad = false
    
    document.getElementById("feed").innerHTML = "<div id=\"account-feeds\"></div>";
    
    if (!readCookie("publicKey")) {
        //document.getElementById("message-area").innerHTML = "@" + urlParams["name"];
        conn.searchAssets(APP_ID) .then(assets => {
            assets = assets.filter(asset => asset.data.appId === APP_ID)
            for (i = 0; i < assets.length; i++) { 
                var asset = assets[i]
                if (asset.data.type === ASSET_ACCOUNT) {
                    if (getUsername(asset.data.account) === urlParams["name"]) {
                        filterMentions(asset.data.account, assets)
                        break
                    }
                }
            }
        })
    } else {
        if (urlParams["name"] && urlParams["name"] != getUsername(readCookie("publicKey"))) {
            //document.getElementById("message-area").innerHTML = "@" + urlParams["name"];
            conn.searchAssets(APP_ID) .then(assets => {
                assets = assets.filter(asset => asset.data.appId === APP_ID)
                for (i = 0; i < assets.length; i++) { 
                    var asset = assets[i]
                    if (asset.data.type === ASSET_ACCOUNT) {
                        if (getUsername(asset.data.account) === urlParams["name"]) {
                            filterMentions(asset.data.account, assets)
                            break
                        }
                    }
                }
            })
        } else {
            conn.searchAssets(APP_ID) .then(assets => {
                assets = assets.filter(asset => asset.data.appId === APP_ID)
                filterMentions(readCookie("publicKey"), assets)
            })
        }
    }
    
    
}

function filterMentions(publicKey, assets) {
    conn.searchMetadata( "\"@" + getUsername(publicKey) + "\"and=\"" + APP_ID + "\"") .then(metas => {
        var metas = metas
        var filtered = []
        for (i = 0; i < assets.length; i++) {
            var check = metas.filter(meta => assets[i].id === meta.id)
            if (check.length > 0) {
                if (check[0].metadata.message.includes("@" + getUsername(publicKey))) {
                    filtered.push(assets[i])
                }
            }
        }
        drawMentions(publicKey, filtered)
    })
}

function drawMentions(publicKey, assets) {
    //if (!urlParams["name"]) {
        
    //} else {
        //if (urlParams["name"] == getUsername(publicKey)) {
            //document.getElementById("username").innerHTML = "<b><h3>Your Account</h3></b>";
        //} else {
            
        //}
    //}
    
    document.getElementById("account-feeds").innerHTML = "<h2> \
    <a href=\"#\" onclick=\"reloadBroadcasts();\">Broadcasts</a> | \
    <b>Mentions</b> | \
    <a href=\"#\" onclick=\"reloadAssets();\">Assets</a></h2>";
    
    cacheAssets(sort_assets_cron(assets));
    readyToLoad = true;
    loadNextPosts();
    
    /*for (i = assets.length-1; i >= 0; i--) { 
        var asset = assets[i]

        var assetElement = document.createElement("div");
            assetElement.id = asset.id;
            assetElement.className = "feedElement";
            assetElement.innerHTML += "<div class=\"feed-title\" id=\"title-" + asset.id + "\"><a class=\"a-light\" href=\"./account.html?name=" + getUsername(asset.data.account) + "\"><b>" + getUsername(asset.data.account) + "</b></a></div>";
            assetElement.innerHTML += "<div class=\"feed-id\">" + asset.id + "</div>";

        if (asset.data.type === ASSET_BROADCAST) {
            //assetElement.innerHTML += "<p><b>Broadcast Message</b></p>";
            //assetElement.innerHTML += "<p><i>This is a broadcast message asset, it is created by account assets.</i></p>"
            feed.appendChild(assetElement);
            
            var spacerElement = document.createElement("div");
                spacerElement.className = "feedSpacer";
                feed.appendChild(spacerElement)
            
            setBroadcastTransaction(asset, assetElement)
        }

    }*/
}
        


function drawBroadcasts(publicKey) {
    document.getElementById("account-feeds").innerHTML = "<h2><b>Broadcasts</b></h2>"// | \<a href=\"#\" onclick=\"reloadMentions();\">Mentions</a> | \<a href=\"#\" onclick=\"reloadAssets();\">Assets</a></h2>";
    
    blockchain.getLatestBroadcastsBy(publicKey)
    
    function checkBroadcasts() {
        if (blockchain.RESPONSE_getLatestBroadcastsBy != null) {
            var broadcastIds = blockchain.RESPONSE_getLatestBroadcastsBy
            console.log("Broadcasts here")
            console.log(broadcastIds)
            var trueIds = []
            for (i = 0; i < broadcastIds.length; i++) {
                if (broadcastIds[i] > 0) {
                    trueIds.push(broadcastIds[i])
                }
            }
            cacheAssets(trueIds);
            readyToLoad = true;
            loadNextPosts();
        } else {
            setTimeout(checkBroadcasts, CALL_DELAY);
        }
    }
    checkBroadcasts()
    
    return
    conn.searchAssets("\"" + publicKey + "\"and=\"" + APP_ID + "\"") .then(assets => {
        assets = assets.filter(asset => asset.data.appId === APP_ID)
        cacheAssets(sort_assets_cron(assets));
        readyToLoad = true;
        loadNextPosts();
        /*var broadcasts = [];
        for (i = assets.length - 1; i >= 0; i--) { 
            var asset = assets[i]

            var assetElement = document.createElement("div");
                assetElement.id = asset.id;
                assetElement.className = "feedElement";
                assetElement.innerHTML += "<div class=\"feed-title\" id=\"title-" + asset.id + "\"><a class=\"a-light\" href=\"./account.html?name=" + getUsername(asset.data.account) + "\"><b>@" + getUsername(asset.data.account) + "</b></a></div>";
                assetElement.innerHTML += "<div class=\"feed-id\">" + asset.id + "</div>";

            if (asset.data.type === ASSET_BROADCAST) {
                feed.appendChild(assetElement);
                
                var spacerElement = document.createElement("div");
                spacerElement.className = "feedSpacer";
                feed.appendChild(spacerElement)
                setBroadcastTransaction(asset, assetElement)
                //broadcasts.push({a: asset, e: assetElement})
            }

        }*/
        
        //for (i = broadcasts.length-1; i >= 0; i--) {
            
        //}
        
        

    })
    
    
}


function drawAssets(publicKey) {
   
    
    /*if (!urlParams["name"]) {
        document.getElementById("username").innerHTML = "<b><h3>Your Account</h3></b>";
    } else {
        if (urlParams["name"] == getUsername(publicKey)) {
            document.getElementById("username").innerHTML = "<b><h3>Your Account</h3></b>";
        } else {
            document.getElementById("username").innerHTML = "<h3><b>Account: " + publicKey + "</b></h3>";
        }
    }*/
    
    document.getElementById("account-feeds").innerHTML = "<h2> \
    <a href=\"#\" onclick=\"reloadBroadcasts();\">Broadcasts</a> | \
    <a href=\"#\" onclick=\"reloadMentions();\">Mentions</a> | \
    <b>Assets</b></h2>";
    
    conn.searchAssets("\"" + publicKey + "\"and=\"" + APP_ID + "\"") .then(assets => {
        assets = assets.filter(asset => asset.data.appId === APP_ID)
        for (i = assets.length-1; i >= 0; i--) { 
            var asset = assets[i]

            var assetElement = document.createElement("div");
                assetElement.id = asset.id;
                assetElement.className = "feedElement";
                assetElement.innerHTML += "<div class=\"feed-title\"><b>ASSET</b></div>";
                assetElement.innerHTML += "<div class=\"feed-id\">" + asset.id + "</div>";
            if (asset.data.type === ASSET_ACCOUNT) {
                    assetElement.innerHTML += "<p><b>Account: " + asset.data.account + "</b></p>";
                    assetElement.innerHTML += "<p><i>This is your account asset, it controls your access to all of your other assets.</i></p>"
                }

            if (asset.data.type === ASSET_BROADCAST) {
                assetElement.innerHTML += "<p><b>Broadcast Message</b></p>";
                assetElement.innerHTML += "<p><i>This is a broadcast message asset, it is created by account assets.</i></p>"
            }
            feed.appendChild(assetElement);
            
            var spacerElement = document.createElement("div");
                spacerElement.className = "feedSpacer";
                feed.appendChild(spacerElement)
            
            setTransactions(asset, assetElement)

        }

    })
}







function setTransactions(asset, assetElement) {
    conn.listTransactions(asset.id) .then(transactions => {
        var HTML = ""
        for (l = transactions.length - 1; l >= 0; l--) {
                HTML += "<div class=\"metadata\"><h5>" + transactions[l].metadata.description + "</h5>"
                
                if (transactions[l].metadata.type === DATA_LOGIN) {
                    HTML += "<p>" + transactions[l].metadata.timestamp + "</p>"
                }
            
                if (transactions[l].metadata.type === DATA_MESSAGE) {
                    HTML += "<p>" + transactions[l].metadata.timestamp + "</p>"
                    
                    var finalMessage = parseRawMessage(transactions[l].metadata.message)
                    
                    
                    HTML += "<p>" + finalMessage + "</p>"
                }
                
                HTML += "</div>";
            }
        
        assetElement.innerHTML += HTML;
        
    })
}



