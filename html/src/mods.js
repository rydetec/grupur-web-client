/*
Filename: mods.js
Description: Responsible for declaration of all object prototype modifications
*/

// Object Prototypes

// Returns the ISO week of the date.
Date.prototype.getWeek = function() {
  var date = new Date(this.getTime());
  date.setHours(0, 0, 0, 0);
  // Thursday in current week decides the year.
  date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
  // January 4 is always in week 1.
  var week1 = new Date(date.getFullYear(), 0, 4);
  // Adjust to Thursday in week 1 and count number of weeks from date to week1.
  return 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000
                        - 3 + (week1.getDay() + 6) % 7) / 7);
}

// Returns the four-digit year corresponding to the ISO week of the date.
Date.prototype.getWeekYear = function() {
  var date = new Date(this.getTime());
  date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
  return date.getFullYear();
}

Date.prototype.getWeekString = function() {
    return (this.getWeekYear()) + "-" + this.getWeek().doubleDigitString()
}

Date.prototype.getWeekRangeString = function() {
    var monday = this.getFirstOfWeek()
    var sunday = this.getLastOfWeek()
    return monday.getDateString() + " - " + sunday.getDateString()
}

Date.prototype.setWeek = function(w = this.getWeek(), y = this.getWeekYear()) {
    return this.getFirstOfWeek(w,y);
}

Date.prototype.lastWeek = function() {
    var w = this.getWeek() - 1;
    var y = this.getWeekYear();
    if (w < 0) {
        y = y - 1
        w = 52
    }
    return this.setWeek(w,y);
}

Date.prototype.nextWeek = function() {
    var w = this.getWeek() + 1;
    var y = this.getWeekYear();
    if (w > 52) {
        y = y + 1
        w = 1
    }
    return this.setWeek(w,y);
}

Date.prototype.getFirstOfWeek = function(w = this.getWeek(), y = this.getWeekYear()) {
    //w = w || this.getWeek();
    //y = y || this.getWeekYear();
    var simple = new Date(y, 0, 1 + (w - 1) * 7);
    var dow = simple.getDay();
    var ISOweekStart = simple;
    if (dow <= 4)
        ISOweekStart.setDate(simple.getDate() - simple.getDay() + 1);
    else
        ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
    return ISOweekStart;
}

Date.prototype.getLastOfWeek = function() {
    var ISOweekEnd = this.getFirstOfWeek();
    ISOweekEnd.setDate(ISOweekEnd.getDate() + 6);
    return ISOweekEnd
}

Date.prototype.getMonthName = function() {
    var months    = ['January','February','March','April','May','June','July','August','September','October','November','December'];
    return months[this.getMonth()];
}

Date.prototype.getDateString = function() {
    return this.getMonthName() + " " + this.getDate()
}

Number.prototype.doubleDigitString = function() {
    return ("0" + this).slice(-2)
}

String.prototype.stripHTML = function() {
    var doc = new DOMParser().parseFromString(this, 'text/html');
    return doc.body.textContent || "";
}

String.prototype.encryptWith = async function(publicKeyIPFSHash, privateKeyIPFSHash, passwordString) {
    //await filesystem.add("/ipfs/" + publicKeyIPFSHash);
    //await filesystem.add("/ipfs/" + privateKeyIPFSHash);
    
    
    notify("[15%] Downloading account information, please wait...")
    await filesystem.pin.add("/ipfs/" + publicKeyIPFSHash);
    notify("[30%] Downloading account information, please wait...")
    await filesystem.pin.add("/ipfs/" + privateKeyIPFSHash);
    
    notify("[45%] Processing armored public key, please wait...")
    const publicKeyArmored = await filesystem.files.read("/ipfs/" + publicKeyIPFSHash);
    notify("[60%] Propcessing armored private key, please wait...")
    const privateKeyArmored = await filesystem.files.read("/ipfs/" + privateKeyIPFSHash);
    
    notify("[75%] Reading armored private key, please wait...")
    const privKeyObj = (await openpgp.key.readArmored(privateKeyArmored)).keys[0]
    try {
        notify("[90%] Attempting PGP decryption, please wait...")
        await privKeyObj.decrypt(passwordString)
        notify("[100%] Decryption complete.", 0)
    } catch (err) {
        notify("[FAIL] Decryption Failed!",  2)
        return null
    }
    return this
}

console.log("Mods loaded")