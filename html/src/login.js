//var mnemonic = bip39.generateMnemonic();

//var m = new Mnemonic("english")
//var words = "";//m.generate();
//alert(words);
//alert(m.check(words));


//const seed = bip39.mnemonicToSeed(mnemonic)
//var seed = m.toSeed(words).slice(0,32);
//alert(seed);
    
if (readCookie("publicKey")) {
    document.location = "./feed.html"
}


function doLogin() {
    let loginForm = document.getElementById("login-form");
    
    let username = loginForm.elements[0].value;
    let password = loginForm.elements[1].value;
    
    let store_password = loginForm.elements[2].checked;
    
    var loginContent = document.getElementById("login-content");
    
    //loginContent.innerHTML += "<h2>Attempting login, please wait...</h2>";
    notify("Attempting login, please wait...")
    
    blockchain.accountId(username)
        
    function checkResponse() {
        console.log("Checking account id for " + username)
        
        if (blockchain.requestResponse != null) {
            if (blockchain.requestResponse > 0) {
                
                blockchain.requestResponse = null
                //loginContent.innerHTML += "<h2>Verified username, checking password...</h2>";
                notify("Verified username, checking password...", 0)
                blockchain.accountPublicKey(username)
                
                async function checkPubResponse() {
                    console.log("Checking account public key hash for " + username)
                    if (blockchain.requestResponse != null) {
                        var publicKeyHash = blockchain.requestResponse
                        
                        blockchain.requestResponse = null
                        //loginContent.innerHTML += "<h2>Got public key hash, looking up private key hash...</h2>";
                        //var publicKey = await filesystem.files.read("/ipfs/" + publicKeyHash)
                        //console.log(publicKey)
                        //console.log((await openpgp.key.readArmored(publicKey)).keys)
                        blockchain.accountPrivateKey(username)
                        
                        async function checkPrivResponse() {
                            console.log("Checking account private key hash for " + username)
                            
                            if (blockchain.requestResponse != null) {
                                var privateKeyHash = blockchain.requestResponse
                                
                                blockchain.requestResponse = null
                                //loginContent.innerHTML += "<h2>Got private key hash, checking provided password...</h2>";
                                
                                var encryptedText = await "Login OK".encryptWith(publicKeyHash, privateKeyHash, password)
                                if (encryptedText != null) {
                                    createCookie("publicKey",publicKeyHash,1);
                                    createCookie("privateKey",privateKeyHash,1);
                                    if (store_password) {
                                        createCookie("password",password,1);
                                    }
                                    console.log("Password was right")
                                    //loginContent.innerHTML += "<h2>Your password was correct. One moment...</h2>";
                                    notify("Your password was correct. One moment...", 0)
                                    document.location = "./account.html"
                                } else {
                                    console.log("Password was wrong")
                                    //loginContent.innerHTML += "<h2>Your password was incorrect. Please try again.</h2>";
                                    notify("Your password was incorrect. Please try again.", 2)
                                    loginForm.elements[1].value = ""
                                }
                            } else {
                                setTimeout(checkPrivResponse, 1000);
                            }
                        }
                        checkPrivResponse()
                        
                        
                    //if (store_private_key) {
                        //console.log("Private Key Stored.");
                        //createCookie("privateKey",user.privateKey,1);
                    //}
                    
                        //document.location = "./account.html"

                    } else {
                        setTimeout(checkPubResponse, 1000);
                    }
                }
                checkPubResponse()
                
                
            } else {
                blockchain.requestResponse = null
                //loginContent.innerHTML += "<h2>Could not find user with username " + username + "</h2>";
                notify("Could not find user with username " + username, 2)
                loginForm.elements[0].value = ""
                loginForm.elements[1].value = ""
            }
        } else {
            setTimeout(checkResponse, 1000);
        }
    }
    checkResponse()
    
    return
    
}