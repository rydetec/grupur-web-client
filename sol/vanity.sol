pragma solidity >=0.4.22 <0.6.0;
pragma experimental ABIEncoderV2;

contract VanityStorage {
    bool private connected = true;
    Vanity[] private vanities;
    struct Vanity {
        string publicKeyAddress;
        string avatarPath;
        string vanityName;
        string bio;
    }
    
    function isConnected() public view returns (bool) {
        return connected;
    }
    
    function getVanaties() public view returns (Vanity[] memory) {
        return vanities;
    }
    
    function addVanity(Vanity memory _vanity) public {
        vanities.push(_vanity);
    }

}

contract VanityManager {
    function addVanity(address _contract, string memory _public, string memory _avatar, string memory _vanity, string memory _bio) public {
        VanityStorage vanStorage = VanityStorage(_contract);
        
        if (!vanStorage.isConnected()) {
            revert();
        } else {
            vanStorage.addVanity(VanityStorage.Vanity(_public,_avatar,_vanity,_bio));
        }
    }
    
    function getLatestVanity(address _contract, string memory _public) public view returns (string memory, string memory, string memory, string memory) {
        VanityStorage vanStorage = VanityStorage(_contract);
        
        if (!vanStorage.isConnected()) {
            revert();
        } else {
            
            VanityStorage.Vanity[] memory vanities = vanStorage.getVanaties();
            if (vanities.length == 0) {
                return (_public,"","","");
            }
            
            for (uint i = vanities.length - 1; i >= 0; i--) {
                if (keccak256(abi.encodePacked(vanities[i].publicKeyAddress)) == keccak256(abi.encodePacked(_public))) {
                    return (vanities[i].publicKeyAddress,vanities[i].avatarPath,vanities[i].vanityName,vanities[i].bio);
                }
            }
            
        }
        return (_public,"","","");
    }
    
    function vanityCount(address _contract) public view returns (uint) {
        VanityStorage vanStorage = VanityStorage(_contract);
        return vanStorage.getVanaties().length;
    }
}