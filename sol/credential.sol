pragma solidity >=0.4.22 <0.6.0;
pragma experimental ABIEncoderV2;

contract CredentialStorage {
    bool private connected = true;
    Credential[] private credentials;
    struct Credential {
        string username;
        string publicKeyAddress;
        string privateKeyAddress;
        string revokeCertAddress;
    }
    
    function isConnected() public view returns (bool) {
        return connected;
    }
    
    function getCredentials() public view returns (Credential[] memory) {
        return credentials;
    }
    
    function addCredential(Credential memory _cred) public {
        credentials.push(_cred);
    }
}

contract CredentialManager {
    function register(address _contract, string memory _username, string memory _pub, string memory _priv, string memory _rev) public {
        CredentialStorage credStorage = CredentialStorage(_contract);
        if (this.getId(_contract, _username) > 0 || !credStorage.isConnected()) {
            revert();
        } else {
            credStorage.addCredential(CredentialStorage.Credential(_username,_pub,_priv,_rev));
        }
    }
    
    function getId(address _contract, string memory _username) public view returns (uint) {
        CredentialStorage credStorage = CredentialStorage(_contract);
        
        if (!credStorage.isConnected()) {
            revert();
        } else {
            CredentialStorage.Credential[] memory credentials = credStorage.getCredentials();
            for (uint i = 0; i < credentials.length; i++) {
                if (keccak256(abi.encodePacked(credentials[i].username)) == keccak256(abi.encodePacked(_username))) {
                    return i+1;
                }
            }
        }
        return 0;
    }
    
    function getPublicKey(address _contract, string memory _username) public view returns (string memory) {
        CredentialStorage credStorage = CredentialStorage(_contract);
        
        if (!credStorage.isConnected()) {
            revert();
        } else {
            CredentialStorage.Credential[] memory credentials = credStorage.getCredentials();
            for (uint i = 0; i < credentials.length; i++) {
                if (keccak256(abi.encodePacked(credentials[i].username)) == keccak256(abi.encodePacked(_username))) {
                    return credentials[i].publicKeyAddress;
                }
            }
        }
        return "";
    }
    
    function getPrivateKey(address _contract, string memory _username) public view returns (string memory) {
        CredentialStorage credStorage = CredentialStorage(_contract);
        
        if (!credStorage.isConnected()) {
            revert();
        } else {
            CredentialStorage.Credential[] memory credentials = credStorage.getCredentials();
            for (uint i = 0; i < credentials.length; i++) {
                if (keccak256(abi.encodePacked(credentials[i].username)) == keccak256(abi.encodePacked(_username))) {
                    return credentials[i].privateKeyAddress;
                }
            }
        }
        return "";
    }
    
    function getRevokeCert(address _contract, string memory _username) public view returns (string memory) {
        CredentialStorage credStorage = CredentialStorage(_contract);
        
        if (!credStorage.isConnected()) {
            revert();
        } else {
            CredentialStorage.Credential[] memory credentials = credStorage.getCredentials();
            for (uint i = 0; i < credentials.length; i++) {
                if (keccak256(abi.encodePacked(credentials[i].username)) == keccak256(abi.encodePacked(_username))) {
                    return credentials[i].revokeCertAddress;
                }
            }
        }
        return "";
    }
    
    
}