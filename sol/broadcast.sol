pragma solidity >=0.4.22 <0.6.0;
pragma experimental ABIEncoderV2;
import "browser/strings.sol";

contract BroadcastStorage {
    bool private connected = true;
    Broadcast[] private broadcasts;
    struct Broadcast {
        string publicKeyAddress;
        uint year;
        uint week;
        string timestamp;
        string message;
        string filename;
        string hash;
    }
    
    function isConnected() public view returns (bool) {
        return connected;
    }
    
    function getBroadcasts() public view returns (Broadcast[] memory) {
        return broadcasts;
    }
    
    function addBroadcast(Broadcast memory _broadcast) public {
        broadcasts.push(_broadcast);
    }
}

contract BroadcastManager {
    using strings for *;
    
    function () external payable {
        
    }
    
    function addBroadcast(address _contract, string memory _public, uint _year, uint _week, string memory _timestamp, string memory _message, string memory _filename, string memory _hash) public {
        BroadcastStorage broadStorage = BroadcastStorage(_contract);
        
        if (!broadStorage.isConnected()) {
            revert();
        } else {
            broadStorage.addBroadcast(BroadcastStorage.Broadcast(_public,_year,_week,_timestamp,_message,_filename,_hash));
        }
    }
    
    function getLatestBroadcastsBy(address _contract, string memory _public) public view returns (uint[] memory) {
        BroadcastStorage broadStorage = BroadcastStorage(_contract);
        
        uint[] memory broadcastIds = new uint[](30);
        
        if (!broadStorage.isConnected()) {
            revert();
        } else {
            BroadcastStorage.Broadcast[] memory broadcasts = broadStorage.getBroadcasts();
            if (broadcasts.length == 0) {
                return broadcastIds;
            }
            
            uint nextId = 0;
            for (uint i = 0; i < broadcasts.length; i++) {
                uint index = (broadcasts.length - 1) - i;
                if (keccak256(abi.encodePacked(broadcasts[index].publicKeyAddress)) == keccak256(abi.encodePacked(_public))) {
                    broadcastIds[nextId] = (index+1);
                    nextId++;
                }
            }
            return broadcastIds;
        }
    }
    
    function getBroadcastsUnder(address _contract, string memory _index) public view returns (uint[] memory) {
        BroadcastStorage broadStorage = BroadcastStorage(_contract);
        
        uint[] memory broadcastIds = new uint[](30);
        
        if (!broadStorage.isConnected()) {
            revert();
        } else {
            BroadcastStorage.Broadcast[] memory broadcasts = broadStorage.getBroadcasts();
            if (broadcasts.length == 0) {
                return broadcastIds;
            }
            
            uint nextId = 0;
            for (uint i = 0; i < broadcasts.length; i++) {
                uint index = (broadcasts.length - 1) - i;
                
                string memory message = broadcasts[index].message;
                string memory term = ">".toSlice().concat(_index.toSlice());
                
                if (message.toSlice().contains(term.toSlice())) {
                    broadcastIds[nextId] = (index+1);
                    nextId++;
                }
            }
            return broadcastIds;
        }
    }
    
    function getBroadcastsDuring(address _contract, uint _year, uint _week) public view returns (uint[] memory) {
        BroadcastStorage broadStorage = BroadcastStorage(_contract);
        
        uint[] memory broadcastIds = new uint[](30);
        
        if (!broadStorage.isConnected()) {
            revert();
        } else {
            BroadcastStorage.Broadcast[] memory broadcasts = broadStorage.getBroadcasts();
            if (broadcasts.length == 0) {
                return broadcastIds;
            }
            
            uint nextId = 0;
            for (uint i = 0; i < broadcasts.length; i++) {
                uint index = (broadcasts.length - 1) - i;
                if (broadcasts[index].year == _year && broadcasts[index].week == _week) {
                    broadcastIds[nextId] = (index+1);
                    nextId++;
                }
            }
            return broadcastIds;
        }
    }
    
    function getTaggedBroadcastsDuring(address _contract, uint _year, uint _week, string memory _tag) public view returns (uint[] memory) {
        BroadcastStorage broadStorage = BroadcastStorage(_contract);
        
        uint[] memory broadcastIds = new uint[](30);
        
        if (!broadStorage.isConnected()) {
            revert();
        } else {
            BroadcastStorage.Broadcast[] memory broadcasts = broadStorage.getBroadcasts();
            if (broadcasts.length == 0) {
                return broadcastIds;
            }
            
            uint nextId = 0;
            for (uint i = 0; i < broadcasts.length; i++) {
                uint index = (broadcasts.length - 1) - i;
                
                string memory message = broadcasts[index].message.lower();
                string memory term = "#".toSlice().concat(_tag.lower().toSlice());
                
                if (broadcasts[index].year == _year && broadcasts[index].week == _week && message.toSlice().contains(term.toSlice())) {
                    broadcastIds[nextId] = (index+1);
                    nextId++;
                }
            }
            return broadcastIds;
        }
    }
    
    function getBroadcastDetails(address _contract, uint _index) public view returns (string memory, uint, uint, string memory, string memory, string memory, string memory) {
        BroadcastStorage broadStorage = BroadcastStorage(_contract);
        BroadcastStorage.Broadcast[] memory broadcasts = broadStorage.getBroadcasts();
        if (_index == 0 || _index > broadcasts.length) {
            revert();
        } else {
            BroadcastStorage.Broadcast memory b = broadcasts[_index - 1];
            return (b.publicKeyAddress,b.year,b.week,b.timestamp,b.message,b.filename,b.hash);
        }
    }
    
    function getBroadcastAt(address _contract, string memory _timestamp) public view returns (uint) {
        BroadcastStorage broadStorage = BroadcastStorage(_contract);
        
        if (!broadStorage.isConnected()) {
            revert();
        } else {
            BroadcastStorage.Broadcast[] memory broadcasts = broadStorage.getBroadcasts();
            if (broadcasts.length == 0) {
                return 0;
            }
            
            for (uint i = 0; i < broadcasts.length; i++) {
                uint index = (broadcasts.length - 1) - i;
                if (broadcasts[index].timestamp.toSlice().compare(_timestamp.toSlice()) > 0) {
                    return (index+1);
                }
            }
        }
    }
}