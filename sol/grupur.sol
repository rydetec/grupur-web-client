pragma solidity >=0.4.22 <0.6.0;
pragma experimental ABIEncoderV2;
import "browser/strings.sol";
import 'browser/credential.sol';
import 'browser/vanity.sol';
import 'browser/wallet.sol';
import 'browser/broadcast.sol';

contract GruPur {
    using strings for *;
    
    string version = "0.0.2a1";
    CredentialManager credManager = new CredentialManager();
    VanityManager vanManager = new VanityManager();
    WalletManager walManager = new WalletManager();
    BroadcastManager broadManager = new BroadcastManager();
    
    function () external payable {
        
    }
    
    function getVersion() public view returns (string memory) {
        return version;
    }
    
    function registerCredential(address _contract, address _wallet, string memory _username, string memory _pub, string memory _priv, string memory _rev) public {
        credManager.register(_contract,_username,_pub,_priv,_rev);
        walManager.transferStatus(_wallet,"SYSTEM",_pub,_pub);
    }
    
    function getCredentialId(address _contract, string memory _username) public view returns (uint) {
        return credManager.getId(_contract,_username);
    }
    
    function getCredentialPublicKey(address _contract, string memory _username) public view returns (string memory) {
        return credManager.getPublicKey(_contract,_username);
    }
    
    function getCredentialPrivateKey(address _contract, string memory _username) public view returns (string memory) {
        return credManager.getPrivateKey(_contract,_username);
    }
    
    function getCredentialRevokeCert(address _contract, string memory _username) public view returns (string memory) {
        return credManager.getRevokeCert(_contract,_username);
    }
    
    function addVantiy(address _contract, string memory _publicKey, string memory _avatar, string memory _vanity, string memory _bio) public {
        vanManager.addVanity(_contract,_publicKey,_avatar,_vanity,_bio);
    }
    
    function getLatestVanity(address _contract, string memory _publicKey) public view returns (string memory, string memory, string memory, string memory) {
        return vanManager.getLatestVanity(_contract,_publicKey);
    }
    
    function getStatusBalance(address _contract, string memory _publicKey) public view returns (uint) {
        return walManager.getStatusBalance(_contract,_publicKey);
    }
    
    function getRelevanceBalance(address _contract, string memory _publicKey) public view returns (uint[] memory) {
        (uint[] memory balances, string[] memory tags) = walManager.getRelevanceBalance(_contract,_publicKey);
        return balances;
    }
    
    function getRelevanceTag(address _contract, string memory _publicKey, uint _index) public view returns (string memory) {
        (uint[] memory balances, string[] memory tags) = walManager.getRelevanceBalance(_contract,_publicKey);
        return tags[_index];
    }
    
    function addBroadcast(address _contract, address _wallet, string memory _public, uint _year, uint _week, string memory _timestamp, string memory _message, string memory _filename, string memory _hash) public {
        broadManager.addBroadcast(_contract,_public,_year,_week,_timestamp,_message,_filename,_hash);
        walManager.transferStatus(_wallet,"SYSTEM",_public, uint2str(broadManager.getBroadcastAt(_contract,_timestamp)));
    }
    
    function getLatestBroadcastsBy(address _contract, string memory _public) public view returns (uint[] memory) {
        return broadManager.getLatestBroadcastsBy(_contract,_public);
    }
    
    function getBroadcastDetails(address _contract, uint _index) public view returns (string memory, uint, uint, string memory, string memory, string memory, string memory) {
        return broadManager.getBroadcastDetails(_contract,_index);
    }
    
    function getBroadcastsDuring(address _contract, uint _year, uint _week) public view returns (uint[] memory) {
        return broadManager.getBroadcastsDuring(_contract,_year,_week);
    }
    
    function getTaggedBroadcastsDuring(address _contract, uint _year, uint _week, string memory _tag) public view returns (uint[] memory) {
        return broadManager.getTaggedBroadcastsDuring(_contract,_year,_week,_tag);
    }
    
    function getBroadcastsUnder(address _contract, string memory _index) public view returns (uint[] memory) {
        return broadManager.getBroadcastsUnder(_contract,_index);
    }
    
    function uint2str(uint _i) internal pure returns (string memory _uintAsString) {
        if (_i == 0) {
            return "0";
        }
        uint j = _i;
        uint len;
        while (j != 0) {
            len++;
            j /= 10;
        }
        bytes memory bstr = new bytes(len);
        uint k = len - 1;
        while (_i != 0) {
            bstr[k--] = byte(uint8(48 + _i % 10));
            _i /= 10;
        }
        return string(bstr);
    }
}