pragma solidity >=0.4.22 <0.6.0;
pragma experimental ABIEncoderV2;

contract WalletStorage {
    bool private connected = true;
    
    StatusTransfer[] private statusTransfers;
    RelevanceTransfer[] private relevanceTransfers;
    
    struct Relevance {
        string tag;
        uint amount;
    }
    
    struct Status {
        uint amount;
    }
    
    struct StatusTransfer {
        string owner;
        string reciever;
        string forAsset;
        Status status;
    }
    
    struct RelevanceTransfer {
        string owner;
        string reciever;
        string forAsset;
        Relevance relevance;
    }
    
    function isConnected() public view returns (bool) {
        return connected;
    }
    
    function getStatusTransfers() public view returns (StatusTransfer[] memory) {
        return statusTransfers;
    }
    
    function getRelevanceTransfers() public view returns (RelevanceTransfer[] memory) {
        return relevanceTransfers;
    }
    
    function addStatusTransfer(string memory _owner, string memory _reciever, string memory _forAsset, uint _amount) public {
        statusTransfers.push(StatusTransfer(_owner,_reciever,_forAsset,Status(_amount)));
    }
    
    function addRelevanceTransfer(string memory _owner, string memory _reciever, string memory _forAsset, string memory _tag, uint _amount) public {
        relevanceTransfers.push(RelevanceTransfer(_owner,_reciever,_forAsset,Relevance(_tag,_amount)));
    }

}

contract WalletManager {
    function () external payable {
        
    }
    
    function getStatusBalance(address _contract, string memory _public) public view returns (uint) {
        WalletStorage walStorage = WalletStorage(_contract);
        
        if (!walStorage.isConnected()) {
            revert();
        } else {
            WalletStorage.StatusTransfer[] memory statusTransfers = walStorage.getStatusTransfers();
            
            uint balance = 0;
            for (uint j = 0; j < statusTransfers.length; j++) {
                if (keccak256(abi.encodePacked(statusTransfers[j].reciever)) == keccak256(abi.encodePacked(_public))) {
                    balance += statusTransfers[j].status.amount;
                }
                if (keccak256(abi.encodePacked(statusTransfers[j].owner)) == keccak256(abi.encodePacked(_public))) {
                    if (balance >= statusTransfers[j].status.amount) {
                        balance -= statusTransfers[j].status.amount;
                    } else {
                        balance = 0;
                    }
                    
                }
            }
            return balance;
        }
        return 0;
    }
    
    function transferStatus(address _contract, string memory _sender, string memory _reciever, string memory _forAsset) public {
        WalletStorage walStorage = WalletStorage(_contract);
        
        if (!walStorage.isConnected()) {
            revert();
        } else {
            if (keccak256(abi.encodePacked("")) == keccak256(abi.encodePacked(_reciever)) || keccak256(abi.encodePacked(_sender)) == keccak256(abi.encodePacked(_reciever))) {
                revert();
            } else {
                walStorage.addStatusTransfer(_sender,_reciever,_forAsset,1);
            }
        }
    }
    
    function transferRelevance(address _contract, string memory _sender, string memory _reciever, string memory _forAsset, string memory _tag) public {
        WalletStorage walStorage = WalletStorage(_contract);
        
        if (!walStorage.isConnected()) {
            revert();
        } else {
            if (keccak256(abi.encodePacked("")) == keccak256(abi.encodePacked(_reciever)) || keccak256(abi.encodePacked(_sender)) == keccak256(abi.encodePacked(_reciever))) {
                revert();
            } else {
                walStorage.addRelevanceTransfer(_sender,_reciever,_forAsset,_tag,1);
            }
        }
    }
    
    function getRelevanceBalance(address _contract, string memory _public) public view returns (uint[] memory,string[] memory) {
        WalletStorage walStorage = WalletStorage(_contract);
        
        uint[] memory balances = new uint[](30);
        string[] memory tags = new string[](30);
        
        if (!walStorage.isConnected()) {
            revert();
        } else {
            WalletStorage.RelevanceTransfer[] memory relevanceTransfers = walStorage.getRelevanceTransfers();
            
            for (uint j = 0; j < relevanceTransfers.length; j++) {
                uint index = 0;
                for (uint k = 0; k < tags.length; k++) {
                    if (keccak256(abi.encodePacked(tags[k])) == keccak256(abi.encodePacked(relevanceTransfers[j].relevance.tag))) {
                        index = k + 1;
                        break;
                    }
                }
                if (index == 0) {
                    for (uint k = 0; k < tags.length; k++) {
                        if (keccak256(abi.encodePacked(tags[k])) == keccak256(abi.encodePacked(""))) {
                            tags[k] = relevanceTransfers[j].relevance.tag;
                            balances[k] = 0;
                            index = k + 1;
                            break;
                        }
                    }
                }
                
                index--;
                
                if (keccak256(abi.encodePacked(relevanceTransfers[j].reciever)) == keccak256(abi.encodePacked(_public))) {
                    balances[index] += relevanceTransfers[j].relevance.amount;
                }
                if (keccak256(abi.encodePacked(relevanceTransfers[j].owner)) == keccak256(abi.encodePacked(_public))) {
                    if (balances[index] >= relevanceTransfers[j].relevance.amount) {
                        balances[index] -= relevanceTransfers[j].relevance.amount;
                    } else {
                        balances[index] = 0;
                    }
                    
                }
            }
        
            return (balances,tags);
        }
        return (balances,tags);
    }
    
}